package codelamp.pio;

import java.util.ArrayList;

public final class Language {

	private final String language;
	private final ArrayList<String> extension;

	private Language(String language, ArrayList<String> extension) {
        this.language = language;
        this.extension = extension;
	}

	public final String getLanguage() {
		return this.language;
	}

	public final String toString() {
		StringBuffer strings = new StringBuffer();
		strings.append(this.language);
		strings.append(" [");
		for (String entry : this.extension) {
			strings.append(entry);
			strings.append(" , ");
		}
		strings.delete(strings.length() - 3, strings.length() - 1);
		strings.append("]");
		return strings.toString();
	}

	public final ArrayList<String> getExtension() {
		return extension;
	}

	public static final Language EMPTY;
	public static final ArrayList<String> EMPTY_EXTENSION = new ArrayList<>();
	static {
		EMPTY_EXTENSION.add("");
		EMPTY = new Language("", EMPTY_EXTENSION);
	}

	public static final Language JAVA;
	private static final ArrayList<String> JAVA_EXTENSION = new ArrayList<>();
	static {
		JAVA_EXTENSION.add(".java");
		JAVA = new Language("Java", JAVA_EXTENSION);
	}

	private static final ArrayList<Language> languageList = new ArrayList<>();
	static {
		languageList.add(JAVA);
	}

	public static final ArrayList<Language> getAll() {
		return languageList;
	}

	public static Language getLanguage(String extension) {
		for (Language entry : languageList) {
			if (entry.extension.contains(extension)) {
				return entry;
			}
		}
		return EMPTY;
	}

	public static ArrayList<String> getExtension(String language) {
		for (Language entry : languageList) {
			if (entry.language.equals(language)) {
				return entry.extension;
			}
		}
		return EMPTY_EXTENSION;
	}

	public static Language parse(String language) {
		for (Language entry : languageList) {
			if (entry.language.equals(language)) {
				return entry;
			}
		}
		return EMPTY;
	}

	public static boolean has(String language) {
		if (parse(language) == EMPTY) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean has(Language language) {
		if (Language.languageList.contains(language)) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		System.out.println(Language.languageList.get(0).toString());
	}

}
