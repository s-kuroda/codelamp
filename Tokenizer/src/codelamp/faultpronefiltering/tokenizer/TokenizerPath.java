package codelamp.faultpronefiltering.tokenizer;

import java.nio.file.Path;
import java.nio.file.Paths;

public class TokenizerPath {
	private TokenizerPath() {
	}
	public static final Path JAVA_TOKENIZER = Paths.get(System.getProperty("user.dir"), "app/mkJavaTokenSC.exe");

}
