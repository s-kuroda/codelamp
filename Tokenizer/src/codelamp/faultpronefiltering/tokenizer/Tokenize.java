package codelamp.faultpronefiltering.tokenizer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import codelamp.util.Commands;

public class Tokenize {

	private Tokenize() {
	}

	private static int prefix = 0;

	private static Path arrangeEmptyLines(Path oldPath) {
		if (oldPath == null || !Files.exists(oldPath)) {
			return null;
		}
		Path newPath = Paths.get(oldPath.getParent().toString(), "EMP" + oldPath.getFileName().toString());
		if (!Files.exists(newPath)) {
			try {
				Files.createFile(newPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		BufferedReader buffReader = null;
		BufferedWriter buffWriter = null;
		try {
			buffReader =  Files.newBufferedReader(oldPath, Charset.forName("UTF-8"));
	    	buffWriter =  Files.newBufferedWriter(newPath, Charset.forName("UTF-8"));
			String str;
			boolean flag = false;
	    	while((str = buffReader.readLine()) != null){
	    		str = str.trim();
				if (str == null || str.equals("")) {
					if (flag == true) {
						continue;
					} else {
			    		buffWriter.newLine();
			    		flag = true;
					}
				} else {
					buffWriter.write(str);
		    		buffWriter.newLine();
		    		flag = false;
				}
			}
	    	buffWriter.flush();
			buffReader.close();
			buffWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return newPath;
	}

	private static Path writeOutStringList(List<String> list, Path dir) {
		if (dir == null || !Files.exists(dir) || !Files.isDirectory(dir)) {
			return null;
		}
		Path newPath = Paths.get(dir.toString(), "TMP_STRTXT.txt");
		try {
			if (Files.exists(newPath)) {
				Files.delete(newPath);
			}
			Files.createFile(newPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (BufferedWriter buffWriter =  Files.newBufferedWriter(newPath, Charset.forName("UTF-8"))) {
			for (String str : list) {
				buffWriter.write(str);
	    		buffWriter.newLine();
			}
	    	buffWriter.flush();
			buffWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return newPath;
	}

	public static void tokenizeLargeFile(Path oldPath, Path newPath) {
		if (oldPath == null || newPath == null) {
			return;
		}
		if (!Files.exists(newPath)) {
			try {
				Files.createFile(newPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Path empPath = arrangeEmptyLines(oldPath);
		List<String> strList = new ArrayList<>();
		try (BufferedReader buffReader = Files.newBufferedReader(empPath, Charset.forName("UTF-8"))) {
			String str;
			int count = 0;
			while((str = buffReader.readLine()) != null){
				strList.add(str);
				if (count > 50) {
					count = 0;
					Path path = writeOutStringList(strList, empPath.getParent());
					if (path == null || !Files.exists(path)) {
						return;
					} else {
						tokenizeJava(path, newPath);
						strList.clear();
					}
				} else {
					count++;
				}
			}
			if (strList != null && !strList.isEmpty()) {
				Path path = writeOutStringList(strList, empPath.getParent());
				if (path == null || !Files.exists(path)) {
					return;
				} else {
					tokenizeJava(path, newPath);
					strList.clear();
				}
			}
	    	buffReader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("Complete!!");
	}


	public static void tokenizeJava(Path oldPath, Path newPath) {
		if (oldPath == null || newPath == null) {
			return;
		}
		if (!Files.exists(newPath)) {
			try {
				Files.createFile(newPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			List<String> pl = Commands.cat(oldPath.toFile());
			pl.add("|");
			pl.add(TokenizerPath.JAVA_TOKENIZER.toString());
			ProcessBuilder pb = new ProcessBuilder(pl);
			pb.redirectOutput(Redirect.appendTo(newPath.toFile()));
			try {
				Process p = pb.start();
//				InputStream is = p.getInputStream();	//標準出力
//				printInputStream(is);
				InputStream es = p.getErrorStream();	//標準エラー
				printInputStream(es);
				p.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			pl.clear();} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Path oldBug = Paths.get(System.getProperty("user.dir"), "BUG.txt");
		Path newBug = Paths.get(System.getProperty("user.dir"), "BUGToken.txt");
		tokenizeLargeFile(oldBug, newBug);
//		System.out.println(arrangeEmptyLines(oldBug));
//		tokenizeJava(oldBug, newBug);
		System.err.println("BUG TOKENIZED!!!");
		Path oldInn = Paths.get(System.getProperty("user.dir"), "INN.txt");
		Path newInn = Paths.get(System.getProperty("user.dir"), "INNToken.txt");
		tokenizeLargeFile(oldInn, newInn);
		System.err.println("INNOCENT TOKENIZED!!!");
	}

	public static void printInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		try {
			for (;;) {
				String line = br.readLine();
				if (line == null) break;
				System.out.println(line);
			}
		} finally {
			br.close();
		}
	}
}
