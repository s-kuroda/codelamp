package codelamp.internal.config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import codelamp.internal.pluginlamp.FileListCapture;
import codelamp.pio.Language;
import codelamp.pio.TargetFile;

/**
 * フィルタリングされたファイルの情報を保存する
 * @author zitsp775
 */
public class FileRecordsConfig implements IXMLHandler{

    private final File configXML;
    private final Language language;
	private ArrayList<TargetFile> fileList = new ArrayList<>();
	private final  double TMP_THRETHHOLD = 0.5;

	/**
	 * コンストラクタ
	 * @param file コンフィグ用XML
	 * 	cannot {@code null}
	 */
    public FileRecordsConfig(File file, Language language) {
        this.language = language;
		if (file.exists()) {
			this.configXML = file;
				try {
                    this.read();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XMLStreamException e) {
                    this.init();
                }
		} else {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.configXML = file;
            this.init();
		}
	}

    private void init() {
        this.configXML.delete();
        File configDir = this.configXML.getParentFile();
        File projectDir = configDir.getParentFile();
        try {
            this.configXML.createNewFile();
            this.fileList.clear();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        this.language.getExtension().forEach(e -> {
            FileRecordsConfig.this.fileList.addAll(FileListCapture.getChildTargetFileList(projectDir, e));
        });
    }

    private static enum XMLFILE {FILE};
    private static enum XMLTAG {PROJECT, NAME, LAST_MODIFIED, LAST_CLASSIFIED, CLASSIFIED_VALUE, INCORRECT, EXCLUDED};

    @Override
    public void read() throws IOException, XMLStreamException {
        if (! this.configXML.exists()) {
            throw new FileNotFoundException();
        }
        XMLInputFactory factory = XMLInputFactory.newInstance();
        BufferedInputStream stream = new BufferedInputStream(new FileInputStream(this.configXML));
        XMLStreamReader reader = factory.createXMLStreamReader(stream);
        for (;reader.hasNext(); reader.next()) {
            if ((reader.getEventType() == XMLStreamConstants.START_ELEMENT)
                    && (reader.getName().toString() == XMLFILE.FILE.toString())) {
                reader = createFileRecod(reader);
            }
        }
        reader.close();
    }

    /**
     * XMLに保存された各ソースコードに関する情報を読み込む
     * @param readerb 読み出し位置
     * @return 読み出し位置
     */
    public XMLStreamReader createFileRecod(XMLStreamReader reader) {
        String fileName = null;
        long lastClassified = 0;
        double classifiedValue = 0;
        boolean excluded = false;
        boolean incorrect = false;
        boolean correctTag = true;
        QName element = reader.getName();
        try {
            for (;reader.hasNext(); reader.next()) {
                if (reader.getEventType() == XMLStreamConstants.START_ELEMENT) {
                    if (reader.getName().toString().equals(XMLTAG.NAME.toString())) {
                        fileName = reader.getElementText();
                    } else if (reader.getName().toString().equals(XMLTAG.LAST_CLASSIFIED.toString())) {
                        lastClassified = Long.parseLong(reader.getElementText());
                    } else if (reader.getName().toString().equals(XMLTAG.CLASSIFIED_VALUE.toString())) {
                        classifiedValue = Double.parseDouble(reader.getElementText());
                    } else if (reader.getName().toString().equals(XMLTAG.EXCLUDED.toString())) {
                        excluded = Boolean.parseBoolean(reader.getElementText());
                    } else if (reader.getName().toString().equals(XMLTAG.INCORRECT.toString())) {
                        incorrect = true;
                        correctTag = Boolean.parseBoolean(reader.getElementText());
                    }
                }
                if ((reader.getEventType() == XMLStreamConstants.END_ELEMENT)
                        && (reader.getName().equals(element))) {
                    break;
                }
            }
            if (fileName == null) {
                return reader;
            }
            TargetFile file = new TargetFile(fileName);
            if (! file.exists()) {
            	return reader;
            }
            file.setLastClassified(lastClassified);
            file.setInferenceVal(classifiedValue, this.TMP_THRETHHOLD);
            if (excluded == true) {
                file.exclude();
            }
            if (incorrect == true) {
                file.setTag(correctTag);
            }
    	    System.out.println("FI_CON : "+ file.getName() + file.getInferenceVal());
            this.fileList.add(file);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return reader;
    }

    @Override
    public void write() throws IOException, XMLStreamException {
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        factory.setProperty("javax.xml.stream.isRepairingNamespaces", Boolean.TRUE);
        XMLStreamWriter writer = factory.createXMLStreamWriter(new FileWriter(this.configXML));
        writer.writeStartDocument();
        writer.writeStartElement("CONFIG");
        writer.writeCharacters("FILE_LIST");
        for (TargetFile file: this.fileList) {
	        writer.writeStartElement(XMLFILE.FILE.toString());
	        writer.writeStartElement(XMLTAG.NAME.toString());
	        writer.writeCharacters(file.getAbsolutePath());
	        writer.writeEndElement();
	        if (file.lastClassified() != 0) {
	            writer.writeStartElement(XMLTAG.LAST_CLASSIFIED.toString());
	            writer.writeCharacters(String.valueOf(file.lastClassified()));
	            writer.writeEndElement();
	            writer.writeStartElement(XMLTAG.CLASSIFIED_VALUE.toString());
	            writer.writeCharacters(String.valueOf(file.getInferenceVal()));
	            writer.writeEndElement();
	        }
	        if (file.isExcluded() == true) {
	            writer.writeStartElement(XMLTAG.EXCLUDED.toString());
	            writer.writeCharacters(String.valueOf(true));
	            writer.writeEndElement();
	        }
	        if (file.isErrata() == true) {
	            writer.writeStartElement(XMLTAG.INCORRECT.toString());
	            writer.writeCharacters(file.getTag());
	            writer.writeEndElement();
	        }
	        writer.writeEndElement();
        }
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();
    }

	/**
	 * 読み込んで取得したデータを取り出す
	 * @return 取得した全データ
	 */
	public ArrayList<TargetFile> getFileList() {
		return fileList;
	}

	/**
	 * 書き出し前にファイルリストを更新する
	 * @param fileList
	 */
	public void updateFileList(ArrayList<TargetFile> fileList) {
		this.fileList = fileList;
	}

    public static void main(String[] args) {
        File file = new File(System.getProperty("user.dir"),"db.xml");
        FileRecordsConfig config = new FileRecordsConfig(file, Language.JAVA);
        System.out.println(config.fileList);
//        ArrayList<TargetFile> list = FileListCapture.getChildTargetFileList(file.getParentFile());
//        config.fileList = list;
        try {
            config.write();
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
