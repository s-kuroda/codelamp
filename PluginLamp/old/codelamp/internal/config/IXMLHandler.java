package codelamp.internal.config;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

/**
 * コンフィグXMLの読み書きインターフェース
 * @author zitsp
 */
public interface IXMLHandler {
	/**
	 * 読み込み。データはクラスが保持し、外部にとり出だす場合は、別途ゲッターを設ける。
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public abstract void read() throws IOException, XMLStreamException;
	/**
	 * 書き込み。データはクラスが保持する。これを更新するためのセッターを別途設ける。
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public abstract void write() throws IOException, XMLStreamException;
}
