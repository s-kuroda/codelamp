package codelamp.internal.pluginlamp;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;

public class GitResultTable extends Composite {

	public GitResultTable(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1,false));
		TreeViewer treeViewer = new TreeViewer(this, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL);
		Tree tree = treeViewer.getTree();
		treeViewer.setLabelProvider(new TreeLabelProvider());
		treeViewer.setContentProvider(new TreeContentProvider());
		treeViewer.setInput("Element");
//		treeViewer.addDoubleClickListener(new FullPathDisplayListener(labelFullPathDisplay));
		treeViewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		setSize(500, 500);
	}

	class TreeLabelProvider extends LabelProvider{
        public String getText(Object element) {
                return ( "[" + element + "]");
        }

}
	class TreeContentProvider implements ITreeContentProvider{

	        public Object[] getChildren(Object parentElement) {
	                String str = (String)parentElement;
	                String [] children = new String[5];
	                for (int i=0; i<children.length; i++){
	                        children[i] = str + "-"+ i;
	                }
	                return children;
	        }

	        public Object getParent(Object element) {
	                String str = (String)element;
	                int index = str.lastIndexOf("-");
	                return str.substring(0, index);
	        }

	        public boolean hasChildren(Object element) {
	                return true;
	        }

	        public Object[] getElements(Object inputElement) {
	                return getChildren(inputElement);
	        }

	        public void dispose() {
	        }

	        public void inputChanged(Viewer viewer,
	                                 Object oldInput,
	                                 Object newInput) {
	        }

}


	  public static void main (String [] args) {
	    Display display = new Display ();
	    Shell shell = new Shell(display);
	    shell.setText("Table Sample1");
	    shell.setLayout(new GridLayout(1,false));

	    GitResultTable viewer = new GitResultTable(shell, SWT.NONE);
	    viewer.setLayoutData(new GridData(GridData.FILL_BOTH));
	    shell.pack();
	    shell.open();

	    while (!shell.isDisposed ()){
	      if (!display.readAndDispatch ()){
	        display.sleep ();
	      }
	    }
	    display.dispose ();
	  }
}
