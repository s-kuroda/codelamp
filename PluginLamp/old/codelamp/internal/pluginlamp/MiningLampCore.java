package codelamp.internal.pluginlamp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.widgets.TableItem;

import codelamp.mininglamp.widget.MiningLampUI;
import codelamp.pluginlamp.internal.CommitTrace;
import codelamp.util.EpocDateTime;
import codelamp.util.HexId;

public class MiningLampCore {

	private final MiningLampUI ui;

	public MiningLampCore(MiningLampUI ui) {
		this.ui = ui;
	}

	private static final int ID_LENGTH = 9;
	private Path project;
	private class SignedCommit {
		private final long id;
		private final RevCommit commit;
		private List<Ref> tag = new ArrayList<>();
		private boolean faultySign = false;
		private boolean accurateSign = false;

		public SignedCommit(RevCommit commit) {
			this.commit = commit;
			String id = CommitTrace.limitIdLength(commit.getId(), ID_LENGTH);
			this.id = HexId.resizeId(id);
		}
		public boolean addTag(Ref tag) {
			for (Ref entry : this.tag) {
				if (tag.equals(entry)) {
					return false;
				}
			}
			this.tag.add(tag);
			return true;
		}
	}

	private List<SignedCommit> commitList = new ArrayList<>();

	private static final List<String> FAULTY_TAGS = Arrays.asList("BUG");
	private static final List<String> ACCURATE_TAG = Arrays.asList("INNOCENT", "FIX");
	private enum SIGN {Y, N, YN};

	private static final String DEFAULT_FAULTY_PATH = "BUG";
	private static final String DEFAULT_ACCURATE_PATH = "NON-BUG";

	public boolean setProject(String projectPath) {
		ui.setEnabledAllBtn(false);
		if (projectPath == null || projectPath.equals("")) {
			return false;
		}
		project = Paths.get(projectPath).toAbsolutePath();
		if (project == null || !Files.exists(project) || !Files.isDirectory(project)) {
			return false;
		}
		project = CommitTrace.setGitDir(projectPath);
		if (project == null) {
			return false;
		} else if (project.getFileName().toString().equals(CommitTrace.GIT_DIR)){
			project = project.getParent();
		}
		List<RevCommit> commits = CommitTrace.getAllComit(project.toString());
		if (commits == null || commits.isEmpty()) {
			return false;
		}
		commitList.clear();
		commits.forEach(e -> commitList.add(new SignedCommit(e)));
		commits.clear();
		List<Ref> tags = CommitTrace.getAllTag(project.toString());
		if (tags != null && !tags.isEmpty()) {
			tags.forEach(e -> {
				if (e.getPeeledObjectId() != null) {
					String tagId = CommitTrace.limitIdLength(e.getPeeledObjectId(), ID_LENGTH);
					commitList.stream().filter(f -> f.id == HexId.resizeId(tagId))
						.forEach(f -> f.addTag(e));
				}
			});
		}
		autoLabel(commitList);
		if (commitList == null || commitList.isEmpty()) {
			return false;
		} else {
			commitList.forEach(e -> {
                List<String> itemData = new ArrayList<>();
                itemData.add("");
                if (e.faultySign == true && e.accurateSign == false) {
                	itemData.add(SIGN.Y.toString());
                } else if (e.faultySign == false && e.accurateSign == true) {
                	itemData.add(SIGN.N.toString());
                } else if (e.faultySign == true && e.accurateSign == true) {
                	itemData.add(SIGN.YN.toString());
                } else {
                	itemData.add("");
                }
//                itemData.add(Long.toHexString(e.id));
                itemData.add(CommitTrace.limitIdLength(e.commit.getId()));
                itemData.add(EpocDateTime.convertEpocToString(e.commit.getCommitTime()));
                if (e.tag != null && !e.tag.isEmpty()) {
                	StringBuffer str = new StringBuffer();
                	e.tag.forEach(f -> str.append(CommitTrace.getTagNameFromRef(f)));
                	itemData.add(String.join(" , ", str));
                } else {
                	itemData.add("");
                }
                itemData.add(e.commit.getShortMessage());
                ui.addInput(itemData, e.id);
        	});
			ui.setPath(Paths.get(project.toString(), DEFAULT_FAULTY_PATH + MiningLampUI.FAULTY_EXTENSION).toString(), true);
			ui.setPath(Paths.get(project.toString(), DEFAULT_ACCURATE_PATH + MiningLampUI.ACCURATE_EXTENSION).toString(), false);
			ui.setEnabledAllBtn(true);
		}
		return true;
	}

	private void autoLabel(List<SignedCommit> commitList) {
		commitList.stream().filter(e -> !e.tag.isEmpty()).forEach(e -> {
			for (String sign : FAULTY_TAGS) {
				if (e.faultySign) {
					break;
				}
				for (Ref tag : e.tag) {
					if (CommitTrace.getTagNameFromRef(tag).startsWith(sign)) {
						e.faultySign = true;
						break;
					}
				}
			}
			for (String sign : ACCURATE_TAG) {
				if (e.accurateSign) {
					break;
				}
				for (Ref tag : e.tag) {
					if (CommitTrace.getTagNameFromRef(tag).startsWith(sign)) {
						e.accurateSign = true;
						break;
					}
				}
			}
		});
	}

	public void setSign(TableItem item, boolean isBug) {
		if (item.getData() instanceof Long) {
			Long id = (Long) item.getData();
			commitList.stream().filter(e -> e.id == id).forEach(e -> {
				if (isBug) {
					e.faultySign = true;
				} else {
					e.accurateSign = true;
				}
                if (e.faultySign == true && e.accurateSign == false) {
                	item.setText(1, SIGN.Y.toString());
                } else if (e.faultySign == false && e.accurateSign == true) {
                	item.setText(1, SIGN.N.toString());
                } else if (e.faultySign == true && e.accurateSign == true) {
                	item.setText(1, SIGN.YN.toString());
                }
			});
		}
	}
	public void clearSign(TableItem item) {
		if (item.getData() instanceof Long) {
			Long id = (Long) item.getData();
			commitList.stream().filter(e -> e.id == id).forEach(e -> {
				e.faultySign = false;
				e.accurateSign = false;
                item.setText(1, "");
			});
		}
	}

	public void launch() {
		ui.setEnabledAllBtn(false);
		List<RevCommit> faultyList = new ArrayList<>();
		List<RevCommit> accurateList = new ArrayList<>();
		commitList.forEach(e -> {
			if (e.faultySign && !e.accurateSign) {
				faultyList.add(e.commit);
			} else if (!e.faultySign && e.accurateSign) {
				accurateList.add(e.commit);
			}
		});
		Thread learnFaulty = new DiffOutputThread(faultyList, project.toString(), ui.getLanguage(), ui.getPath(true));
		Thread learnAccurate = new DiffOutputThread(accurateList, project.toString(), ui.getLanguage(), ui.getPath(false));
		learnFaulty.start();
		learnAccurate.start();
	}



	public class DiffOutputThread extends Thread {
		private boolean kill = false;
		private final List<RevCommit> commitList;
		private final List<String> extensions;
		private final String repoPath;
		private final Path output;
		public DiffOutputThread(List<RevCommit> commitList, String repoPath, List<String> extensions, Path output) {
			this.commitList = commitList;
			this.repoPath = repoPath;
			this.extensions = extensions;
			this.output = output;
		}

//		private void setVisibleProgress(boolean visible) {
//			getDisplay().asyncExec(new Runnable() {
//				public void run() {
//					progress.setVisible(visible);
//				}
//			});
//		}

		public void run() {
			if (commitList == null || commitList.isEmpty()) {
				return;
			}
			List<DiffEntry> diffEntries = new ArrayList<>();
			for (RevCommit commit : commitList) {
				if (kill) {
					return;
				}
				List<DiffEntry> tmp = CommitTrace.getDiffEntry(commit, repoPath);
				if (tmp != null && !tmp.isEmpty()) {
					diffEntries.addAll(tmp);
					tmp.clear();
				}
			}
			if (diffEntries == null || diffEntries.isEmpty()) {
				return;
			}
			List<DiffEntry> trimedEntries = new ArrayList<>();
			extensions.forEach(e -> {
				List<DiffEntry> tmp = CommitTrace.trimExtraExtensionFiles(diffEntries, e);
				if (tmp != null && !tmp.isEmpty()) {
					trimedEntries.addAll(tmp);
					tmp.clear();
				}
			});
			if (trimedEntries == null || trimedEntries.isEmpty()) {
				return;
			}
//			System.out.println(repoPath + output.toAbsolutePath().toString());
			if (!Files.exists(output)) {
				try {
					Files.createFile(output);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			CommitTrace.outputDiffToText(trimedEntries, repoPath, output.toString());
			GitShowEdit.TrimDiffText(output, true);
			System.out.println("END : " + output.toString());
		}

		public void kill() {
		    this.kill = true;
		}
	}
}
