package codelamp.internal.pluginlamp;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.revwalk.RevCommit;

import codelamp.util.EpocDateTime;

public class GitDiffSet {

	private final RevCommit fromCommit;
	public RevCommit getFromCommit() {
		return this.fromCommit;
	}

	private final RevCommit toCommit;
	public RevCommit getToCommit() {
		return this.toCommit;
	}

	public GitDiffSet(RevCommit fromCommit, RevCommit toCommit) {
		this.fromCommit = fromCommit;
		this.toCommit = toCommit;
	}

	public ObjectId getCommitId() {
		return  this.toCommit.getId();
	}
	public LocalDateTime getCommitTime() {
		return EpocDateTime.convertEpocToDateTime(toCommit.getCommitTime());
	}
	public String getShorMessage() {
		return toCommit.getShortMessage();
	}

	private List<DiffEntry> diffList;
	private HashMap<DiffEntry, Double> diffValMap = new HashMap<>();

	public void setDiffList(List<DiffEntry> diffList) {
		this.diffList = diffList;
		this.diffList.forEach(e -> diffValMap.put(e, 0.0));
	}
	public List<DiffEntry> getDiffList() {
		return this.diffList;
	}

	public void diffsClassified(HashMap<DiffEntry, Double> valMap) {
		valMap.forEach((k, v) -> diffValMap.replace(k, v));
	}
}
