package codelamp.internal.pluginlamp;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

public class CommitDiff {

	private final String GIT_DIR = ".git";
	private Path getGitDir(Path path) {
		if (path == null || !Files.exists(path) || !Files.isDirectory(path)) {
			return null;
		}
		ArrayList<Path> pathList = new ArrayList<>();
		try(Stream<Path> paths = Files.list(path)){
			paths.map(Path::getFileName).filter(e -> e.toString().equals(GIT_DIR))
				.filter(e -> Files.isDirectory(e)).forEach(e -> pathList.add(e.toAbsolutePath()));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if (pathList == null || pathList.isEmpty()) {
			return null;
		} else if (Files.exists(pathList.get(0)) && Files.isDirectory(pathList.get(0))){
			return pathList.get(0);
		} else {
			return null;
		}
	}
	private Path getGitDir(String path) {
		return getGitDir(Paths.get(path));
	}

	private Path setGitDir(String path) {
		if (path == null || !Files.exists(Paths.get(path))) {
			return null;
		} else if (Paths.get(path).getFileName().toString().equals(GIT_DIR)) {
			return Paths.get(path);
		} else {
			Path repositoryDir = getGitDir(path);
			if (repositoryDir == null) {
				return null;
			}
			return repositoryDir;
		}
	}

	Repository repository;
	public CommitDiff(String path) {
		Path repositoryDir = setGitDir(path);
		if (repositoryDir == null) {
			return;
		}
		try (Repository repository = new FileRepositoryBuilder().setGitDir(repositoryDir.toFile()).build()) {
			this.repository = repository;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<DiffEntry> getDiffEntry(RevCommit commit) {
		if (repository == null) {
			return null;
		}
		List<DiffEntry> diffEntries = new ArrayList<>();
		Arrays.stream(commit.getParents()).forEach(entry -> {
	        RevTree fromTree = entry.getTree();
	        RevTree toTree = commit.getTree();
	    	try (ObjectReader reader = this.repository.newObjectReader()) {
	    		CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
	    		oldTreeIter.reset(reader, fromTree);
	    		CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
	    		newTreeIter.reset(reader, toTree);
	    		try (Git git = new Git(repository)) {
	    			List<DiffEntry> diffs = git.diff().setNewTree(newTreeIter).setOldTree(oldTreeIter).call();
	    			diffEntries.addAll(diffs);
	    		} catch (GitAPIException e) {
	    			e.printStackTrace();
	    		} finally {
	    		}
	    	} catch (IncorrectObjectTypeException e) {
	    		e.printStackTrace();
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	}
	    });
		if (diffEntries == null || diffEntries.isEmpty()) {
			return null;
		} else {
			return diffEntries;
		}
	}

	public List<DiffEntry> getMargedDiffEntry(List<RevCommit> commits) {
		if (repository == null) {
			return null;
		}
		List<DiffEntry> diffEntries = new ArrayList<>();
		for (RevCommit commit : commits) {
			Arrays.stream(commit.getParents()).forEach(entry -> {
		        RevTree fromTree = entry.getTree();
		        RevTree toTree = commit.getTree();
		    	try (ObjectReader reader = this.repository.newObjectReader()) {
		    		CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
		    		oldTreeIter.reset(reader, fromTree);
		    		CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
		    		newTreeIter.reset(reader, toTree);
		    		try (Git git = new Git(repository)) {
		    			List<DiffEntry> diffs = git.diff().setNewTree(newTreeIter).setOldTree(oldTreeIter).call();
		    			diffEntries.addAll(diffs);
		    		} catch (GitAPIException e) {
		    			e.printStackTrace();
		    		} finally {
		    		}
		    	} catch (IncorrectObjectTypeException e) {
		    		e.printStackTrace();
		    	} catch (IOException e) {
		    		e.printStackTrace();
		    	}
		    });
		}
		if (diffEntries == null || diffEntries.isEmpty()) {
			return null;
		} else {
			return diffEntries;
		}
	}

	public List<DiffEntry> extensionPicUp(List<DiffEntry> diffEntries, String extension) {
		List<DiffEntry> list = new ArrayList<>();
		if (extension == null || extension.equals("")) {
			return diffEntries;
		}
		diffEntries.forEach(e -> {
			if (e.getNewPath().endsWith(extension)) {
				list.add(e);
			}
		});
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}

	public boolean outputDiff(List<DiffEntry> diffEntries, OutputStream outStream) {
		try (DiffFormatter diffFormatter = new DiffFormatter(outStream)) {
			diffFormatter.setRepository(repository);
			diffFormatter.format(diffEntries);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean outputDiffToText(List<DiffEntry> diffEntries, String path) {
		if (path == null || path.equals("") || diffEntries == null || diffEntries.isEmpty()) {
			return false;
		}
		Path outPut = Paths.get(path).toAbsolutePath();
		if (!Files.exists(outPut)) {
			try {
				Files.createFile(outPut);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		try (OutputStream outStream = Files.newOutputStream(outPut)) {
			try (DiffFormatter diffFormatter = new DiffFormatter(outStream)) {
				diffFormatter.setRepository(repository);
				diffFormatter.format(diffEntries);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
