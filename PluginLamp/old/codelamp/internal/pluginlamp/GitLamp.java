package codelamp.internal.pluginlamp;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import codelamp.pio.Language;
import codelamp.pluginlamp.internal.CRM;
import codelamp.pluginlamp.widget.GitResultTreeTable;
import codelamp.pluginlamp.widget.faultpronefiltering.SrcResultTreeTable;

public class GitLamp {

	private final GitResultTreeTable gitTable;
	private final SrcResultTreeTable srcTable;
	public GitLamp(SrcResultTreeTable srcTable, GitResultTreeTable gitTable) {
		this.srcTable = srcTable;
		this.gitTable = gitTable;
	}

	private Path faultyDataBase;
	private Path accurateDataBase;
	private Language language = Language.JAVA;
	public void setDataBase(Path faultyDB, Path accurateDB) {
		this.faultyDataBase = faultyDB;
		this.accurateDataBase = accurateDB;
	}
	public void setDataBase(Path faultyDB, Path accurateDB, Language language) {
		this.faultyDataBase = faultyDB;
		this.accurateDataBase = accurateDB;
		this.language = language;
	}
	public boolean existDBs() {
		if (this.faultyDataBase == null || this.accurateDataBase == null) {
			return false;
		} else if (!Files.exists(faultyDataBase) || !Files.exists(accurateDataBase)) {
			return false;
		} else {
			return true;
		}
	}

	private Path targetDir;
	private GitCommits git;
	private final void setDirectory(String dir) {
		if (dir == null) {
			throw new NullPointerException();
		} else if (this.targetDir.equals(Paths.get(dir))) {
			return;
		}
		this.targetDir = null;
		this.targetDir = Paths.get(dir);
		try (DirectoryStream<Path> filelist = Files.newDirectoryStream(targetDir)) {
			filelist.forEach(e -> {
				if (e.getFileName().toString().equals(".git")) {
					try {
						git = new GitCommits(dir);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});
		} catch (IOException e1) {
			e1.printStackTrace();
		}
//		if () {
//			// Thread
//			RevCommit c;
//		}
	}


    public List<DiffEntry> trim(List<DiffEntry> diffList) {
		ArrayList<DiffEntry> array = new ArrayList<>();
		if (language == null) {
			return diffList;
		}
		for (String extension : language.getExtension()) {
			for (DiffEntry entry : diffList) {
				if (entry.getNewPath().endsWith(extension)) {
					array.add(entry);
				}
			}
		}
		return array;
	}

    private class GitFilterThread extends Thread {

        private boolean kill = false;
    	private ObjectId headCommitId = null;
    	private LocalDateTime headCommitTime = null;

        private LinkedList<GitDiffSet> learnFaultyList = new LinkedList<>();
        private LinkedList<GitDiffSet> learnAccurateList = new LinkedList<>();
        private LinkedList<GitDiffSet> classifyList = new LinkedList<>();


        private boolean learn(GitDiffSet commit, boolean isAccurate) {
        	for (DiffEntry entry : commit.getDiffList()) {
                if (this.kill) {
                	return false;
                }
                Path tmp = git.createDiffOfFile(entry);
                double val = 0;
                try {
                    val = CRM.learn(tmp.toFile(), isAccurate, new FilterGroup(faultyDataBase.toFile(), accurateDataBase.toFile(), language));
                } catch (NullPointerException | IOException e) {
                    e.printStackTrace();
                }
        	}
        	return true;
        }

        public GitDiffSet classify(GitDiffSet commit) {
        	HashMap<DiffEntry, Double> map = new HashMap<>();
        	for (DiffEntry entry : commit.getDiffList()) {
                if (this.kill) {
                	return null;
                }
                Path tmp = git.createDiffOfFile(entry);
                double val = 0;
                try {
                    val = CRM.classify(tmp.toFile(), new FilterGroup(faultyDataBase.toFile(), accurateDataBase.toFile(), language));
                    map.put(entry, val);
                } catch (NullPointerException | IOException e) {
                    e.printStackTrace();
                }
        	}
        	if (map != null || !map.isEmpty()) {
        		commit.diffsClassified(map);
        	}
        	return commit;
        }


        public void run() {
        	if (!existDBs()) {
        		return;
        	}
            while (!kill) {
                if (!this.learnFaultyList.isEmpty()) {
                    GitDiffSet commit = learnFaultyList.removeFirst();
                    this.learn(commit, false);
                    if (this.kill) {
                    	return;
                    }
                }
                if (!this.learnAccurateList.isEmpty()) {
                    GitDiffSet commit = learnAccurateList.removeFirst();
                    this.learn(commit, true);
                    if (this.kill) {
                    	return;
                    }
                }
                if (!this.classifyList.isEmpty()) {
                    GitDiffSet commit = classifyList.removeFirst();
                    this.classify(commit);
                    if (this.kill) {
                    	return;
                    }
                }
                if (this.headCommitId == null || !headCommitId.equals(git.getHead())) {
                    this.headCommitId = git.getHead();
                    GitDiffSet diffSet = git.getHeadCommit();
                    diffSet.setDiffList(trim(git.getDiffEntries(diffSet)));
                    diffSet = this.classify(diffSet);
                }
            }

        }

        public void kill() {
            this.kill = true;
        }
    }

    private class FaultyButtonActionb implements SelectionListener{
		@Override
		public void widgetSelected(SelectionEvent e) {
			gitTable.getSelected();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
    }
    private class NonFaultyButtonActionb implements SelectionListener{
		@Override
		public void widgetSelected(SelectionEvent e) {
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
    }
    private class ClassifyButtonActionb implements SelectionListener{
		@Override
		public void widgetSelected(SelectionEvent e) {
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
    }


	public static void main(String[] args) {
	}

}
