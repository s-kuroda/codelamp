package codelamp.internal.pluginlamp;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import codelamp.pio.TargetFile;

public class FileListCapture {

	private FileListCapture(){
	}


	public static final ArrayList<File> getProjectDir(File root) {
		ArrayList<File> list = new ArrayList<File>();
		for (File file : root.listFiles()) {
			if (file.isDirectory() && ! file.getName().startsWith(".")) {
				list.add(new File(root.getAbsolutePath(), file.getName()));
			}
		}
		return list;
	}

    public static final ArrayList<File> getChildFileList(File parent) {
        ArrayList<File> list = new ArrayList<File>();
        for (File file : parent.listFiles()) {
            list.add(file);
            if (file.isDirectory()) {
                list.addAll(getChildFileList(file));
            }
        }
        return list;
    }
    public static final ArrayList<TargetFile> getChildTargetFileList(File parent) {
        ArrayList<TargetFile> list = new ArrayList<TargetFile>();
        for (File file : parent.listFiles()) {
            if (file.isDirectory()) {
                list.addAll(getChildTargetFileList(file));
            } else {
                list.add(new TargetFile(file));
            }
        }
        return list;
    }

    public static final ArrayList<File> getSpecificExtensionFileList(File parent, String extension) {
        ArrayList<File> list = new ArrayList<File>();
        for (File file : parent.listFiles()) {
            if (file.isDirectory()) {
                list.addAll(getSpecificExtensionFileList(file, extension));
            } else if (file.getName().endsWith(extension)) {
                list.add(file);
            }
        }
        return list;
    }
    
    public static final ArrayList<TargetFile> getChildTargetFileList(File parent, String extension) {
        ArrayList<TargetFile> list = new ArrayList<>();
        for (File file : parent.listFiles()) {
            if (file.isDirectory()) {
                list.addAll(getChildTargetFileList(file, extension));
            } else if (file.getName().endsWith(extension)) {
                list.add(new TargetFile(file));
            }
        }
        return list;
    }

	public static final boolean isFileAddOrRemove(ArrayList<File> previousList, ArrayList<File> currentList) {
		return (previousList.size() != currentList.size()) ? true: false;
	}

	public static final boolean isFileModify(HashMap<File,Date> previousList, HashMap<File,Date> currentList) {
		for (Entry<File, Date> file: currentList.entrySet()) {
			if (file.getValue().after(previousList.get(file.getKey()))) {
				return true;
			}
		}
		return false;
	}

	public static final ArrayList<File> getModifyFile(HashMap<File,Date> previousList, HashMap<File,Date> currentList) {
		ArrayList<File> list = new ArrayList<File>();
		for (Entry<File, Date> file: currentList.entrySet()) {
			if (file.getValue().after(previousList.get(file.getKey()))) {
				list.add(file.getKey());
			}
		}
		return list;
	}

	public static final HashMap<File, Long> convertFileListToFileDateMap(ArrayList<File> list) {
		HashMap<File, Long> map = new HashMap<File, Long>();
		for (File file: list) {
			map.put(file, (file.lastModified()));
		}
		return map;
	}


	public static void main(String[] args) {
		String root = "./";
		File rootFile = new File(root);
		System.out.println(rootFile.getAbsolutePath());
		ArrayList<File> list = new ArrayList<File>();
//		FileListCapture filelist = new FileListCapture(root);
//		list.addAll(getChildFileList(rootFile));
//		for (File file : list) {
//			System.out.println(file);
//		}
		list.addAll(getSpecificExtensionFileList(rootFile, ".java"));
		for (File file : list) {
			System.out.println(file.getAbsolutePath().substring(rootFile.getAbsolutePath().length()));
		}
		String str = "./";
		File f1 = new File(str);
		File f2 = new File("./");
		if (f1.equals(f2)) System.out.println("SAME");
		str += "src";
		if (f1.equals(f2)) System.out.println("SAME");
	}

}
