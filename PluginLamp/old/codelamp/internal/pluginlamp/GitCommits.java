package codelamp.internal.pluginlamp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.DepthWalk.RevWalk;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

public class GitCommits {

	private Repository repository;

	public GitCommits(String dir) {
		if (dir == null) {
			throw new NullPointerException();
		}
		Path targetDir = Paths.get(dir);
		try (DirectoryStream<Path> filelist = Files.newDirectoryStream(targetDir)) {
			filelist.forEach(e -> {
				if (e.getFileName().toString().equals(".git")) {
					try {
						repository = new FileRepositoryBuilder().setGitDir(e.toFile()).build();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public ObjectId getHead() {
		try {
			ObjectId headId = repository.resolve(Constants.HEAD);
			return headId;
		} catch (RevisionSyntaxException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ObjectId getHeadCaret() {
		try {
			ObjectId caretId = repository.resolve("HEAD^");
			return caretId;
		} catch (RevisionSyntaxException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public GitDiffSet getHeadCommit() {
		GitDiffSet diffSet;
        try (RevWalk revWalk = new RevWalk(repository, 100)) {
        	RevCommit headCommit = revWalk.parseCommit(getHead());
        	RevCommit caretCommit = revWalk.parseCommit(getHeadCaret());
        	diffSet = new GitDiffSet(caretCommit, headCommit);
        	return diffSet;
        } catch (IOException e) {
			e.printStackTrace();
		}
        return null;
	}

    public List<DiffEntry> getDiffEntries(GitDiffSet gitDiffSet) {
        RevTree fromTree = gitDiffSet.getFromCommit().getTree();
        RevTree toTree = gitDiffSet.getToCommit().getTree();
    	try (ObjectReader reader = repository.newObjectReader()) {
    		CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
    		oldTreeIter.reset(reader, fromTree);
    		CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
    		newTreeIter.reset(reader, toTree);
    		try (Git git = new Git(repository)) {
    			ArrayList<DiffEntry> diffs = (ArrayList<DiffEntry>) git.diff().setNewTree(newTreeIter).setOldTree(oldTreeIter).call();
    			if (diffs == null || diffs.isEmpty()) {
    				return null;
    			} else {
    				return diffs;
    			}
    		} catch (GitAPIException e) {
    			e.printStackTrace();
    		}
    	} catch (IncorrectObjectTypeException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
		return null;
	}

    public Path createDiffOfFile(DiffEntry diff) {
    	Path tmp = null;
		try {
			tmp = Files.createTempFile("TmpDiffFile", ".txt");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	try (DiffFormatter diffFormatter = new DiffFormatter(new FileOutputStream(tmp.toFile()))){
	        diffFormatter.setRepository(this.repository);
	        diffFormatter.format(diff);
	        diffFormatter.close();
	        tmp = this.cutDiffFile(tmp);
	        return tmp;
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
    }

	private Path cutDiffFile(Path path) {
		if (path == null || Files.exists(path)) {
			return null;
		}
		BufferedReader buffReader = null;
		BufferedWriter buffWriter = null;
		try {
			buffReader =  Files.newBufferedReader(path, Charset.forName("UTF-8"));
	    	Path tmp = Files.createTempFile("TmpTrimedDiff", ".txt");
	    	buffWriter =  Files.newBufferedWriter(path, Charset.forName("UTF-8"));
	    	String str;
	    	while((str = buffReader.readLine()) != null){
	    		if (str.startsWith("+")){
	    			System.out.println(str);
	    			buffWriter.write(str.substring(1));
	    		}
			}
			buffWriter.flush();
			buffReader.close();
			Files.delete(path);
			buffWriter.close();
			return tmp;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
	}

}
