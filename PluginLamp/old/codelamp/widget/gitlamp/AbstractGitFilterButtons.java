package codelamp.widget.gitlamp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public abstract class AbstractGitFilterButtons extends Composite implements SelectionListener {
    
    private Button asFaultyButton;
    private Button asAccurateButton;
    private Button classifyButton;
    
    public AbstractGitFilterButtons(Composite parent, int style) {
        super(parent, style);
        this.setLayout(new FillLayout(SWT.HORIZONTAL));
        asFaultyButton = new Button(this,SWT.PUSH);
        asFaultyButton.setText("Learn as Faulty");
        asFaultyButton.addSelectionListener(this);
        asAccurateButton = new Button(this,SWT.PUSH);
        asAccurateButton.setText("Learn as Accurate");
        asAccurateButton.addSelectionListener(this);
        Label empty = new Label(this, SWT.NONE);
        empty.setVisible(false);
        classifyButton = new Button(this,SWT.PUSH);
        classifyButton.setText("Classify");
        classifyButton.addSelectionListener(this);
        this.pack();
    }
    
    @Override
    public void widgetDefaultSelected(SelectionEvent event) {
    }
    
    @Override
    public void widgetSelected(SelectionEvent event) {
        Object obj = event.getSource();
        if (obj == asFaultyButton) {
            faultyButtonAction();
        } else if (obj == asAccurateButton) {
            accurateButtonAction();
        } else if (obj == classifyButton) {
            classifyButtonAction();
        }
    }

    public abstract void accurateButtonAction();
    public abstract void faultyButtonAction();
    public abstract void classifyButtonAction();
}
