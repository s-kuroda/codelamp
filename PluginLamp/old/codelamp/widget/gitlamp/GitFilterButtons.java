package codelamp.widget.gitlamp;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

public class GitFilterButtons extends Composite implements SelectionListener {
    
    private Button asBugButton;
    private Button asInnocentButton;
    private Button classifyButton;
    
    public GitFilterButtons(Composite parent, int style) {
        super(parent, style);
        this.setLayout(new FillLayout(SWT.HORIZONTAL));
        asBugButton = new Button(this,SWT.PUSH);
        asBugButton.setText("Learn as Bug");
        asBugButton.addSelectionListener(this);
        asInnocentButton = new Button(this,SWT.PUSH);
        asInnocentButton.setText("Learn as NotBug");
        asInnocentButton.addSelectionListener(this);
        Label empty = new Label(this, SWT.NONE);
        empty.setVisible(false);
        classifyButton = new Button(this,SWT.PUSH);
        classifyButton.setText("Classify");
        classifyButton.addSelectionListener(this);
        this.pack();
    }
    
    @Override
    public void widgetDefaultSelected(SelectionEvent event) {
    }
    
    @Override
    public void widgetSelected(SelectionEvent event) {
        Object obj = event.getSource();
        if (obj == asBugButton) {
            bugButtonAction();
        } else if (obj == asInnocentButton) {
            innocentButtonAction();
        }
        reflectTo();
    }
    
    private void innocentButtonAction() {
        System.out.println("INNOCENT BUTTON ACTION");
    }
    
    protected void bugButtonAction() {
        System.out.println("BUG BUTTON ACTION");
    }
    
    public void reflectTo(){
        System.out.println("REFLECT ACTION");
    }
    
    public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setText("CodeLamp --- Fault-Prone Filtering Plugin");
    shell.setLayout(new GridLayout(1,false));
    Menu menu = new Menu(shell, SWT.BAR);
    shell.setMenuBar(menu);
    MenuItem configButton = new MenuItem(menu, SWT.PUSH);
    configButton.setText("Setting");
    configButton.addSelectionListener(new SelectionAdapter() {
        public void widgetSelected(SelectionEvent e){
            System.out.println("MENU BUTTON SELECTED");
            System.out.println(shell.getSize());
        }
    });
    ArrayList<File> tmp = new ArrayList<>();
    tmp.add(new File("first"));
    tmp.add(new File("second"));
    GitFilterButtons composite = new GitFilterButtons(shell, SWT.BORDER);
    composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    shell.setSize(800,450);
    shell.open();
    while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                    display.sleep();
            }
    }
    display.dispose();
}
}
