package codelamp.widget.creator;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import codelamp.widget.common.AbstractProjectSelector;

public class CodeLampCreator extends Composite {

    GitProjectSelector selector;
    CreatorView view;
    
    public CodeLampCreator(Composite parent, int style) {
        super(parent, style);
        this.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.setLayout(new GridLayout(1,false));
        selector = new GitProjectSelector(this, SWT.BORDER);
        selector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        view = new CreatorView(this, SWT.BORDER);
        view.setLayoutData(new GridData(GridData.FILL_BOTH));
        DropTarget target = new DropTarget(this,DND.DROP_DEFAULT|DND.DROP_COPY);
        FileTransfer transfer = FileTransfer.getInstance();
        Transfer[] types = new Transfer[]{transfer};
        target.setTransfer(types);
        target.addDropListener(new DropTargetAdapter() {
            public void dragEnter(DropTargetEvent evt){
            evt.detail = DND.DROP_COPY;
            }
            public void drop(DropTargetEvent evt){
                String[] files = (String[])evt.data;
                CodeLampCreator.this.selector.addProject(files[0]);
            }
        });
        this.pack();
    }
    
    private final class GitProjectSelector extends AbstractProjectSelector {
        public GitProjectSelector(Composite parent, int style) {
            super(parent, style);
        }

        @Override
        public void reflect(File targetProject) {
            if (targetProject == null || !targetProject.exists()) { 
                return;
            }
            CodeLampCreator.this.view.setProject(targetProject);
        }
    }
    
    private final void setProject(String path) {
        File file = new File(path);
        if (file == null || !file.exists()) {
            return;
        } else {
            CodeLampCreator.this.selector.addProject(file.getAbsolutePath());
        }
    }
    
    
    public static void main(String[] args) {
        Display display = Display.getDefault();
        Shell shell = new Shell();
        shell.setLayout(new GridLayout(1, false));
        CodeLampCreator creator = new CodeLampCreator(shell,SWT.NONE);
        shell.pack();
        shell.open();
        shell.setSize(600,500);
        if (args != null) {
            if (1 < args.length) {
                creator.setProject(args[0]);
            }
        }
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        creator.dispose();
        display.dispose();
    }

    public void dispose() {
        this.view.dispose();
        this.selector.dispose();
        super.dispose();
    }

    
}
