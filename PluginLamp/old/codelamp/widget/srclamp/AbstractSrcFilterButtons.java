package codelamp.widget.srclamp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public abstract class AbstractSrcFilterButtons
		extends Composite implements SelectionListener {

    private Button excludeButton;
    private Button faultyButton;
    private Button accurateButton;
    private Button classifyButton;
    private Button learnButton;

	public AbstractSrcFilterButtons(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(2,false));
        ScrolledComposite scroll = new ScrolledComposite(this, SWT.NONE | SWT.V_SCROLL);
        scroll.setLayoutData(new GridData(GridData.FILL_BOTH));
        Composite composite = new Composite(scroll, SWT.NONE);
        composite.setLayout(new GridLayout(2,false));
        GridData data = new GridData(GridData.FILL_HORIZONTAL);
        GridData wideData = new GridData(GridData.FILL_HORIZONTAL);
        wideData.horizontalSpan = 2;
        faultyButton = new Button(composite, SWT.PUSH);
        faultyButton.setText("Faulty");
        faultyButton.setLayoutData(data);
        faultyButton.addSelectionListener(this);
        accurateButton = new Button(composite, SWT.PUSH);
        accurateButton.setText("Accurate");
        accurateButton.setLayoutData(data);
        accurateButton.addSelectionListener(this);
        excludeButton = new Button(composite, SWT.PUSH);
        excludeButton.setText("Exclude");
        excludeButton.setLayoutData(wideData);
        excludeButton.addSelectionListener(this);
        classifyButton = new Button(composite, SWT.PUSH);
        classifyButton.setText("Classify");
        classifyButton.setLayoutData(wideData);
        classifyButton.addSelectionListener(this);
        learnButton = new Button(composite, SWT.PUSH);
        learnButton.setText("Learn");
        learnButton.setLayoutData(wideData);
        learnButton.addSelectionListener(this);
        composite.pack();
        scroll.setContent(composite);
        scroll.setMinSize(composite.getSize().x, composite.getSize().y);
        scroll.setExpandHorizontal(true);
        scroll.setExpandVertical(true);
        scroll.pack();
        this.pack();
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		Object obj = event.getSource();
		if (obj == faultyButton) {
            faultyButtonAction();
        } else if (obj == accurateButton) {
        	accurateButtonAction();
        } else if (obj == excludeButton) {
			excludeButtonAction();
		} else  if (obj == classifyButton) {
            classifyButtonAction();
        } else  if (obj == learnButton) {
        	learnButtonAction();
        }
	}

	public void setBtnsEnabled(boolean enabled) {
	    excludeButton.setEnabled(enabled);
	    faultyButton.setEnabled(enabled);
	    accurateButton.setEnabled(enabled);
	    classifyButton.setEnabled(enabled);
	    learnButton.setEnabled(enabled);
	}
	
	protected abstract void faultyButtonAction();
	protected abstract void accurateButtonAction();
	protected abstract void excludeButtonAction();
	protected abstract void classifyButtonAction();
	protected abstract void learnButtonAction();

	@Override
	public void dispose() {
        excludeButton.dispose();
        faultyButton.dispose();
        accurateButton.dispose();
        classifyButton.dispose();
        learnButton.dispose();
        super.dispose();
	}
}
