package codelamp.widget.config;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import codelamp.internal.pluginlamp.FilterGroup;
import codelamp.widget.SrcGitLamp;

public class FilterTab extends Composite {
    
    private ArrayList<FilterGroup> filterList = null;
    public void setFilterList(ArrayList<FilterGroup> filters) {
        this.filterList = filters;
        this.reflectRemoveButton();
    }
    public void addFilter(FilterGroup filter) {
        if (!this.filterList.contains(filter)) {
            this.filterList.add(filter);
        }
    }
    
//    private FilterGroup targetFilter;
//    public void setTargetFilter(FilterGroup targetFilter) {
//        this.targetFilter = targetFilter;
//    }

    private final SrcGitLamp parentWidget;
    
	public FilterTab(Composite parent, int style, SrcGitLamp parentWidget) {
		super(parent, style);
		this.parentWidget = parentWidget;
		this.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.setLayout(new GridLayout(2,false));
        SashForm sash = new SashForm(this, SWT.HORIZONTAL);
        sash.setLayoutData(new GridData(GridData.FILL_BOTH));
		table = new FilterTable(sash, SWT.NONE, this);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
		btns = new FilterButtons(sash, SWT.NONE);
        btns.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.reflectRemoveButton();
        int[] sashWeight = {3, 1};
        sash.setWeights(sashWeight);
        this.setFilterList(this.parentWidget.getAllFilters());
        this.table.setTargetFilter(this.parentWidget.getFilter());
        this.table.setFilterList(this.filterList);
        this.pack();
	}

	private FilterTable table;
	private FilterButtons btns;

	private class FilterButtons extends AbstractFilterButtons {
	    private Shell shell = null;

		public FilterButtons(Composite parent, int style) {
			super(parent, style);
		}

		@Override
		protected void addButtonAction() {
		    if (shell == null || !shell.isDisposed()) {
                shell = new Shell();
                shell.setText("CodeLamp --- New Filter");
                shell.setLayout(new GridLayout(1,false));
                FilterEntryWindow entry = new FilterEntryWindow(shell, SWT.NONE, FilterTab.this);
                entry.setLayoutData(new GridData(GridData.FILL_BOTH));
                shell.setSize(800,450);
                shell.open();
		    }
        }

		@Override
		protected void modifyButtonAction() {
            FilterGroup modif = FilterTab.this.table.getSelectedFilter();
            if (modif != null) {
                Shell shell = new Shell();
                shell.setText("CodeLamp --- Modify Filter");
                shell.setLayout(new GridLayout(1,false));
                FilterEntryWindow entry = new FilterEntryWindow(shell, SWT.NONE, FilterTab.this, modif);
                entry.setLayoutData(new GridData(GridData.FILL_BOTH));
                shell.setSize(800,450);
                shell.open();
            }
        }

		@Override
		protected void removeButtonAction() {
            FilterGroup filter = FilterTab.this.table.getSelectedFilter();
            FilterTab.this.remove(filter);
		}

		@Override
		public void dispose() {
            if (shell != null && !shell.isDisposed()) {
                this.shell.dispose();
            }
		    super.dispose();
		}
	}

    public void reflectRemoveButton() {
        if (this.filterList == null || this.filterList.size() <= 1) {
            FilterTab.this.btns.disableRemove();
        } else {
            FilterTab.this.btns.enableRemove();
        }
    }
    
    public void remove(FilterGroup modify) {
        if (this.filterList != null) {
            this.filterList.remove(modify);
            this.reflectRemoveButton();
            this.table.setFilterList(this.filterList);
            this.parentWidget.setAllFilter(this.filterList);
            this.parentWidget.setProject(this.table.getSelectedFilter());
        }
    }
    
    public void add(FilterGroup newEntry) {
        if (this.filterList != null) {
            this.filterList.add(newEntry);
            this.reflectRemoveButton();
            this.table.setFilterList(this.filterList);
            this.parentWidget.setAllFilter(this.filterList);
            this.parentWidget.setProject(this.table.getSelectedFilter());
        }
    }

    public void setFilter(FilterGroup filter) {
        this.parentWidget.setProject(filter);
    }
    
    @Override
    public void dispose() {
        if (this.parentWidget != null) {
            this.parentWidget.setFilter(this.table.getSelectedFilter());
        }
        this.table.dispose();
        this.btns.dispose();
        super.dispose();
    }
    
    public static void main(String[] args) {
        Display display = Display.getDefault();
        Shell shell = new Shell();
        shell.setLayout(new GridLayout(1, false));
        FilterTab f = new FilterTab(shell,SWT.NONE, null);
        shell.pack();
        shell.open();
        shell.setSize(600,500);
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        f.dispose();
        display.dispose();
    }
}
