package codelamp.widget.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;


public abstract class AbstractFilterButtons extends Composite
		implements SelectionListener {

    protected Button addButton;
    private Button modifyButton;
    private Button removeButton;
	public enum BUTTON_LABEL {ADD, MODIFY, REMOVE}

	public AbstractFilterButtons(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1,false));
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
        addButton = new Button(this, SWT.PUSH);
        addButton.setText("Add");
        addButton.setLayoutData(data);
        addButton.addSelectionListener(this);
        modifyButton = new Button(this, SWT.PUSH);
        modifyButton.setText("Modify");
        modifyButton.setLayoutData(data);
        modifyButton.addSelectionListener(this);
        removeButton = new Button(this, SWT.PUSH);
        removeButton.setText("Remove");
        removeButton.setLayoutData(data);
        removeButton.addSelectionListener(this);
        this.pack();
	}

	public void setButtonDisable(BUTTON_LABEL label){
		switch (label) {
			case ADD:
				addButton.setEnabled(false);
				break;
			case MODIFY:
				modifyButton.setEnabled(false);
				break;
			case REMOVE:
				removeButton.setEnabled(false);
				break;
			default:
				break;
		}
	}
	public void setButtonEnable(BUTTON_LABEL label){
		switch (label) {
			case ADD:
				addButton.setEnabled(true);
				break;
			case MODIFY:
				modifyButton.setEnabled(true);
				break;
			case REMOVE:
				removeButton.setEnabled(true);
				break;
			default:
				break;
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		Object obj=event.getSource();
		if (obj == addButton) {
			addButtonAction();
		} else if (obj == modifyButton) {
			modifyButtonAction();
		} else if (obj == removeButton) {
			removeButtonAction();
		}
	}

	public void disableRemove() {
		this.removeButton.setEnabled(false);
	}
	public void enableRemove() {
		this.removeButton.setEnabled(true);
	}

	abstract protected void addButtonAction();
	abstract protected void modifyButtonAction();
	abstract protected void removeButtonAction();
}
