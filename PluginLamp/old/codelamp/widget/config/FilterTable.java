package codelamp.widget.config;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import codelamp.internal.pluginlamp.FilterGroup;

public class FilterTable extends Composite implements SelectionListener{

	private Table table;
	private Label label;
	private ArrayList<TableColumn> column = new ArrayList<TableColumn>();
	private final FilterTab parentWidget;
	private ArrayList<FilterGroup> filterList = null;
    public void setFilterList(ArrayList<FilterGroup> filters) {
        this.filterList = filters;
        this.setTableItem();
    }
    public void addFilter(FilterGroup filter) {
        if (!this.filterList.contains(filter)) {
            this.filterList.add(filter);
        }
    }

	private FilterGroup targetFilter;
	public void setTargetFilter(FilterGroup targetFilter) {
		this.targetFilter = targetFilter;
	}

    private final String[] COLUMN_TEXT = {"", "Language","Faulty DataBase","Accurate DataBase"};

	public FilterTable(Composite parent, int style, FilterTab parentWidget) {
		super(parent, style);
		this.parentWidget = parentWidget;
		this.setLayout(new GridLayout(1, false));
		label = new Label(this, SWT.NONE);
		label.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		label.setText("Language and DataBase For Filtering");
	    table = new Table(this, SWT.SINGLE|SWT.FULL_SELECTION|SWT.BORDER);
	    table.setLayoutData(new GridData(GridData.FILL_BOTH));
	    table.setHeaderVisible(true);
	    table.setLinesVisible(true);
	    table.addSelectionListener(this);
        for(int i=0; i < COLUMN_TEXT.length; i++){
            TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
            tmpCol.setText(COLUMN_TEXT[i].toString());
            tmpCol.addSelectionListener(this);
            column.add(tmpCol);
        }
        this.pack();
    }

	protected  void setTableItem(){
        this.table.removeAll();
        if(filterList == null) {
            this.resizeTable();
            return;
            /* view test*/
//	            TableItem item = new TableItem(this.table,SWT.NULL);
//	            String[] itemData = new String[6];
//	        	for (int i = 0; i < 6; i++) {
//	        		itemData[i] = "bar";
//	        	}
//	            item.setText(itemData);
        } else {
	        for (FilterGroup file : filterList) {
	            if (!file.exists()) {
	                continue;
	            } else {
	                TableItem item = new TableItem(this.table,SWT.NULL);
	                String[] itemData = new String[4];
	                itemData[0] = (file == this.targetFilter)? "*": "";
	                itemData[1] = file.getLanguageToString();
	                itemData[2] = file.getFaultyDB().getAbsolutePath();
	                itemData[3] = file.getAccurateDB().getAbsolutePath();
	                item.setText(itemData);
	                item.setData(file);
	            }
	        }
        }
        this.resizeTable();
    }

    private void resizeTable(){
        table.setVisible(false);
        TableColumn[] columns = table.getColumns();
        for (int i = 0; i < columns.length; i++)
        {
          columns[i].pack();
        }
        table.setVisible(true);
    }

    public FilterGroup getSelectedFilter() {
        TableItem[] selectedItems = this.table.getSelection();
        if (selectedItems != null && 0 < selectedItems.length) {
            if (selectedItems[0].getData() instanceof FilterGroup) {
                FilterGroup filter = (FilterGroup) selectedItems[0].getData();
                for (FilterGroup entry : FilterTable.this.filterList) {
                    if (entry.equals(filter)) {
                        return entry;
                    }
                }
            }
        }
        return null;
    }

    public int getItemCount() {
        return this.table.getItemCount();
    }

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	    FilterGroup filter = this.getSelectedFilter();
	    if (filter != null) {
	        FilterTable.this.targetFilter = filter;
	        this.parentWidget.setFilter(filter);
	        this.setTableItem();
	    }
	}

	@Override
	public void widgetSelected(SelectionEvent arg0) {
	}
}