

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import codelamp.pio.Language;
import codelamp.util.FileCopy;

public final class PluginDiscriminator {

	private static final String PROJECT_WORKING_DIR = "codelamp";
	private static final String PLUGINLAMP_APP_DIR = "codelamp.pluginlamp.externalprocess";
	private static final Path appPath = Paths.get(System.getProperty("user.dir"), PLUGINLAMP_APP_DIR);

	private final Optional<Path> workingDir;

	public enum FILTER_TYPE {CRM};

	private final Optional<FILTER_TYPE> filterType;

	private class CRMDataSet {
		public static final String PLUGINLAMP_DEFAULT_FAULTY_DB = "Default.fpdb";
		public static final String PLUGINLAMP_DEFAULT_ACCURATE_DB = "Default.apdb";
		public static final String PROJECT_FAULTY_DB = "Faulty.fpdb";
		public static final String PROJECT_ACCURATE_DB = "NonFaulty.apdb";
		private boolean learnTransaction = false;
		private boolean classifyTransaction = false;
		private Path faultyDB;
		private Path accurateDB;
	}
	private CRMDataSet crm;

	public PluginDiscriminator(Path projectDir) {
		this(projectDir, PluginDiscriminator.FILTER_TYPE.CRM);
	}

	public PluginDiscriminator(Path projectDir, PluginDiscriminator.FILTER_TYPE type) {
		if (projectDir == null || !Files.isDirectory(projectDir)) {
			workingDir = Optional.empty();
			filterType = Optional.empty();
			throw new NullPointerException();
		}
		Path dir = projectDir.resolve(PROJECT_WORKING_DIR);
		if (Files.notExists(dir)) {
			try {
				Files.createDirectory(dir);
			} catch (IOException e) {
				workingDir = Optional.empty();
				filterType = Optional.empty();
				e.printStackTrace();
				return;
			}
		} else if (!Files.isDirectory(dir)) {
			workingDir = Optional.empty();
			filterType = Optional.empty();
			throw new NullPointerException();
		}
		switch (type) {
			case CRM:
				crm = new CRMDataSet();
				Path faultyDB = dir.resolve(CRMDataSet.PROJECT_FAULTY_DB);
				Path accurateDB = dir.resolve(CRMDataSet.PROJECT_ACCURATE_DB);
				if (Files.notExists(faultyDB)) {
					try {
						FileCopy.copyBinaryFile(appPath.resolve(CRMDataSet.PLUGINLAMP_DEFAULT_FAULTY_DB).toString(), faultyDB.toString());
					} catch (IOException exception) {
						workingDir = Optional.empty();
						filterType = Optional.empty();
						return;
					}
				}
				crm.faultyDB = faultyDB;
				if (Files.notExists(accurateDB)) {
					try {
						FileCopy.copyBinaryFile(appPath.resolve(CRMDataSet.PLUGINLAMP_DEFAULT_ACCURATE_DB).toString(), accurateDB.toString());
					} catch (IOException exception) {
						workingDir = Optional.empty();
						filterType = Optional.empty();
						return;
					}
				}
				crm.accurateDB = accurateDB;
				workingDir = Optional.of(dir);
				filterType = Optional.of(FILTER_TYPE.CRM);
				break;
			default :
				workingDir = Optional.empty();
				filterType = Optional.empty();
				break;
		}
	}

	public boolean learn(Path targetPath, Language language, boolean isFaulty) {
		if (!workingDir.isPresent() || !filterType.isPresent()) {
			throw new NullPointerException();
		}
		if (targetPath == null || Files.notExists(targetPath) || Files.isDirectory(targetPath)) {
			throw new NullPointerException();
		}
		switch (filterType.get()) {
			case CRM:
				if (crm.classifyTransaction == true || crm.learnTransaction == true) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					learn(targetPath, language, isFaulty);
				}
				Path database;
				if (isFaulty) {
					database = crm.faultyDB;
				} else {
					database = crm.accurateDB;
				}
				crm.learnTransaction = true;
				try {
					if (CRM.learn(targetPath, language, database) == false) {
						return false;
					}
				} catch (NullPointerException | IOException e) {
					e.printStackTrace();
				}
				crm.learnTransaction = false;
				break;
			default:
				break;
		}
		return true;
	}

	public double classify(Path targetPath, Language language) {
		if (!workingDir.isPresent() || !filterType.isPresent()) {
			throw new NullPointerException();
		}
		if (targetPath == null || Files.notExists(targetPath) || Files.isDirectory(targetPath)) {
			throw new NullPointerException();
		}
		double classifiedValue = -1;
		switch (filterType.get()) {
			case CRM:
				if (crm.learnTransaction == true) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					classifiedValue = classify(targetPath, language);
				} else {
					crm.classifyTransaction = true;
					try {
						classifiedValue = CRM.classify(targetPath, language, crm.faultyDB, crm.accurateDB);
					} catch (NullPointerException | IOException e) {
						e.printStackTrace();
					}
					crm.classifyTransaction = false;
				}
				break;
			default:
				break;
		}
		return classifiedValue;
	}

}
