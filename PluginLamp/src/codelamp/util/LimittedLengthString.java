package codelamp.util;

public class LimittedLengthString {
	private LimittedLengthString() {
	}

	private static final int DEFAULT_DECIMAL_LENGTH = 4;
	public static String cutDouble(Double val) {
		String str = String.valueOf(val);
		if (!str.contains(".")) {
			return str;
		}
		int index = str.indexOf(".");
		if (DEFAULT_DECIMAL_LENGTH < str.length() - index) {
			return str.substring(0, index + DEFAULT_DECIMAL_LENGTH + 1);
		}
		int diff = str.length() - index;
		StringBuffer sb = new StringBuffer();
		sb.append(str);
		for (int i = diff; i <= DEFAULT_DECIMAL_LENGTH; i++) {
			sb.append(0);
		}
		return sb.toString();
	}
	public static void main(String[] args) {
		System.out.println(LimittedLengthString.cutDouble(0.0));
		System.out.println(LimittedLengthString.cutDouble(0.111));
		System.out.println(LimittedLengthString.cutDouble(0.1111));
		System.out.println(LimittedLengthString.cutDouble(0.111111));
	}
}
