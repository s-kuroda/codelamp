package codelamp.util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class ApplicationPath {

	private ApplicationPath() {
	}

    public static final File root = new File(System.getProperty("user.dir"));

	public static final Path APPLICATION_DIR = Paths.get(System.getProperty("user.dir"), "codelamp.pluginlamp.externalprocess");

	public static final Optional<Path> TOKENITHER_JAVA;
	static {
		Path tmpPath = null;
		if (PlatformInfo.isWindows() == true) {
			tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/tokenizer/windows/java/mkJavaTokenSC.exe");
		} else if (PlatformInfo.isLinux() == true) {
			tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/tokenizer/linux/java/mkJavaTokenSC");
	    } else if (PlatformInfo.isMac() == true) {
	    	tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/tokenizer/mac/java/mkJavaTokenSC");
	    }
		if (tmpPath == null || !Files.exists(tmpPath) || Files.isDirectory(tmpPath)) {
			TOKENITHER_JAVA = Optional.empty();
		} else {
			TOKENITHER_JAVA = Optional.of(tmpPath);
		}
	}

	public static final Optional<Path> CRM114;
	static {
		Path tmpPath = null;
		if (PlatformInfo.isWindows() == true) {
			if (PlatformInfo.is64bitArch() == true) {
				tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/windows/x64_release/crm114.exe");
			} else if (PlatformInfo.is32bitArch() == true) {
				tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/windows/i382_release/crm114.exe");
			}
		} else if (PlatformInfo.isMac() == true) {
			tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/mac/crm114");
		} else if (PlatformInfo.isLinux() == true) {
			tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/linux/crm114");
		}
		if (tmpPath == null || !Files.exists(tmpPath) || Files.isDirectory(tmpPath)) {
			CRM114 = Optional.empty();
		} else {
			CRM114 = Optional.of(tmpPath);
		}
	}

	public static final Optional<Path> CRM114_DEFAULT_FAULTY_DATABASE;
	static {
		Path tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/default/default.fpdb");
		if (tmpPath == null || !Files.exists(tmpPath) || Files.isDirectory(tmpPath)) {
			CRM114_DEFAULT_FAULTY_DATABASE = Optional.empty();
		} else {
			CRM114_DEFAULT_FAULTY_DATABASE = Optional.of(tmpPath);
		}
	}

	public static final Optional<Path> CRM114_DEFAULT_ACCURATE_DATABASE;
	static {
		Path tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/default/default.apdb");
		if (tmpPath == null || !Files.exists(tmpPath) || Files.isDirectory(tmpPath)) {
			CRM114_DEFAULT_ACCURATE_DATABASE = Optional.empty();
		} else {
			CRM114_DEFAULT_ACCURATE_DATABASE = Optional.of(tmpPath);
		}
	}

	public static final Optional<Path> CRM114_EMPTY_FAULTY_DATABASE;
	static {
		Path tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/default/empty.fpdb");
		if (tmpPath == null || !Files.exists(tmpPath) || Files.isDirectory(tmpPath)) {
			CRM114_EMPTY_FAULTY_DATABASE = Optional.empty();
		} else {
			CRM114_EMPTY_FAULTY_DATABASE = Optional.of(tmpPath);
		}
	}

	public static final Optional<Path> CRM114_EMPTY_ACCURATE_DATABASE;
	static {
		Path tmpPath = ApplicationPath.APPLICATION_DIR.resolve("lib/CRM114/default/empty.apdb");
		if (tmpPath == null || !Files.exists(tmpPath) || Files.isDirectory(tmpPath)) {
			CRM114_EMPTY_ACCURATE_DATABASE = Optional.empty();
		} else {
			CRM114_EMPTY_ACCURATE_DATABASE = Optional.of(tmpPath);
		}
	}

	public static void main(String[] args) {
		System.out.println(TOKENITHER_JAVA.isPresent());
		System.out.println(CRM114.isPresent());
		System.out.println(CRM114_DEFAULT_ACCURATE_DATABASE.isPresent());
		System.out.println(CRM114_DEFAULT_FAULTY_DATABASE.isPresent());
	}
}
