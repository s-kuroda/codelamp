package codelamp.pluginlamp.widget;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TreeItem;

import codelamp.pio.Charsets;
import codelamp.pio.Language;
import codelamp.pluginlamp.internal.CommitDiffEdit;
import codelamp.pluginlamp.internal.CommitTrace;
import codelamp.pluginlamp.internal.PluginDiscriminator;
import codelamp.util.EpocDateTime;

public class JPluginLampUI extends Composite {

	ProjectSelector projectSelector;
	GitResultTreeTable table;
	LevelButtons btns;
	DiffViewer textViewer;
	LanguageSelector languageSelector;

	public JPluginLampUI(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1, false));
		projectSelector = new ProjectSelector(this, SWT.NONE);
		projectSelector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		SashForm sash = new SashForm(this, SWT.HORIZONTAL);
		sash.setLayoutData(new GridData(GridData.FILL_BOTH));
		Composite left = new Composite(sash, SWT.NONE);
		left.setLayoutData(new GridData(GridData.FILL_BOTH));
		left.setLayout(new GridLayout(2, false));
		table = new GitResultTreeTable(left, SWT.NONE);
		GridData wide = new GridData(GridData.FILL_BOTH);
		wide.horizontalSpan = 2;
		table.setLayoutData(wide);
		languageSelector = new LanguageSelector(left, SWT.NONE);
		languageSelector.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING));
		btns = new LevelButtons(left, SWT.NONE);
		btns.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		textViewer = new DiffViewer(sash, SWT.NONE);
		textViewer.setLayoutData(new GridData(GridData.FILL_BOTH));
		int[] weights = {1, 1};
		sash.setWeights(weights);
		init();
		pack();
	}

	private final static String[] COLUMN_LABEL =
		{"CommitID / FileName", "Classified Result", "Commit Time", "Commit Message / Absolute Path"};
	private static final String FAULTY_LABEL = "Faulty";
	private static final String ACCURATE_LABEL = "Non-Faulty";
	private Language language;
	private void init() {
		getProjects();
		projectSelector.addComboSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
			    try {
			    	if (projectSelector.getText() != null) {
			    		Path project = projectsMap.get(projectSelector.getText());
			    		if (project == null || project.equals(targetProject)) {
			    			return;
			    		} else {
			    			changeProject(project);
			    		}
			    	}
			    } catch (java.util.ConcurrentModificationException e) {
			        this.widgetSelected(event);
			    }
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		projectSelector.addBtnSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				getProjects();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});

		table.setColumns(Arrays.asList(COLUMN_LABEL));
		table.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
		        Object obj = event.item;
		        System.out.println(obj);
		        if (obj != null && obj instanceof TreeItem) {
		        	TreeItem item = (TreeItem) obj;
		        	Object data = item.getData();
		        	if (data instanceof DiffEntry) {
		        		DiffEntry entry = (DiffEntry) data;
//		        		textViewer.setDocument(CommitTrace.getDiffText(entry, gitDir));
		        	}else if (data instanceof RevCommit) {
		        		RevCommit commit = (RevCommit) data;
//		        		textViewer.setDocument(CommitTrace.getDiffText(commit, gitDir));
		        	}
		        }
			}
		});

		languageSelector.setInput(Language.getAll());
		languageSelector.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				language = Language.parse(languageSelector.getText());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		language = Language.parse(languageSelector.getText());
		btns.addButton(FAULTY_LABEL, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				List<DiffEntry> tmpList = getSelectedLearnTargets();
				if (tmpList == null || tmpList.isEmpty()) {
					return;
				}
				if (leaner != null) {
					leaner.learnFaultyList.addAll(tmpList);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});

		btns.addButton(ACCURATE_LABEL, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				List<DiffEntry> tmpList = getSelectedLearnTargets();
				if (tmpList == null || tmpList.isEmpty()) {
					return;
				}
				if (leaner != null) {
					leaner.learnAccurateList.addAll(tmpList);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
	}

	private HashMap<String, Path> projectsMap = new HashMap<>();
	private Path targetProject = null;
	private void getProjects() {
		List<Path> projectsPath = new ArrayList<>();
//		IWorkspace workspace = ResourcesPlugin.getWorkspace();
//		IWorkspaceRoot root = workspace.getRoot();
//		IProject[] projects = root.getProjects();
//		if (projects == null || projects.length <= 0) {
//			return;
//		}
//		Arrays.stream(projects).forEach(e -> projectsPath.add(e.getFullPath().toFile().toPath()));
		projectsPath.add(Paths.get("D:/Users/ZiTsP/Develop/GitClone/jgit-cookbook"));
		projectsPath.add(Paths.get("D:/Users/ZiTsP/Develop/GitClone/BitBucket/codelamp"));
		projectsMap.clear();
		projectsPath.forEach(e -> projectsMap.put(e.getFileName().toString(), e));
		List<String> names = new ArrayList<>();
		projectsMap.forEach((k,v) -> names.add(k));
		projectSelector.setInput(names);
	}

	private List<DiffEntry> getSelectedLearnTargets() {
		List<TreeItem> selected = table.getSelectedItem();
		if (selected == null || selected.isEmpty()) {
			return null;
		}
		List<RevCommit> tmpCommits  = new ArrayList<>();
		selected.stream().filter(e -> e.getData() instanceof RevCommit)
			.forEach(e -> tmpCommits.add((RevCommit) e.getData()));
		List<DiffEntry> allEntries = new ArrayList<>();
		List<DiffEntry> tmpEntries = new ArrayList<>();
		if (tmpCommits != null && !tmpCommits.isEmpty()) {
			tmpCommits.forEach(e -> allEntries.addAll(CommitTrace.getDiffEntry(e, gitDir.toString())));
		}
		if (allEntries != null && !allEntries.isEmpty()) {
			for (String extension : language.getExtension()) {
				List<DiffEntry> tmpList = CommitTrace.trimExtraExtensionFiles(allEntries, extension);
				if (tmpList != null && !tmpList.isEmpty()) {
					tmpEntries.addAll(tmpList);
				}
			}
		}
		selected.stream().filter(e -> e.getData() instanceof DiffEntry)
			.map(e -> (DiffEntry) e.getData())
			.filter(e -> !tmpEntries.contains(e))
			.forEach(e -> tmpEntries.add(e));
		if (tmpEntries == null || tmpEntries.isEmpty()) {
			return null;
		}
		return tmpEntries;
	}

	private static final String NOT_EXIST = "No such project exists";
	private static final String NOT_REPOSITORY = "Not git project";
	private Path gitDir;
	private HeadTrace headTrace = null;
	private LearnThread leaner = null;
	private PluginDiscriminator filter = null;
	private void changeProject(Path path) {
		clearProjectData();
		targetProject = path;
		if (path == null || !Files.exists(path)) {
			textViewer.setDocument(NOT_EXIST);
			return;
		}
		gitDir = CommitTrace.setGitDir(path.toString());
		if (gitDir == null) {
			textViewer.setDocument(NOT_REPOSITORY);
			return;
		}
		headTrace = new HeadTrace(gitDir);
		headTrace.start();
		leaner = new LearnThread();
		leaner.start();
	}

	private void clearProjectData() {
		targetProject = null;
		gitDir = null;
		filter = null;
		table.clear();
		textViewer.clear();
		if (headTrace != null) {
			headTrace.kill();
			headTrace = null;
		}
		if (leaner != null) {
			leaner.kill();
			leaner = null;
		}
	}

	private class LearnThread extends Thread {
		private boolean kill = false;
		public LearnThread() {
		}

		@Override
		public void run() {
			/*
			try {
				while (!kill) {
					if (learnFaultyList == null || learnFaultyList.isEmpty()){
						sleep(100);
						continue;
					} else {
						DiffEntry diff = learnFaultyList.removeFirst();
						if (diff != null) {
//							PluginDiscriminator.learn( isAccurate, filter);
						}
					}
					if (kill) {
						break;
					}
					if (learnAccurateList == null || learnAccurateList.isEmpty()){
						sleep(100);
						continue;
					} else {
						DiffEntry diff = learnAccurateList.removeFirst();
						if (diff != null) {
//							PluginDiscriminator.learn( isAccurate, filter);
						}
					}
						sleep(1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			*/
			while(true){

			}
		}
		public void kill() {
			this.kill = true;
		}

		private LinkedList<DiffEntry> learnFaultyList = new LinkedList<>();
		private LinkedList<DiffEntry> learnAccurateList = new LinkedList<>();
	}

	private class HeadTrace extends Thread {
		private final Path gitDir;
		private boolean kill = false;
		public HeadTrace(Path gitDir) {
			this.gitDir = gitDir;
			if (this.gitDir == null) {
				kill = true;
			}
		}

		private RevCommit head = null;
		@Override
		public void run() {
			while (!kill) {
				if (head == null || !head.equals(CommitTrace.getHeadCommit(gitDir))){
					head = CommitTrace.getHeadCommit(gitDir);
					addNewHead(head);
					List<DiffEntry> diffEntries = table.getChildsData(head);
					if (diffEntries == null || diffEntries.isEmpty()) {
						continue;
					}
					Map<DiffEntry, Double> classifiedMap = new HashMap<>();
					diffEntries.forEach(e -> {
						Optional<Double> val = Optional.empty();
						val = PluginDiscriminator.classify(e, gitDir, language);
						val.ifPresent(classifiedVal -> {
							classifiedMap.put(e, classifiedVal);
						});
					});
					if (classifiedMap == null || classifiedMap.isEmpty()) {
						continue;
					}
					getDisplay().asyncExec(new Runnable() {
						@Override
						public void run() {

						}
					});

        			TreeItem commit = table.getTree().getItem(0);
        			Arrays.stream(commit.getItems()).filter(e -> e.getData() instanceof DiffEntry)
        				.forEach(e -> {
        					e.setText(1, classify((DiffEntry) e.getData()));
        			});
//					classify(head);
//					reflectClassified();
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
		public void kill() {
			this.kill = true;
		}
	}

	private class ClassifiedCommit {
		private final RevCommit commit;
		private Map<DiffEntry, Double> diffEntries = new HashMap<>();
		public ClassifiedCommit(RevCommit commit) {
			this.commit = commit;
		}
	}
	private ArrayList<ClassifiedCommit> dataList = new ArrayList<>();


	private static final String DIFF_TEXT = "diff.txt";
	private void classify(RevCommit commit) {
		ClassifiedCommit newHead = new ClassifiedCommit(commit);
		List<DiffEntry> tmpEntries = CommitTrace.getDiffEntry(commit, gitDir.toString());
		List<DiffEntry> diffEntries = new ArrayList<>();
		for (String extension : language.getExtension()) {
			List<DiffEntry> tmpList = CommitTrace.trimExtraExtensionFiles(tmpEntries, extension);
			if (tmpList != null && !tmpList.isEmpty()) {
				diffEntries.addAll(tmpList);
			}
		}
		for (DiffEntry entry : diffEntries) {
			try {
				PipedOutputStream gitDiffWriter = new PipedOutputStream();
				PipedInputStream gitDiffReader = new PipedInputStream(gitDiffWriter);
				if (CommitTrace.outputDiff(entry, gitDir, gitDiffWriter) == false) {
					continue;
				}
				gitDiffWriter.flush();
				gitDiffWriter.close();
				PipedOutputStream editWriter = new PipedOutputStream();
				PipedInputStream editReader = new PipedInputStream(editWriter);
				if (CommitDiffEdit.extractAddedLine(gitDiffReader, editWriter) == false) {
//					Files.delete(diff);
					continue;
				}
				gitDiffReader.close();
				editWriter.flush();
				editWriter.close();
				Path diff = Paths.get(targetProject.toString(), DIFF_TEXT);
				BufferedWriter output = Files.newBufferedWriter(diff);
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(editReader, Charsets.UTF8))) {
					reader.lines().forEach(e -> {
						System.out.println(e);
						try {
							output.write(e);
							output.newLine();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					});
					output.flush();
					output.close();
				}
				throw new FileNotFoundException();
//				Path diff = Paths.get(targetProject.toString(), DIFF_TEXT);
//				OutputStream output = Files.newOutputStream(diff);
//				if (CommitDiffEdit.extractAddedLine(gitDiffReader, output) == false) {
////					Files.delete(diff);
//					continue;
//				}
//				gitDiffReader.close();
//				output.flush();
//				output.close();
//				double val = filter.classify(diff, language);
//				double val = 0;
//				newHead.diffEntries.put(entry, val);
//				diffEntries.forEach(System.out :: println);
			} catch(IOException exception) {
				exception.printStackTrace();
			}
		}
		ArrayList<ClassifiedCommit> tmpList = new ArrayList<>();
		tmpList.add(newHead);
		tmpList.addAll(dataList);
		dataList = tmpList;
	}

	public String classify(DiffEntry e) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	LinkedHashMap<RevCommit, List<DiffEntry>> records = new LinkedHashMap<>();
	private void addNewHead(RevCommit head) {
		List<DiffEntry> diffEntries = CommitTrace.getDiffEntry(head, gitDir.toString());
		List<DiffEntry> targetEntries = new ArrayList<>();
		for (String extension : language.getExtension()) {
			List<DiffEntry> tmpList = CommitTrace.trimExtraExtensionFiles(diffEntries, extension);
			if (tmpList != null && !tmpList.isEmpty()) {
				targetEntries.addAll(tmpList);
			}
		}
		LinkedHashMap<RevCommit, List<DiffEntry>> tmp = new LinkedHashMap<>();
		tmp.putAll(records);
		records.clear();
		records.put(head, targetEntries);
		records.putAll(tmp);
		setEntries();
	}
	private static final String NOTYET_CLASSIFIED = "---";
	private void setEntries() {
		records.forEach((k, v) -> {
			ArrayList<String> commitText = new ArrayList<>();
			commitText.add(CommitTrace.limitIdLength(k.getId()));
			commitText.add("");
			commitText.add(EpocDateTime.convertEpocToString(k.getCommitTime()));
			commitText.add(k.getShortMessage());
			Map<DiffEntry, ArrayList<String>> diffsText = new HashMap<>();
			if (v != null && !v.isEmpty()) {
				v.forEach(e -> {
					ArrayList<String> text = new ArrayList<>();
					Path newPath = Paths.get(gitDir.getParent().toString(), e.getNewPath());
					long lastModified = 0;
					try {
						if (Files.exists(newPath)) {
							lastModified = newPath.toFile().lastModified();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					text.add(newPath.getFileName().toString());
					text.add(NOTYET_CLASSIFIED);
					if (lastModified != 0) {
						text.add(EpocDateTime.convertEpocToString(lastModified));
					} else {
						text.add("");
					}
					text.add(newPath.toString());
					diffsText.put(e, text);
				});
			}
			getDisplay().asyncExec(new Runnable() {
                public void run() {
                	TreeItem commit = new TreeItem(table.getTree(), SWT.NONE);
                	commit.setText(commitText.toArray(new String[commitText.size()]));
                	commit.setData(k);
        			if (diffsText != null && !diffsText.isEmpty()) {
        				diffsText.forEach((entry, text) -> {
                        	TreeItem diffEntry = new TreeItem(commit, SWT.NONE);
                        	diffEntry.setText(text.toArray(new String[text.size()]));
                        	diffEntry.setData(entry);
        				});
        			}
                }
            });
		});
	}

	public void reflectClassified() {
		LinkedHashMap<ArrayList<String>, RevCommit> map = new LinkedHashMap<>();
		dataList.forEach(e -> {
			ArrayList<String> text = new ArrayList<>();
			text.add(CommitTrace.limitIdLength(e.commit.getId()));
			text.add("");
			text.add(EpocDateTime.convertEpocToString(e.commit.getCommitTime()));
			text.add(e.commit.getShortMessage());
			map.put(text, e.commit);
		});
		if (map != null && !map.isEmpty()) {
			getDisplay().asyncExec(new Runnable() {
                public void run() {
        			table.setParentInput(map);
                }
            });
		} else {
			return;
		}
		dataList.forEach(e -> {
			if (e.diffEntries == null || e.diffEntries.isEmpty()) {
				return;
			}
			Map<ArrayList<String>, DiffEntry> diffMap = new HashMap<>();
			e.diffEntries.entrySet().forEach(f -> {
				ArrayList<String> text = new ArrayList<>();
				Path newPath = Paths.get(f.getKey().getNewPath());
				text.add(newPath.getFileName().toString());
				text.add(String.valueOf(f.getValue()));
				text.add("");
				text.add(newPath.toAbsolutePath().toString());
				diffMap.put(text, f.getKey());
			});
			if (diffMap != null && !diffMap.isEmpty()) {
				getDisplay().asyncExec(new Runnable() {
	                public void run() {
	    				table.setChildInput(table.getParentItem(e), diffMap);
	                }
	            });
			}
		});
	}

	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		JPluginLampUI c = new JPluginLampUI(shell, SWT.BORDER);
		shell.pack();
		shell.open();
		shell.setSize(800, 400);
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}

}
