package codelamp.pluginlamp.widget;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.LineBackgroundEvent;
import org.eclipse.swt.custom.LineBackgroundListener;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class DiffViewer extends Composite {

	StyledText text;

	public DiffViewer(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new FillLayout());
        text = new StyledText(this, SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL | SWT.READ_ONLY);
        text.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
	}

	public void setColeredLine(String keyWord, Color color) {
		text.addLineBackgroundListener(new LineBackgroundListener(){
			@Override
			public void lineGetBackground(LineBackgroundEvent event){
		      if (event.lineText.startsWith(keyWord)){
		          event.lineBackground = color;
		      }
		   }
		});
	}

	public void setColeredLine(String keyWord, int colorStyle) {
		setColeredLine(keyWord, getDisplay().getSystemColor(colorStyle));
	}

	public void setDocument(List<String> doc) {
		System.out.println(doc.isEmpty());
		StringJoiner sj = new StringJoiner("\n");
		doc.forEach(e -> sj.add(e));
		text.setText(sj.toString());
	}
	public void setDocument(String doc) {
		text.setText(doc);
	}

	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		LevelButtons cc = new LevelButtons(shell, SWT.BORDER);
		cc.addButton("BTN1", SWT.PUSH, null);
		cc.addButton("BTN2", SWT.PUSH, null);
		Button btn2 = new Button(shell, SWT.PUSH);
		btn2.setText("ANOTHER");
		DiffViewer dv = new DiffViewer(shell, SWT.BORDER);
		dv.setDocument(Arrays.asList("holo","SWI", "Vandid"));
		dv.setColeredLine("holo", SWT.COLOR_CYAN);
		dv.setColeredLine("SWI", SWT.COLOR_DARK_CYAN);
		dv.setColeredLine("Vandid", SWT.COLOR_MAGENTA);
//		TextViewer tv = new TextViewer(shell, SWT.BORDER);
//		tv.setInput("BAR");
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}

	public void clear() {
		text.setText("");
	}

}
