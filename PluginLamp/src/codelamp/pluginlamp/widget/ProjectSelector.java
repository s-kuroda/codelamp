package codelamp.pluginlamp.widget;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

public class ProjectSelector extends Composite {

	private Combo combo;
	private Button btn;
    private static final String SELECT_PROJECT = "Select Project";
    private static final String RELOAD_BUTTON_TEXT = "Reload";

	public ProjectSelector(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		combo = new Combo(this,SWT.DROP_DOWN | SWT.READ_ONLY);
		combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		btn = new Button(this, SWT.PUSH);
		btn.setText(RELOAD_BUTTON_TEXT);
		setInput(null);
		this.pack();
	}


	public void setInput(List<String> data) {
		combo.removeAll();
		combo.add(SELECT_PROJECT);
		if (data == null || data.isEmpty()) {
			combo.select(0);
			return;
		}
		data.forEach(e -> combo.add(e.toString()));
		combo.select(0);
	}

	public void addInput(String data) {
		if (data == null) {
			return;
		}
		if (!Arrays.asList(combo.getItems()).contains(data)) {
			combo.add(data);
		}
		combo.select(combo.indexOf(data));
	}

	public String getText() {
		if (combo.getText().equals(SELECT_PROJECT)) {
			return null;
		} else {
			return combo.getText();
		}
	}

    public void addComboSelectionListener(SelectionListener listener) {
    	combo.addSelectionListener(listener);
    }
    public void addBtnSelectionListener(SelectionListener listener) {
    	btn.addSelectionListener(listener);
    }

	@Override
	public void dispose() {
	    this.combo.dispose();
	    super.dispose();
	}
}
