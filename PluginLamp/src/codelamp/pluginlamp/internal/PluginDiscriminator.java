package codelamp.pluginlamp.internal;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.jgit.diff.DiffEntry;

import codelamp.pio.Charsets;
import codelamp.pio.Language;
import codelamp.util.ApplicationPath;
import codelamp.util.FileCopy;

public final class PluginDiscriminator {

	private static final String PLUGINLAMP_DATA_DIRECTORY_NAME = "codelamp";
	private static final String PLUGINLAMP_FAULTY_DATABASE_NAME = "Faulty.fpdb";
	private static final String PLUGINLAMP_ACCURATE_DATABASE_NAME = "NonFaulty.apdb";

	private PluginDiscriminator() {
	}

	public static final String GIT_DIR = ".git";
	public static final Optional<Path> getGitDir(Path repositoryPath) {
		if (repositoryPath == null || !Files.exists(repositoryPath) || !Files.isDirectory(repositoryPath)) {
			return Optional.empty();
		} else if (repositoryPath.getFileName().toString().equals(GIT_DIR) && Files.isDirectory(repositoryPath)) {
			return Optional.of(repositoryPath);
		} else {
			try (DirectoryStream<Path> fileList = Files.newDirectoryStream(repositoryPath, GIT_DIR)) {
				if (fileList != null) {
				    for (Path file : fileList) {
				    	return Optional.of(file);
				    }
				}
			} catch (IOException exception) {
				exception.printStackTrace();
			}
		}
		return Optional.empty();
	}


	private static class Transaction {
		boolean learning = false;
		boolean classifying = false;
		public Transaction() {
		}
		public boolean enableLearn() {
			return (learning == true || classifying == true)? false : true;
		}
		public boolean enableClassify() {
			return (learning == true)? false : true;
		}
	}

	private static final Transaction transaction = new Transaction();

	public static final Optional<Double> classify(DiffEntry diffEntry, Path repositoryPath, Language language) {
		Optional<Path> gitDir = getGitDir(repositoryPath);
		if (!gitDir.isPresent()) {
			return Optional.empty();
		}
		Path workDir = repositoryPath.getParent().resolve(PLUGINLAMP_DATA_DIRECTORY_NAME);
		if (workDirCheck(workDir) == false) {
			return Optional.empty();
		}
		System.out.println(workDir);
		double classifiedValue = classifying(diffEntry, repositoryPath, language, workDir);
		if (classifiedValue < 0) {
			return Optional.empty();
		} else {
			return Optional.of(classifiedValue);
		}
	}

	private static final String CLASSIFY_TMPFILE_PREFIX = "PLUGINLAMP_CLASSIFY";

	private static final String FROMPATH_LINE = "+++";
	private static final String ADDED_LINE = "+";

	private static double classifying(DiffEntry diffEntry, Path repositoryPath, Language language, Path workDir) {
		double val = -1;
		if (transaction.enableClassify() == false) {
			return classifying(diffEntry, repositoryPath, language, workDir);
		} else {
			transaction.classifying = true;
			try {
				Path tmpDiff = Files.createTempFile(CLASSIFY_TMPFILE_PREFIX, null);
				ArrayList<String> doc = new ArrayList<>();
				List<String> diffText = CommitTrace.getDiffText(diffEntry, repositoryPath);
				diffText.forEach(text -> {
					if (!text.startsWith(FROMPATH_LINE) && text.startsWith(ADDED_LINE)) {
						doc.add(text.substring(ADDED_LINE.length()));
					} else {
						doc.add("");
					}
				});
				if (doc == null || doc.isEmpty()) {
					return -1;
				}
				Files.write(tmpDiff, doc, Charsets.UTF8);
				val = CRM.classify(tmpDiff, language, workDir.resolve(PLUGINLAMP_FAULTY_DATABASE_NAME), workDir.resolve(PLUGINLAMP_ACCURATE_DATABASE_NAME));
				Files.delete(tmpDiff);
			} catch (IOException exception) {
				exception.printStackTrace();
			}
			transaction.classifying = false;
		}
		return val;
	}

	public static boolean learn(DiffEntry diffEntry, Path repositoryPath, Language language, boolean isFaulty) {
		Optional<Path> gitDir = getGitDir(repositoryPath);
		if (!gitDir.isPresent()) {
			return false;
		}
		Path workDir = repositoryPath.getParent().resolve(PLUGINLAMP_DATA_DIRECTORY_NAME);
		if (workDirCheck(workDir) == false) {
			return false;
		}
		Path database = (isFaulty)? workDir.resolve(PLUGINLAMP_FAULTY_DATABASE_NAME) : workDir.resolve(PLUGINLAMP_ACCURATE_DATABASE_NAME);
		return learning(diffEntry, repositoryPath, language, database, workDir);
	}

	private static final String LEARN_TMPFILE_PREFIX = "PLUGINLAMP_LEARN";
	private static boolean learning(DiffEntry diffEntry, Path repositoryPath, Language language, Path database, Path workDir) {
		if (transaction.enableLearn() == false) {
			return learning(diffEntry, repositoryPath, language, database, workDir);
		} else {
			transaction.learning = true;
			boolean success = false;
			try {
				Path tmpDiff = Files.createTempFile(LEARN_TMPFILE_PREFIX, null);
				ArrayList<String> doc = new ArrayList<>();
				List<String> diffText = CommitTrace.getDiffText(diffEntry, repositoryPath);
				diffText.forEach(text -> {
					if (!text.startsWith(FROMPATH_LINE) && text.startsWith(ADDED_LINE)) {
						doc.add(text.substring(ADDED_LINE.length()));
					} else {
						doc.add("");
					}
				});
				if (doc == null || doc.isEmpty()) {
					return false;
				}
				Files.write(tmpDiff, doc, Charsets.UTF8);
				success = CRM.learn(tmpDiff, language, database);
				Files.delete(tmpDiff);
			} catch (IOException exception) {
				exception.printStackTrace();
			}
			transaction.learning = false;
			return success;
		}
	}

	private static boolean workDirCheck(Path workDir) {
		try {
			if (Files.notExists(workDir)) {
				Files.createDirectory(workDir);
			} else if (!Files.isDirectory(workDir)) {
				return false;
			}
			if (Files.notExists(workDir.resolve(PLUGINLAMP_FAULTY_DATABASE_NAME))) {
				copyDataBase(workDir.resolve(PLUGINLAMP_FAULTY_DATABASE_NAME), true);
			} else if (Files.isDirectory(workDir.resolve(PLUGINLAMP_FAULTY_DATABASE_NAME))) {
				return false;
			}
			if (Files.notExists(workDir.resolve(PLUGINLAMP_ACCURATE_DATABASE_NAME))) {
				copyDataBase(workDir.resolve(PLUGINLAMP_ACCURATE_DATABASE_NAME), true);
			} else if (Files.isDirectory(workDir.resolve(PLUGINLAMP_ACCURATE_DATABASE_NAME))) {
				return false;
			}
		} catch (IOException exception) {
			exception.printStackTrace();
			return false;
		}
		return true;
	}

	private static void copyDataBase(Path newPath, boolean isFaulty) throws IOException {
		Optional<Path> defaultDataBase = (isFaulty)?
				ApplicationPath.CRM114_DEFAULT_FAULTY_DATABASE : ApplicationPath.CRM114_DEFAULT_ACCURATE_DATABASE;
		if (defaultDataBase.isPresent()) {
			FileCopy.copyBinaryFile(defaultDataBase.get(), newPath);
		} else {
			throw new NoSuchFileException(defaultDataBase.toString());
		}
	}
}
