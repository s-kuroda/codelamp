package codelamp.pluginlamp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import codelamp.pluginlamp.widget.AbstractPluginLamp;

public class PluginLamp extends ViewPart {


	@Override
	public void createPartControl(Composite parent) {
		AbstractPluginLamp view = new AbstractPluginLamp(parent, SWT.NONE);
	}

	@Override
	public void setFocus() {
	}
}
