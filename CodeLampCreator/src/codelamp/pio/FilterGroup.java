package codelamp.pio;

import java.io.File;

public class FilterGroup {

	private final File faultyDB;
	private final File accurateDB;
	private final Language language;

	public FilterGroup(String faultyDB, String accurateDB, String language) {
		this.faultyDB = new File(faultyDB);
		this.accurateDB = new File(accurateDB);
		this.language = Language.pase(language);
	}

	public FilterGroup(File faultyDB, File accurateDB, String language) {
		this.faultyDB = faultyDB;
		this.accurateDB = accurateDB;
		this.language = Language.pase(language);
	}

	public FilterGroup(String faultyDB, String accurateDB, Language language) {
		this.faultyDB = new File(faultyDB);
		this.accurateDB = new File(accurateDB);
		this.language = language;
	}

	public FilterGroup(File faultyDB, File accurateDB, Language language) {
		this.faultyDB = faultyDB;
		this.accurateDB = accurateDB;
		this.language = language;
	}

	public final boolean exists() {
		if (this.faultyDB == null || this.accurateDB == null || this.language == null) {
			throw new NullPointerException();
		} else if (!this.faultyDB.exists() || !this.accurateDB.exists()) {
			return false;
		} else if (this.language == Language.EMPTY || !Language.has(this.language)) {
			return false;
		} else {
			return true;
		}
	}

	public final File getFaultyDB() {
		return this.faultyDB;
	}

	public final File getAccurateDB() {
		return this.accurateDB;
	}

	public final Language getLanguage() {
		return language;
	}

	public final String getLanguageToString() {
		return this.language.getLanguage();
	}

	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj instanceof FilterGroup) {
			FilterGroup tmp = (FilterGroup) obj;
			if ((tmp.getFaultyDB().getAbsolutePath().equals(this.faultyDB.getAbsolutePath()))
					&& (tmp.getAccurateDB().getAbsolutePath().equals(this.accurateDB.getAbsolutePath()))
					&& (tmp.getLanguage().equals(this.getLanguage()))) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}
