package codelamp.widget.creator;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import codelamp.internal.filter.Discriminator;
import codelamp.internal.git.GitCommitDiff;
import codelamp.pio.FilterGroup;

public class CreatorView extends Composite {

	private GitLogTable table;
	private CreatorButtons btns;
	private LearnProgress progressBar;

	private final String DIRECTORY_SELECTION = "Select Directory where you want to create DB for CodeLamp";
	
	private GitCommitDiff git = null;

	private LearnThread learn;

    private final String BUG_DB = "bug.fpdb";
    private final String INNOCENT_DB = "notbug.apdb";

	private boolean learning = false;
	public boolean isLearning() {
		return this.learning;
	}

	public final void setProject(File project) {
	    try {
    		this.git = new GitCommitDiff(project);
    		this.table.setCommitList(CreatorView.this.git.getCommitsList());
	    } catch (NullPointerException e) {
	        // nothing
	    }
	}

	public CreatorView(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(1,false));
        SashForm sash = new SashForm(this, SWT.HORIZONTAL);
        sash.setLayoutData(new GridData(GridData.FILL_BOTH));
		this.table = new GitLogTable(sash, SWT.BORDER);
		this.table.setLayoutData(new GridData(GridData.FILL_BOTH));
		this.btns = new CreatorButtons(sash, SWT.NONE);
		this.btns.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        int[] sashWeight = {3, 1};
        sash.setWeights(sashWeight);
        this.progressBar = new LearnProgress(this, SWT.BORDER);
        this.progressBar.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.pack();
	}

	private final class CreatorButtons extends AbstractCreatorButtons {

		public CreatorButtons(Composite parent, int style) {
			super(parent, style);
		}

		@Override
		protected void setFaulty() {
			CreatorView.this.table.setSelectedState(true);
		}

		@Override
		protected void setAccurate() {
			CreatorView.this.table.setSelectedState(false);
		}

		@Override
		protected void unSelect() {
			CreatorView.this.table.cancelState();
		}

		@Override
		protected void clearChecked() {
			CreatorView.this.table.clearSelected();
		}

		@Override
		protected void reverseChecked() {
			CreatorView.this.table.reverseSelected();
		}

		@Override
		protected void startSetUp() {
			super.setButtonsLock(true);
			CreatorView.this.learning = true;
			try {
				DirectoryDialog dialog = new DirectoryDialog(this.getShell());
				dialog.setFilterPath(System.getProperty("user.dir"));
				dialog.setMessage(DIRECTORY_SELECTION);
				File directory = new File(dialog.open());
	            File faultyDB = new File(directory.getAbsolutePath(), BUG_DB);
	            if (!faultyDB.exists()) {
					faultyDB = Discriminator.create(faultyDB.getAbsolutePath());
	            }
	            File accurateDB = new File(directory.getAbsolutePath(), INNOCENT_DB);
	            if (!accurateDB.exists()) {
	            	accurateDB = Discriminator.create(accurateDB.getAbsolutePath());
	            }
	            FilterGroup filter = new FilterGroup(faultyDB, accurateDB, this.getLanguage());
				CreatorView.this.progressBar.start(CreatorView.this.table.getTargetCommits().size());
				StringBuilder msg = new StringBuilder();
				msg.append("Create DataBase in ");
				msg.append(filter.getFaultyDB().getParentFile().getAbsolutePath());
				CreatorView.this.progressBar.setMessage(msg.toString());
	            learn = new LearnThread(filter);
	            learn.start();
	        } catch (NullPointerException e) {
				super.setButtonsLock(false);
				CreatorView.this.learning = false;
				return;
	        }
		}

	}

	private final class LearnThread extends Thread {

		private final FilterGroup filter;
		public LearnThread(FilterGroup filter) {
			this.filter = filter;
		}

		private boolean kill = false;

		public void run() {
			getDisplay().asyncExec(new Runnable() {
	            public void run() {
					if (!CreatorView.this.git.commitExists()) {
						return;
					}
	    			for (Entry<RevCommit, Boolean> entry: CreatorView.this.table.getTargetCommits().entrySet()) {
	    				if (kill) {
	    					CreatorView.this.progressBar.setMessage("STOP");;
	    					break;
	    				} else if (entry.getKey().getId().equals(CreatorView.this.git.getFirstId())) {
	    					CreatorView.this.progressBar.step();
	    					continue;
	    				} else {
	    					File tmpFile = new File(LearnThread.this.filter.getFaultyDB().getParentFile().getAbsolutePath(), "diff.txt");
	    					try {
	    						CreatorView.this.git.getDiffFile(tmpFile, entry.getKey(), LearnThread.this.filter.getLanguage());
	    						Discriminator.learn(tmpFile, entry.getValue(), LearnThread.this.filter);
	    						CreatorView.this.progressBar.step();
	    					} catch (IOException e) {
	    						e.printStackTrace();
	    					}
	    				}
	    			}
	    			CreatorView.this.btns.setButtonsLock(false);
	    			CreatorView.this.table.cancelStateAll();
					CreatorView.this.progressBar.setMessage(null);
	    		}
			});
		}

		public void kill() {
			this.kill = true;
		}
	}

	public static void main(String[] args) {
        Display display = Display.getDefault();
        Shell shell = new Shell();
        shell.setLayout(new GridLayout(1, false));
        CreatorView f = new CreatorView(shell,SWT.NONE);
        f.setLayoutData(new GridData(GridData.FILL_BOTH));
        shell.pack();
        shell.open();
        shell.setSize(600,500);
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        f.dispose();
        display.dispose();
    }

	@Override
	public void dispose() {
        if (this.learn != null) {
            this.learn.kill();
        }
		this.btns.dispose();
		this.table.dispose();
		this.progressBar.dispose();
		super.dispose();
	}


}
