package codelamp.internal.git;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

import codelamp.pio.Language;

public final class GitCommitDiff {

	private File gitDir;

	public GitCommitDiff(File file) throws NullPointerException {
		this.setGitDirectory(file);
	}

	public final void refresh() {
		try {
            this.setAllCommits();
        } catch (GitAPIException | IOException e) {
            e.printStackTrace();
        }
	}

	private final void setGitDirectory(File gitDir) throws NullPointerException {
		if (gitDir == null) {
			throw new NullPointerException();
		}
		if (! gitDir.getName().equals(".git")) {
			if (this.hasGitFolder(gitDir)) {
				this.gitDir = new File(gitDir.getAbsolutePath(), ".git");
			} else {
				throw new NullPointerException();
			}
		} else {
			this.gitDir = gitDir;
		}
		try {
			this.setGitRepository(this.gitDir);
			this.setAllCommits();
		} catch (IOException | GitAPIException e) {
			e.printStackTrace();
		}
	}

	private final boolean hasGitFolder(File file) {
		List<File> fileList = Arrays.asList(file.listFiles());
		for (File tmp: fileList) {
			if (tmp.getName().equals(".git") && tmp.isDirectory()) {
				return true;
			}
		}
		return false;
	}

	private Repository repository;
	private final void setGitRepository(File gitDir) throws IOException {
		this.repository = new FileRepositoryBuilder().setGitDir(gitDir)
//				.readEnvironment().findGitDir() // It does not work in Win10
				.build();
	}

    private ArrayList<RevCommit> commitList = new ArrayList<>();
	private HashMap<RevCommit, RevCommit> commitDiffPair = new HashMap<>();
	private ObjectId headId;
	private ObjectId firstId;
	private final void setAllCommits() throws NoHeadException, GitAPIException, IOException {
		this.commitList.clear();
		this.commitDiffPair.clear();
		try (Git git = new Git(this.repository)) {
	        Iterable<RevCommit> commits = null;
	        try {
	        	commits = git.log().all().call();
	        } catch (NullPointerException e) {
	        	commits = git.log().call();
	        }
	        for (RevCommit tmpCommit : commits) {
	        	this.commitList.add(tmpCommit);
	        }
	        for (int i = 0; i < this.commitList.size() - 1; i++) {
	        	this.commitDiffPair.put(this.commitList.get(i), this.commitList.get(i+1));
	        }
			this.headId = this.commitList.get(0);
			this.firstId = this.commitList.get(this.commitList.size() - 1);
		}
	}

    public final ObjectId getHeadId() {
        try {
            ObjectId head = repository.resolve(Constants.HEAD);
            if (!head.equals(this.headId)) {
                try {
                    this.setAllCommits();
                    if (!head.equals(this.headId)) {
                        this.headId = head;
                    }
                } catch (GitAPIException e) {
                    e.printStackTrace();
                }
            }
            return this.headId;
        } catch (RevisionSyntaxException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public final ObjectId getFirstId() {
        return this.firstId;
    }

    public final ArrayList<RevCommit> getCommitsList() {
    	return this.commitList;
    }

    public final HashMap<RevCommit, RevCommit> getCommitsDiffPair() {
    	return this.commitDiffPair;
    }

	public final File getDiffFile(String outPutFile, RevCommit rev, Language language) throws IOException {
		File tmpFile = new File(outPutFile);
		return this.getDiffFile(tmpFile, rev, language);
	}

	public final File getDiffFile(File tmpFile, RevCommit commit, Language language) throws IOException {
		RevCommit toCommit = null;
		for (RevCommit entry : this.commitList) {
			if (entry.getId().equals(commit.getId())) {
				toCommit = entry;
			}
		}
		if (toCommit == null || toCommit.equals(this.firstId)) {
			return null;
		}
		RevCommit fromCommit = this.commitDiffPair.get(commit);
		return this.createDiffFile(tmpFile, fromCommit, toCommit, language);
	}

	private final File createDiffFile(File outPut, RevCommit fromCommit, RevCommit toCommit, Language language) throws IOException {
		FileOutputStream outStream = new FileOutputStream(outPut);
        RevTree fromTree = fromCommit.getTree();
        RevTree toTree = toCommit.getTree();
    	try (ObjectReader reader = this.repository.newObjectReader()) {
    		CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
    		oldTreeIter.reset(reader, fromTree);
    		CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
    		newTreeIter.reset(reader, toTree);
    		try (Git git = new Git(this.repository)) {
    			ArrayList<DiffEntry> diffs = (ArrayList<DiffEntry>) git.diff().setNewTree(newTreeIter).setOldTree(oldTreeIter).call();
    			List<DiffEntry> trimed = this.diffTrim(diffs, language);
				DiffFormatter diffFormatter = new DiffFormatter(outStream);
    	        diffFormatter.setRepository(this.repository);
    	        diffFormatter.format(trimed);
    	        diffFormatter.close();
    		} catch (GitAPIException e) {
    			e.printStackTrace();
    		} finally {
    			outStream.close();
    		}
    	} catch (IncorrectObjectTypeException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
		return outPut;
	}

	private final List<DiffEntry>  diffTrim(List<DiffEntry> diffList, Language language) {
		ArrayList<DiffEntry> array = new ArrayList<>();
		if (language == null) {
			return diffList;
		}
		for (String extension : language.getExtension()) {
			for (DiffEntry entry : diffList) {
				if (entry.getNewPath().endsWith(extension)) {
					array.add(entry);
				}
			}
		}
		return array;
	}

	public final boolean commitExists() {
		if (this.commitList == null || this.commitDiffPair == null) {
			return false;
		} else if (this.commitList.size() <= 1) {
			return false;
		} else {
			return true;
		}
	}


	public static void main(String[] args) {
		File file = new File(System.getProperty("user.dir"));
		System.out.println(file.getAbsolutePath());
		GitCommitDiff git = new GitCommitDiff(file);
		System.out.println(git.repository);
		try {
			git.setAllCommits();
			System.out.println(git.commitList);
			System.out.println(git.commitDiffPair.get(git.commitList.get(0)).getCommitTime());
			System.out.println(git.commitList.get(0).getCommitTime());
			git.createDiffFile(new File("./diff.txt"), git.commitDiffPair.get(git.commitList.get(0)), git.commitList.get(0), Language.JAVA);
		} catch (GitAPIException | IOException e) {
			e.printStackTrace();
		}
	}

    // public void getDiffHead(File tmpFile, Language language) {
    // }

}
