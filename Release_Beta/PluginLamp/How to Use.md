# How to Use - CodeLamp.PluginLamp

Now, this works on only Windows (64bit)

## install as Eclipse-plugin

Copy "plugins" folder to Eclipse's "plugins" folder. 

Now, "codelamp.pluginlamp.externalprocess" folder has external application "CRM114" binary (windows),
and default two database. 
When use other database created by other app of "CodeLamp", replace these default database.

## Start this on Eclipse

Open PluginLamp View like 

1. Select the Menu "Window" on toolbar
1. Select "view"
1. Select "others", pop up views dialog
1. Select "pluginlamp" in the "others" group
1. Then, Select Project from "pluginlamp" view on workbench
1. When first time, copy default database to "codelamp/" under the project directory

## Function

- (Auto) Classify Bugs in current commit ( = HEAD commit)
- (Mannually) Learn Bugs or not code to databse 
- Records classified informations since start eclipse (or this plugin)
- Show "git diff" on right field, when double click commit or diffentry.