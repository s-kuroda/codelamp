package others;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import codelamp.util.EpocDateTime;

public class GitLogTable
		extends Composite implements SelectionListener {
	private Table table;
	private ArrayList<TableColumn> column = new ArrayList<TableColumn>();
	private ArrayList<RevCommit> commitList = null;
	public void setCommitList(ArrayList<RevCommit> list) {
		this.commitList = list;
		this.setTableItem();
	}
	private final ArrayList<String> COLUMN_TEXT = new ArrayList<>();
	{
//		COLUMN_TEXT.add("");
		COLUMN_TEXT.add("Learn as");
		COLUMN_TEXT.add("Commit Date");
		COLUMN_TEXT.add("Commit Message");
	}
	
	private final ArrayList<String> LEARN_TEXT = new ArrayList<>();
	{
		LEARN_TEXT.add("");
		LEARN_TEXT.add("BUG");
		LEARN_TEXT.add("NOT BUG");
	}
	public GitLogTable(Composite parent, int style) {
		super(parent, style);
		this.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.setLayout(new GridLayout(1,false));
        Label tableLabel = new Label(this,SWT.NONE);
        tableLabel.setText("Git Reflog");
        table = new Table(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION | SWT.CHECK);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.table.setHeaderVisible(true);
        TableColumn checkCol = new TableColumn(this.table,SWT.LEFT);
        checkCol.setText("");
        checkCol.setWidth(50);
        checkCol.addSelectionListener(this);
        column.add(checkCol);
        COLUMN_TEXT.forEach(e ->{
            TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
            tmpCol.setText(e);
            column.add(tmpCol);
        });
        this.setTableItem();
	}

    protected final void setTableItem(){
        this.table.removeAll();
        try {
	        if(commitList == null) {
	            this.resizeTable();
	            return;
	            /* View Test */
//	            TableItem item = new TableItem(this.table,SWT.NULL);
//	            String[] itemData = new String[6];
//	        	for (int i = 1; i < 6; i++) {
//	        		itemData[i] = LEARN_TEXT.get(2);
//	        	}
//                itemData[2] = EpocDateTime.convertEpocToString(new Date().getTime());
//                itemData[3] = "bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n";
//	            item.setText(itemData);
//	            item = new TableItem(this.table,SWT.NULL);
//	            item.setText(LEARN_TEXT.toArray( new String[3]));
	        } else {
	        	commitList.forEach(e -> {
	                TableItem item = new TableItem(this.table,SWT.NULL);
	                String[] itemData = new String[4];
	                itemData[0] = "";
	                itemData[1] = LEARN_TEXT.get(0);
	                itemData[2] = EpocDateTime.convertEpocToString(e.getCommitTime());
	                itemData[3] = e.getShortMessage();
	                item.setText(itemData);
	        	});
	        }
        } catch (NullPointerException e) {
        	System.out.println("Target : null");
        }
        this.resizeTable();
    }

    private final void resizeTable(){
        this.table.setVisible(false);
        TableColumn[] columns = this.table.getColumns();
        Arrays.stream(columns).forEach(e -> e.pack());
        columns[0].setWidth(30);
//        columns[1].setWidth(70);
        this.table.setVisible(true);
    }
	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
      Object obj = event.getSource();
      if (obj == this.column.get(0)) {
    	  reverseSelected();
      }
	}
	
	public final void clearSelected() {
		TableItem[] items = table.getItems();
		Arrays.stream(items).forEach(e -> e.setChecked(false));
	}
	
	public final void reverseSelected() {
		TableItem[] items = table.getItems();
		Arrays.stream(items).forEach(e -> e.setChecked(! e.getChecked()));
	}

	public final void setSelectedState(boolean isBug) {
		TableItem[] items = table.getItems();
		Arrays.stream(items).filter(e -> e.getChecked()).forEach(e -> {
			e.setChecked(false);
			e.setText(1, (isBug)? LEARN_TEXT.get(1): LEARN_TEXT.get(2));
		});
	}
    public final void cancelState() {
        TableItem[] items = table.getItems();
        Arrays.stream(items).filter(e -> e.getChecked()).forEach(e -> {
            e.setChecked(false);
            e.setText(1,LEARN_TEXT.get(0));
        });
    }
    public final void cancelStateAll() {
        TableItem[] items = table.getItems();
        Arrays.stream(items).forEach(e -> {
            e.setChecked(false);
            e.setText(1,LEARN_TEXT.get(0));
        });
    }
	public final HashMap<RevCommit, Boolean> getTargetCommits() {
		TableItem[] items = this.table.getItems();
		HashMap<RevCommit, Boolean> map = new HashMap<>();
		for (int i = 0; i < items.length; i++) {
			if (items[i].getText(1).equals(LEARN_TEXT.get(1))) {
				map.put(commitList.get(i), false);
			} else if (items[i].getText(1).equals(LEARN_TEXT.get(2))) {
				map.put(commitList.get(i), true);
			}
		}
		return map;
	}
	
	@Override
	public void dispose() {
	    this.column.forEach(e -> e.dispose());
	    this.table.dispose();
	    super.dispose();
	}
	
	public static void main(String[] args) {
        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setText("CodeLamp --- Fault-Prone Filtering Plugin");
        shell.setLayout(new GridLayout(1,false));
        Menu menu = new Menu(shell, SWT.BAR);
        shell.setMenuBar(menu);
        MenuItem configButton = new MenuItem(menu, SWT.PUSH);
        configButton.setText("Setting");
        configButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e){
            	System.out.println("MENU BUTTON SELECTED");
            	System.out.println(shell.getSize());
            }
        });
        ArrayList<File> tmp = new ArrayList<>();
        tmp.add(new File("first"));
        tmp.add(new File("second"));
        GitLogTable composite = new GitLogTable(shell, SWT.BORDER);
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        shell.setSize(800,450);
        shell.open();
        while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                        display.sleep();
                }
        }
        display.dispose();
    }
}
