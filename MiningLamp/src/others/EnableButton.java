package others;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class EnableButton extends Composite {

	private static final String ON = "ON";
	private static final String OFF = "OFF";

	private Button btn;
	private Label label;

	public EnableButton(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		btn = new Button(this, SWT.TOGGLE);
		btn.setText(OFF);
		btn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (btn.getSelection()) {
					btn.setText(ON);
				} else {
					btn.setText(OFF);
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {

			}
		});
		label = new Label(this, SWT.NONE);
		label.setText("");
		this.pack();
	}

	public void setText(String text) {
		label.setText(text);
		this.pack();
	}

    public void addSelectionListener(SelectionListener listener) {
    	btn.addSelectionListener(listener);
    }

    public boolean getSelection() {
    	return btn.getSelection();
    }

    public void setSelection(boolean selected) {
    	btn.setSelection(selected);
    }

	@Override
	public void dispose() {
	    this.btn.dispose();
	    this.label.dispose();
	    super.dispose();
	}

//	   public static void main(String[] args) {
//	       System.out.println(System.getProperty("user.dir"));
//	         Display display = Display.getDefault();
//	         Shell shell = new Shell();
//	         shell.setLayout(new GridLayout(1, false));
//	         EnableButton f = new EnableButton(shell,SWT.NONE);
//	         f.setText("Auto Classify");
//	         shell.pack();
//	         shell.open();
//	         shell.setSize(600,500);
//	         while (!shell.isDisposed()) {
//	             if (!display.readAndDispatch())
//	                 display.sleep();
//	         }
//	         f.dispose();
//	         display.dispose();
//		   Path path = Paths.get(System.getProperty("user.dir"));
//		   System.out.println(path.toString());
//	     }
}
