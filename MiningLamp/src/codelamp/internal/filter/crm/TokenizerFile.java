package codelamp.internal.filter.crm;

import java.io.File;

import codelamp.pio.Language;

@SuppressWarnings("serial")
public final class TokenizerFile extends File {

	public TokenizerFile(String pathname, String language) {
		super(pathname);
        this.language = Language.pase(language);
	}
	public TokenizerFile(String parent, String child, String language) {
		super(parent, child);
        this.language = Language.pase(language);
	}
	public TokenizerFile(File file, String language) {
		this(file.getAbsolutePath(), language);
	}
	public TokenizerFile(File parent, String child, String language) {
		this(parent.getAbsolutePath(), child, language);
	}

	private final Language language;
	public Language getLanguage() {
		return language;
	}

}
