package codelamp.internal.filter;

import java.io.File;
import java.io.IOException;

import codelamp.internal.filter.crm.CRM;
import codelamp.pio.FilterGroup;

public final class Discriminator {
    
    public final static File create(String path) {
        File file = CRM.create(path);
        return file;
    }

    public final static void learn(File tmpFile, Boolean value, FilterGroup filter) {
        try {
            CRM.learn(tmpFile, value, filter);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
    }
    
}
