package codelamp.util;

import java.math.BigInteger;

public class HexId {
	private static int ID_LENGTH = 9;
	public static final long setLength(int length) {
		ID_LENGTH = length;
		ID_MAX = limitFromHexLength();
		return ID_MAX;
	}

	private static long ID_MAX = limitFromHexLength();
	private static final long limitFromHexLength(int length) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			sb.append("F");
		}
		BigInteger bi = new BigInteger(sb.toString(), 16);
		return bi.longValue();
	}
	private static final long limitFromHexLength() {
		return limitFromHexLength(ID_LENGTH);
	}

	public static final long resizeId(long id) {
		if (ID_MAX < id || id < 0) {
			return -1;
		} else {
			return id;
		}
	}

	public static final long resizeId(String id) {
		if (id == null) {
			return -1;
		} else if (ID_LENGTH  < id.length()) {
			id = id.substring(0, ID_LENGTH);
		}
		try {
			BigInteger bi = new BigInteger(id, 16);
			return resizeId(bi.longValue());
		} catch (NumberFormatException e) {
			return -1;
		}
	}

}
