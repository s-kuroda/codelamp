package codelamp.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GitShowEdit {

	private GitShowEdit() {
	}

	private static final String OLD_ENTRY = "---";
	private static final String NEW_ENTRY = "+++";
	private static final String ADDED_LINE = "+";
	private static final String DELETED_LINE = "-";
	private static final String DEF_PREFIX = "GitShowEdited-";
	private static final String DEF_SUFFIX = "";
	private static final String[] COMMENT_PREFIX = {"/*", "//", "*"};

	private static String getExtension(Path path) {
		String name = path.getFileName().toString();
		int index = name.lastIndexOf(".");
		if (index > 0) {
			return name.substring(index, name.length());
		} else {
			return "";
		}
	}

	private static String getNameWithouExtension(Path path, String extension) {
		String name = path.getFileName().toString();
		if (extension == null || extension.equals("")) {
			return name;
		}
		int index = name.lastIndexOf(extension);
		if (index > 0) {
			return name.substring(0, index);
		} else {
			return "";
		}
	}

	private static Path renamedPath(Path oldPath, String prefix, String suffix) {
		if (oldPath == null || !Files.exists(oldPath)) {
			throw new NullPointerException();
		}
		String path = oldPath.toAbsolutePath().getParent().toString();
		String extension = getExtension(oldPath);
		StringBuffer newName = new StringBuffer();
		if (prefix == null || prefix.equals("")) {
			newName.append(DEF_PREFIX);
		} else {
			newName.append(prefix);
		}
		newName.append(getNameWithouExtension(oldPath, extension));
		if (suffix != null && !suffix.equals("")) {
			newName.append(suffix);
		}
		if (extension != null && !extension.equals("")) {
			newName.append(extension);
		}
		return Paths.get(path, newName.toString());
	}

	private static boolean isTrim(String str, String[] prefixs) {
		if (str == null || prefixs == null || prefixs.length <= 0) {
			throw new NullPointerException();
		}
		for (String entry : prefixs) {
			if (str.startsWith(OLD_ENTRY) || str.startsWith(NEW_ENTRY)) {
				return false;
			} else if (str.startsWith(entry)) {
				return false;
			}
		}
		return true;
	}

	private static boolean isTrim(String str, String prefix) {
		if (str == null || prefix == null) {
			throw new NullPointerException();
		}
		if (str.startsWith(OLD_ENTRY) || str.startsWith(NEW_ENTRY)) {
			return false;
		} else if (str.startsWith(prefix)) {
			return false;
		} else {
			return true;
		}
	}

	private static Path TrimText(Path oldPath, String[] prefixOfDeleteLine, String prefix, String suffix) {
		if (oldPath == null || !Files.exists(oldPath)) {
			throw new NullPointerException();
		}
		Path newPath = renamedPath(oldPath, prefix, suffix);
		System.out.println(newPath.toString());
		BufferedReader buffReader = null;
		BufferedWriter buffWriter = null;
		try {
			if (Files.exists(newPath)) {
				Files.delete(newPath);
			}
			Files.createFile(newPath);
			buffReader =  Files.newBufferedReader(oldPath, Charset.forName("UTF-8"));
	    	buffWriter =  Files.newBufferedWriter(newPath, Charset.forName("UTF-8"));
	    	String str = buffReader.readLine();
	    	if (prefixOfDeleteLine != null && prefixOfDeleteLine.length == 1) {
			    	while(str!= null){
		    			System.out.println(str);
			    		if (isTrim(str, prefixOfDeleteLine[0])){
			    			if (!isTrim(str, ADDED_LINE) || !isTrim(str, DELETED_LINE)) {
			    				if (!isComment(str.substring(1))) {
					    			System.out.println(str);
					    			buffWriter.write(str.substring(1));
			    				}
			    			}
			    		}
			    		buffWriter.newLine();
//			    		try {
			    			str = buffReader.readLine();
//			    		} catch (MalformedInputException exception){
//			    			str = buffReader.readLine();
//			    		}
					}
		    } else {
		    	while((str = buffReader.readLine()) != null){
		    		if (isTrim(str, prefixOfDeleteLine)){
		    			if (!isTrim(str, ADDED_LINE) || !isTrim(str, DELETED_LINE)) {
		    				if (!isComment(str.substring(1))) {
				    			System.out.println(str);
				    			buffWriter.write(str.substring(1));
		    				}
		    			}
		    		}
		    		buffWriter.newLine();
				}
		    }
			buffWriter.flush();
			buffReader.close();
			buffWriter.close();
			return newPath;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}


	public static Path TrimDiffText(Path oldPath, boolean isAddedText, String prefix, String suffix) {
		String deletePrefix = (isAddedText)? DELETED_LINE : ADDED_LINE;
		String[] prefixOfDeleteLine = {deletePrefix};
		return TrimText(oldPath, prefixOfDeleteLine, prefix, suffix);
	}

	public static Path TrimDiffText(Path oldPath, boolean isAddedText, String prefix) {
		return TrimDiffText(oldPath, isAddedText, prefix, DEF_SUFFIX);
	}

	public static Path TrimDiffText(Path oldPath, boolean isAddedText) {
		return TrimDiffText(oldPath, isAddedText, DEF_PREFIX, DEF_SUFFIX);
	}

	public static Path DeleteDiffText(Path oldPath, String prefix, String suffix) {
		String[] prefixOfDeleteLine = {DELETED_LINE, ADDED_LINE};
		return TrimText(oldPath, prefixOfDeleteLine, prefix, suffix);
	}

	public static Path DeleteDiffText(Path oldPath, String prefix) {
		return DeleteDiffText(oldPath, prefix, DEF_SUFFIX);
	}

	public static Path DeleteDiffText(Path oldPath) {
		return DeleteDiffText(oldPath, DEF_PREFIX, DEF_SUFFIX);
	}

	private static boolean isComment(String str) {
		String tmp = str.trim();
		for (String entry : COMMENT_PREFIX) {
			if (tmp.startsWith(entry)) {
				return true;
			}
		}
		return false;
	}
	public static void main(String[] args) {
		Path path = Paths.get("D:/Users/ZiTsP/Downloads/mina/NON-BUG.apdb");
		Path newPath =TrimDiffText(path, true);
		System.out.println(path.toString());
	}
}
