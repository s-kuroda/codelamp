package codelamp.util;

import java.io.File;

public class ApplicationPath {

    public static final File root = new File(System.getProperty("user.dir"));
    
/* for Eclipse-Plugin
    public static final File root = new File(System.getProperty("user.dir"));
*/

}
