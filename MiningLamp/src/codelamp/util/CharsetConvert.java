package codelamp.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.MalformedInputException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CharsetConvert {

	private CharsetConvert() {
	}


	private static final Charset WindowsSJIS = Charset.forName("Windows-31J");
	private static final Charset UTF8 = Charset.forName("UTF-8");

	/**
	 * Convert TextFile(Windows Shift-JIS (Windows-31J)) to NewTextFile(UTF-8)
	 * @param oldPath
	 * @param newPath
	 * @return
	 */
	public static void convertMS932ToUTF8(Path oldPath, Path newPath) {
		BufferedReader buffReader = null;
		BufferedWriter buffWriter = null;
		try {
			if (Files.exists(newPath)) {
				Files.delete(newPath);
			}
			Files.createFile(newPath);
			buffReader =  Files.newBufferedReader(oldPath, WindowsSJIS);
	    	buffWriter =  Files.newBufferedWriter(newPath, UTF8);
	    	String str = buffReader.readLine();
			while((str = buffReader.readLine()) != null){
    			buffWriter.write(new String(str.getBytes(WindowsSJIS), UTF8));
			}
			buffWriter.flush();
			buffReader.close();
			buffWriter.close();
		} catch (MalformedInputException e){
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
