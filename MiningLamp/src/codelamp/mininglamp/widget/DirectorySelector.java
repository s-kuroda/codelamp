package codelamp.mininglamp.widget;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class DirectorySelector extends Composite {

	private Text text;
	private Button button;
	private static final String OPEN_DIR = "OPEN";

	public DirectorySelector(Composite parent, int widgetStyle) {
		super(parent, widgetStyle);
		this.setLayout(new GridLayout(2, false));
        text = new Text(this, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
        text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        button = new Button(this, SWT.PUSH);
        button.setText(OPEN_DIR);
        button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
	            try {
	                DirectoryDialog dialog = new DirectoryDialog(DirectorySelector.this.getShell());
	                Path path = Paths.get(dialog.open());
	                text.setText(path.toAbsolutePath().toString());
	            } catch (NullPointerException e) {
	            }
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
        this.pack();
	}

    public void clearText() {
        text.setText("");
    }

    public void setInput(String absolutePath) {
    	Path path = Paths.get(absolutePath);
    	if (Files.exists(path)) {
    		text.setText(path.toAbsolutePath().toString());
    	} else {
    		text.setText("");
    	}
    }

    public String getText() {
        return text.getText();
    }
    public void setText(String message) {
        text.setText(message);
    }

	public void setEnable(boolean lock) {
		button.setEnabled(!lock);
	}
    public void dispose() {
    	this.text.dispose();
    	this.button.dispose();
    	super.dispose();
    }
    public void addBtnListener(SelectionListener listener) {
    	button.addSelectionListener(listener);
    }

	   public static void main(String[] args) {
	       System.out.println(System.getProperty("user.dir"));
	         Display display = Display.getDefault();
	         Shell shell = new Shell();
	         shell.setLayout(new GridLayout(1, false));
	         DirectorySelector f = new DirectorySelector(shell,SWT.NONE);
//	         f.setText("Auto Classify");
	         shell.pack();
	         shell.open();
	         shell.setSize(600,500);
	         while (!shell.isDisposed()) {
	             if (!display.readAndDispatch())
	                 display.sleep();
	         }
	         f.dispose();
	         display.dispose();
		   Path path = Paths.get(System.getProperty("user.dir"));
		   System.out.println(path.toString());
	     }

}


