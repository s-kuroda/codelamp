package codelamp.mininglamp.widget;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import codelamp.mininglamp.internal.MiningLampCore;
import codelamp.pio.Language;

public class MiningLampUI extends Composite {

	private final DirectorySelector projectSelector;
	private final CheckerTable table;
	private final LanguageSelector langSelector;
	private final LinerButtons btns;
	private final GroupedPathSelector faultyPath;
	private final GroupedPathSelector accuratePath;
	private final FootComposite foot;
	private final MiningLampCore core;

	public MiningLampUI(Composite parent, int style) {
		super(parent, style);
		core = new MiningLampCore(this);
		this.setLayout(new GridLayout(2, false));
		GridData wide = new GridData(GridData.FILL_HORIZONTAL);
		wide.horizontalSpan = 2;
		projectSelector = new DirectorySelector(this, SWT.BORDER);
		projectSelector.setLayoutData(wide);
		table = new CheckerTable(this, SWT.BORDER);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.FILL_VERTICAL | GridData.HORIZONTAL_ALIGN_END));
		composite.setLayout(new GridLayout(1, false));
		langSelector = new LanguageSelector(composite, SWT.NONE);
		langSelector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		btns = new LinerButtons(composite, SWT.NONE);
		btns.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		GridData wide2 = new GridData(GridData.FILL_HORIZONTAL);
		wide2.horizontalSpan = 2;
		faultyPath = new GroupedPathSelector(this, SWT.NONE, SWT.SAVE);
		faultyPath.setLayoutData(wide2);
		accuratePath = new GroupedPathSelector(this, SWT.NONE, SWT.SAVE);
		accuratePath.setLayoutData(wide2);
		foot = new FootComposite(this, SWT.NONE);
		foot.setLayoutData(wide2);
		init();
		this.pack();
		setDnDGitDir(this);
	}

	private class FootComposite extends Composite {
		private final LearnProgress progress;
		private final Button startBtn;

		public FootComposite(Composite parent, int style) {
			super(parent, style);
			this.setLayout(new GridLayout(2, false));
			progress = new LearnProgress(this, SWT.BORDER);
			progress.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			startBtn = new Button(this, SWT.PUSH);
			startBtn.setLayoutData(new GridData(GridData.FILL_VERTICAL));
		}
	}

	private void setDnDGitDir(Control control){
        DropTarget target = new DropTarget(control,DND.DROP_DEFAULT|DND.DROP_COPY);
        FileTransfer transfer = FileTransfer.getInstance();
        Transfer[] types = new Transfer[]{transfer};
        target.setTransfer(types);
        target.addDropListener(new DropTargetAdapter() {
            public void dragEnter(DropTargetEvent evt){
            	evt.detail = DND.DROP_COPY;
            }
            public void drop(DropTargetEvent evt){
                String[] files = (String[]) evt.data;
				if (core.setProject(files[0])) {;
					projectSelector.setInput(files[0]);;
				}
            }
        });
	}

	private static final String OPEN_GITDIR_TEXT = "Choose a Git Repository";
	private static final List<String> COLUMN_LABEL = Arrays.asList("isBUG", "ID", "Date", "Tags", "Message");
	private static final String FAULTY_BUTTON_TEXT = "Faulty";
	private static final String ACCURATE_BUTTON_TEXT = "Non-Faulty";
	private static final String CLEAR_BUTTON_TEXT = "CLEAR";
	private static final String FAULTY_PATH_MSG = "Faulty DataBase Path";
	public static final String FAULTY_EXTENSION = ".fpdb";
	private static final String ACCURATE_PATH_MSG = "Non-Faulty DataBase Path";
	public static final String ACCURATE_EXTENSION = ".apdb";
	private static final String START_BUTTON_TEXT = "START";

	private void init() {
		projectSelector.setText(OPEN_GITDIR_TEXT);
		projectSelector.addBtnListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if (!core.setProject(projectSelector.getText())) {;
					projectSelector.clearText();
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		table.setColumns(COLUMN_LABEL);
		langSelector.setInput(Language.getAll());
		btns.addButton(FAULTY_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.getSelectedItems().forEach(e -> {
					core.setSign(e, true);
				});
				table.setCheckAll(false);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.addButton(ACCURATE_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.getSelectedItems().forEach(e -> {
					core.setSign(e, false);
				});
				table.setCheckAll(false);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.addButton(CLEAR_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.getSelectedItems().forEach(e -> {
					core.clearSign(e);
				});
				table.setCheckAll(false);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.setEnabledAll(false);
		faultyPath.setGroupText(FAULTY_PATH_MSG);
		faultyPath.setExtension(FAULTY_EXTENSION);
		accuratePath.setGroupText(ACCURATE_PATH_MSG);
		accuratePath.setExtension(ACCURATE_EXTENSION);
		foot.startBtn.setText(START_BUTTON_TEXT);
		foot.startBtn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (projectSelector.getText() == null || projectSelector.getText().equals("")) {
					foot.startBtn.setEnabled(false);
					return;
				} else if (faultyPath.getText() == null || faultyPath.getText().equals("")) {
					return;
				} else if (accuratePath.getText() == null || accuratePath.getText().equals("")) {
					return;
				} else if (!Files.exists(Paths.get(faultyPath.getText()).getParent())
						|| !Files.exists(Paths.get(accuratePath.getText()).getParent())) {
					return;
	    		} else {
	    			core.launch();
	    		}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		foot.startBtn.setEnabled(false);
	}


	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
//		shell.setLayout(new FillLayout());
		shell.setLayout(new GridLayout(1, false));
		MiningLampUI cc = new MiningLampUI(shell, SWT.BORDER);
		cc.setLayoutData(new GridData(GridData.FILL_BOTH));
		shell.pack();
		shell.open();
		shell.setSize(600, 600);
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}

	public void addInput(List<String> newItem, Object data) {
		table.addInput(newItem, data);
	}
	public void setEnabledAllBtn(boolean enabled) {
		btns.setEnabledAll(enabled);
		foot.startBtn.setEnabled(enabled);
	}


	public List<String> getLanguage() {
		return langSelector.getLanguge().getExtension();
	}


	public Path getPath(boolean isFaultyPath) {
		if (isFaultyPath) {
			return faultyPath.getPath();
		} else {
			return accuratePath.getPath();
		}
	}
	public void setPath(String path, boolean isFaultyPath) {
		if (isFaultyPath) {
			faultyPath.setPath(path);
		} else {
			accuratePath.setPath(path);
		}
	}
}
