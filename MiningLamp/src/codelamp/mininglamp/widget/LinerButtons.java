package codelamp.mininglamp.widget;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class LinerButtons
		extends Composite {

	private ArrayList<Button> btns = new ArrayList<>();

	public void addButton(String text, int style, SelectionListener listener) {
		Button btn = new Button(this, style);
		btn.setText(text);
		btn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		if (listener != null) {
			btn.addSelectionListener(listener);
		}
		this.btns.add(btn);
		this.pack();
	}

	public LinerButtons(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(1, false));
		this.pack();
	}

	public void setEnabledAll(boolean enabled) {
		this.btns.forEach(e -> e.setEnabled(enabled));
	}

	public void setEnabled(int btnIndex, boolean enabled) {
		if (btns.size() <= btnIndex) {
			return;
		}
		btns.get(btnIndex).setEnabled(enabled);
	}

	@Override
	public void dispose() {
	    this.btns.forEach(e -> e.dispose());
	    super.dispose();
	}

	public static void main (String [] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		LinerButtons cc = new LinerButtons(shell, SWT.BORDER);
		cc.addButton("BTN1", SWT.PUSH, null);
		cc.addButton("BTN2", SWT.PUSH, null);
		Button btn2 = new Button(shell, SWT.PUSH);
		btn2.setText("ANOTHER");
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}
}
