package codelamp.mininglamp.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import codelamp.mininglamp.internal.CommitTrace;
import codelamp.util.EpocDateTime;

public class CommitsTable2 extends Composite {

	private final static String[] COLUMN_LABEL =
		{"isBUG", "ID", "Date", "Tags", "Message"};

	private enum IS_FAULTY {Y, N, YN};


	private Table table;
	private ArrayList<TableColumn> column = new ArrayList<TableColumn>();

	public CommitsTable2(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1,false));
        table = new Table(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION | SWT.CHECK);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        setColumns();
    	this.resizeTable();
	}

	private void setColumns() {
        this.table.setHeaderVisible(true);
        TableColumn checkCol = new TableColumn(this.table,SWT.LEFT);
        checkCol.setText("");
        checkCol.setWidth(30);
        column.add(checkCol);
        Arrays.stream(COLUMN_LABEL).forEach(e ->{
            TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
            tmpCol.setText(e);
            column.add(tmpCol);
        });
	}

    private final void resizeTable(){
        this.table.setVisible(false);
        TableColumn[] columns = this.table.getColumns();
        Arrays.stream(columns).forEach(e -> e.pack());
        columns[0].setWidth(30);
        this.table.setVisible(true);
    }

    private class TagCommit {
    	public RevCommit commit;
    	public List<Ref> tag = new ArrayList<>();
    	public IS_FAULTY faulty;
    	public TagCommit(RevCommit commit) {
    		this.commit = commit;
    	}
    	@Override
    	public boolean equals(Object obj) {
    		if (obj == null) {
    			return false;
    		} else if (obj instanceof TagCommit) {
    			return (commit.equals(((TagCommit) obj).commit))? true : false;
    		} else if (obj instanceof RevCommit) {
    			return (commit.equals((RevCommit) obj))? true : false;
    		} else {
    			return false;
    		}
    	}
    }

	public ArrayList<RevCommit> getSelected() {
		ArrayList<RevCommit> list = new ArrayList<>();
		TableItem[] items = table.getItems();
		Arrays.stream(items).filter(e -> e.getChecked()).forEach(e -> {
			TagCommit data = (TagCommit) e.getData();
			list.add(data.commit);
		});
		items = table.getSelection();
		Arrays.stream(items).forEach(e -> {
			TagCommit data = (TagCommit) e.getData();
			list.add(data.commit);
		});
		return list;
	}

    private ArrayList<TagCommit> items = new ArrayList<>();
	public void setInputCommit(List<RevCommit> commitList) {
		items.clear();
		commitList.forEach(e -> {
			TagCommit commit = new TagCommit(e);
			items.add(commit);
		});
		setTableItem();
	}

	public void setInputTag(List<Ref> tags) {
		if (items == null || items.isEmpty()) {
			return;
		}
		tags.forEach(e -> {
			if (e.getPeeledObjectId() != null) {
				for (TagCommit entry : items) {
					if (entry.commit.getId().equals(e.getPeeledObjectId())) {
						entry.tag.add(e);
						break;
					}
				}
			}
		});
		setTableItem();
	}
	public void setInputFaultyMarker(List<RevCommit> list, boolean isFaulty) {
		list.forEach(e -> {
			if (items != null && !items.isEmpty()) {
				if (items.contains(e)) {
					IS_FAULTY f = items.get(items.indexOf(e)).faulty;
					if (f == null) {
						items.get(items.indexOf(e)).faulty = (isFaulty? IS_FAULTY.Y : IS_FAULTY.N);
					} else if ((f == IS_FAULTY.N && isFaulty) || (f == IS_FAULTY.Y && !isFaulty)) {
						items.get(items.indexOf(e)).faulty = IS_FAULTY.YN;
					}
				}
			}});
		setTableItem();
	}
	public void setInputMarkedId(List<ObjectId> list, boolean isFaulty) {
		if (items == null || items.isEmpty()) {
			return;
		}
		list.forEach(e -> {
			for (TagCommit entry : items) {
				if (entry.commit.getId().equals(e)) {
					if (entry.faulty == null) {
						entry.faulty = (isFaulty? IS_FAULTY.Y : IS_FAULTY.N);
					} else if ((entry.faulty == IS_FAULTY.N && isFaulty) || (entry.faulty == IS_FAULTY.Y && !isFaulty)) {
						entry.faulty = IS_FAULTY.YN;
					}
					break;
				}
			}
		});
		setTableItem();
	}
	public void clearFaultyMarker() {
		if (items != null && !items.isEmpty()) {
			items.forEach(e -> {
				e.faulty = null;
			});
		}
		setTableItem();
	}

    public void setTableItem(){
        this.table.removeAll();
        try {
	        if(items != null) {
	        	items.forEach(e -> {
	                TableItem item = new TableItem(this.table,SWT.NULL);
	                String[] itemData = new String[6];
	                itemData[0] = "";
	                if (e.faulty != null) {
	                	itemData[1] = e.faulty.toString();
	                }
	                itemData[2] = CommitTrace.limitIdLength(e.commit.getId());
	                itemData[3] = EpocDateTime.convertEpocToString(e.commit.getCommitTime());
	                if (e.tag != null && !e.tag.isEmpty()) {
	                	StringBuffer str = new StringBuffer();
	                	e.tag.forEach(f -> str.append(CommitTrace.getTagNameFromRef(f)));
	                	itemData[4] = String.join(" , ", str);
	                }
	                itemData[5] = e.commit.getShortMessage();
	                item.setText(itemData);
	                item.setData(e);
	        	});
	        }
        } catch (NullPointerException e) {
        	System.out.println("Target : null");
        }
        this.resizeTable();
    }

	  public static void main (String [] args) {
		    Display display = new Display();
		    final Shell shell = new Shell(display);
		    shell.setLayout(new FillLayout());
		    CommitsTable2 c = new CommitsTable2(shell, SWT.BORDER);
		    shell.pack();
		    shell.open();
		    while (!shell.isDisposed()) {
		      if (!display.readAndDispatch()) {
		        display.sleep();
		      }
		    }
		    display.dispose();
    }
}
