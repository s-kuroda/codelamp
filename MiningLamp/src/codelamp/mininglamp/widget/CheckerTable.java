package codelamp.mininglamp.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class CheckerTable extends Composite {

	private Table table;
//	private ArrayList<TableColumn> column = new ArrayList<TableColumn>();

	public CheckerTable(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1,false));
        table = new Table(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION | SWT.CHECK);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	public void setColumns(List<String> columnlabel) {
        table.setHeaderVisible(true);
        TableColumn checkCol = new TableColumn(this.table,SWT.LEFT);
        checkCol.setText("");
        checkCol.setWidth(30);
//        column.add(checkCol);
        columnlabel.forEach(e ->{
            TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
            tmpCol.setText(e);
//            column.add(tmpCol);
        });
        resizeTable();
	}

    private final void resizeTable(){
        table.setVisible(false);
        TableColumn[] columns = table.getColumns();
        Arrays.stream(columns).forEach(e -> e.pack());
        columns[0].setWidth(30);
        table.setVisible(true);
        table.redraw();
    }

	public List<Object> getSelectedData() {
		List<Object> list = new ArrayList<>();
		TableItem[] items = table.getItems();
		Arrays.stream(items).filter(e -> e.getChecked()).forEach(e -> {
			list.add(e.getData());
		});
		items = table.getSelection();
		Arrays.stream(items).forEach(e -> {
			list.add(e.getData());
		});
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}
	public List<String> getSelectedText() {
		List<String> list = new ArrayList<>();
		TableItem[] items = table.getItems();
		Arrays.stream(items).filter(e -> e.getChecked()).forEach(e -> {
			list.add(e.getText());
		});
		items = table.getSelection();
		Arrays.stream(items).forEach(e -> {
			list.add(e.getText());
		});
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}

	public List<TableItem> getSelectedItems() {
		List<TableItem> list = new ArrayList<>();
		TableItem[] items = table.getItems();
		Arrays.stream(items).filter(e -> e.getChecked()).forEach(e -> {
			list.add(e);
		});
		items = table.getSelection();
		Arrays.stream(items).forEach(e -> {
			list.add(e);
		});
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}
	public List<TableItem> getAllItems() {
		TableItem[] items = table.getItems();
		if (items == null) {
			return null;
		} else {
			return Arrays.asList(items);
		}
	}

	public void setCheckAll(boolean checked) {
		TableItem[] items = table.getItems();
		Arrays.stream(items).forEach(e -> e.setChecked(checked));
	}

	public void addInput(List<String> newItem, Object data) {
        if (newItem == null || newItem.isEmpty()) {
        	return;
        }
        TableItem item = new TableItem(table, SWT.NULL);
        item.setText(newItem.toArray(new String[newItem.size()]));
        item.setData(data);
        resizeTable();
	}
}
