package codelamp.mininglamp.widget;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class GroupedPathSelector extends Composite {

	private Text text;
	private Button button;
	private Group group;
	private static final String OPEN_DIR = "OPEN";

	private List<String> extension;
	public void setExtension(List<String> extensions) {
		this.extension = extensions;
	}
	public void setExtension(String extension) {
		this.extension = Arrays.asList(extension);
	}

	public GroupedPathSelector(Composite parent, int widgetStyle) {
		super(parent, widgetStyle);
		this.setLayout(new GridLayout(1, false));
		group = new Group(this, SWT.NONE);
        group.setLayoutData(new GridData(GridData.FILL_BOTH));
		group.setLayout(new GridLayout(2, false));
        text = new Text(group, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
        text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        button = new Button(group, SWT.PUSH);
        button.setText(OPEN_DIR);
        button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
	            try {
	                FileDialog dialog = new FileDialog(GroupedPathSelector.this.getShell());
	                if (extension != null && !extension.isEmpty()) {
	                	dialog.setFilterExtensions((String[])extension.toArray(new String[extension.size()]));
	                }
	                Path path = Paths.get(dialog.open());
	                text.setText(path.toAbsolutePath().toString());
	            } catch (NullPointerException e) {
	            }
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
        this.pack();
	}

	public GroupedPathSelector(Composite parent, int widgetStyle, int fileDialogStyle) {
		super(parent, widgetStyle);
		this.setLayout(new GridLayout(1, false));
		group = new Group(this, SWT.NONE);
        group.setLayoutData(new GridData(GridData.FILL_BOTH));
		group.setLayout(new GridLayout(2, false));
        text = new Text(group, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
        text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        button = new Button(group, SWT.PUSH);
        button.setText(OPEN_DIR);
        button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
	            try {
	                FileDialog dialog = new FileDialog(GroupedPathSelector.this.getShell(),fileDialogStyle);
	                if (extension != null && !extension.isEmpty()) {
	                	dialog.setFilterExtensions((String[])extension.toArray(new String[extension.size()]));
	                }
	                Path path = Paths.get(dialog.open());
	                text.setText(path.toAbsolutePath().toString());
	            } catch (NullPointerException e) {
	            }
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
        this.pack();
	}

	public void setGroupText(String msg) {
		group.setText(msg);
	}

    public void clearText() {
        text.setText("");
    }

    public void setPath(String path) {
        text.setText(path);
    }

    public void setInput(String absolutePath) {
    	Path path = Paths.get(absolutePath);
    	if (Files.exists(path)) {
    		text.setText(path.toAbsolutePath().toString());
    	} else {
    		text.setText("");
    	}
    }

    public String getText() {
        return text.getText();
    }
    public Path getPath() {
    	return Paths.get(text.getText());
    }

	public void setEnable(boolean lock) {
		button.setEnabled(!lock);
	}
    public void dispose() {
    	this.text.dispose();
    	this.button.dispose();
    	super.dispose();
    }

	   public static void main(String[] args) {
	       System.out.println(System.getProperty("user.dir"));
	         Display display = Display.getDefault();
	         Shell shell = new Shell();
	         shell.setLayout(new GridLayout(1, false));
	         GroupedPathSelector f = new GroupedPathSelector(shell,SWT.NONE);
	         f.setPath(System.getProperty("user.dir") + "bar.txt");
//	         f.setText("Auto Classify");
	         shell.pack();
	         shell.open();
	         shell.setSize(600,500);
	         while (!shell.isDisposed()) {
	             if (!display.readAndDispatch())
	                 display.sleep();
	         }
	         f.dispose();
	         display.dispose();
		   Path path = Paths.get(System.getProperty("user.dir"));
		   System.out.println(path.toString());
	     }

}


