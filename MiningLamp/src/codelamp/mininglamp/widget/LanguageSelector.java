package codelamp.mininglamp.widget;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import codelamp.pio.Language;

public class LanguageSelector extends Composite {

	private static final String LANGUAGE_LABEL = "Language";

	public LanguageSelector(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(1, false));
        Group group = new Group(this, SWT.NONE);
        group.setText(LANGUAGE_LABEL);
        group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        group.setLayout(new GridLayout(1, false));
        combo = new Combo(group, SWT.READ_ONLY);
        combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.pack();
	}

	private Combo combo;
	private List<Language> language;

	public void setInput(List<Language> data) {
		this.language = data;
		this.language.forEach(e -> combo.add(e.getLanguage()));
		combo.select(0);
	}

	public String getText() {
		return this.combo.getText();
	}
	public Language getLanguge() {
		return Language.parse(combo.getText());
	}

    public void addSelectionListener(SelectionListener listener) {
    	combo.addSelectionListener(listener);
    }

	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		LanguageSelector cc = new LanguageSelector(shell, SWT.BORDER);
//		cc.setInput(Arrays.asList("A","B"));
		Button btn2 = new Button(shell, SWT.PUSH);
		btn2.setText("ANOTHER");
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}

}
