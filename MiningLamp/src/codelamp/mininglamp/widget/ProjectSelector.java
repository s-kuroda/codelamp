package codelamp.mininglamp.widget;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;

public class ProjectSelector extends Composite {

	private Combo combo;
	private Button btn;
    private static final String SELECT_PROJECT = "Select Project";
    private static final String OPENDIR_BUTTON_TEXT = "OPEN";
    private String dirMsg = "";

	public ProjectSelector(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		combo = new Combo(this,SWT.DROP_DOWN | SWT.READ_ONLY);
		combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		btn = new Button(this, SWT.PUSH);
		btn.setText(OPENDIR_BUTTON_TEXT);
		btn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
	            try {
	            	DirectoryDialog dialog = new DirectoryDialog(ProjectSelector.this.getShell());
	            	if (dirMsg != null && dirMsg != "") {
	            		dialog.setMessage(dirMsg);
	            	}
	                Path path = Paths.get(dialog.open());
	                addInput(path.toAbsolutePath().toString());
	            } catch (NullPointerException e) {
	            }
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		setInput(null);
		this.pack();
	}


	public void setInput(List<String> data) {
		combo.removeAll();
		combo.add(SELECT_PROJECT);
		if (data == null || data.isEmpty()) {
			this.combo.select(0);
			return;
		}
		data.forEach(e -> combo.add(e.toString()));
		this.combo.select(0);
	}

	public void addInput(String data) {
		if (data == null) {
			return;
		}
		if (!Arrays.asList(combo.getItems()).contains(data)) {
			combo.add(data);
		}
		combo.select(combo.indexOf(data));
	}

	public String getText() {
		if (combo.getText().equals(SELECT_PROJECT)) {
			return "";
		} else {
			return combo.getText();
		}
	}

	public void setDialogText(String str) {
		dirMsg = str;
	}

    public void addSelectionListener(SelectionListener listener) {
    	combo.addSelectionListener(listener);
    }
    public void addBtnListener(SelectionListener listener) {
    	btn.addSelectionListener(listener);
    }

	@Override
	public void dispose() {
	    this.combo.dispose();
	    super.dispose();
	}
}
