package codelamp.mininglamp.widget;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class ProjectSelector2 extends Composite {

	private Combo combo;
    private static final String SELECT_PROJECT = "Select Project";

	public ProjectSelector2(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		Label label = new Label(this, SWT.NONE);
		label.setText("Project :");
		this.combo = new Combo(this,SWT.DROP_DOWN | SWT.READ_ONLY);
		this.combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		setInput(null);
		this.pack();
	}


	public void setInput(List<String> data) {
		combo.removeAll();
		combo.add(SELECT_PROJECT);
		combo.add("SECOND");
		if (data == null || data.isEmpty()) {
			this.combo.select(0);
			return;
		}
		data.forEach(e -> combo.add(e.toString()));
		this.combo.select(0);
	}

	public String getText() {
		if (combo.getText().equals(SELECT_PROJECT)) {
			return "";
		} else {
			return combo.getText();
		}
	}

    public void addSelectionListener(SelectionListener listener) {
    	combo.addSelectionListener(listener);
    }

	@Override
	public void dispose() {
	    this.combo.dispose();
	    super.dispose();
	}
}
