package codelamp.mininglamp;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import codelamp.mininglamp.internal.CommitTrace;
import codelamp.mininglamp.widget.CommitsTable;
import codelamp.mininglamp.widget.GroupedPathSelector;
import codelamp.mininglamp.widget.LanguageSelector;
import codelamp.mininglamp.widget.LearnProgress;
import codelamp.mininglamp.widget.LinerButtons;
import codelamp.mininglamp.widget.PathSelector;
import codelamp.mininglamp.widget.ProjectSelector;
import codelamp.pio.Language;

public class MiningLamp2 extends Composite {

	ProjectSelector projectSelector;
	CommitsTable table;
	LanguageSelector langSelector;
	LinerButtons btns;
	GroupedPathSelector faultyPath;
	GroupedPathSelector accuratePath;
	LearnProgress progress;
	Button startBtn;

	public MiningLamp2(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		GridData wide = new GridData(GridData.FILL_HORIZONTAL);
		wide.horizontalSpan = 2;
		projectSelector = new ProjectSelector(this, SWT.BORDER);
		projectSelector.setLayoutData(wide);
		table = new CommitsTable(this, SWT.BORDER);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));
		Composite composite = new Composite(this, SWT.BORDER);
		composite.setLayoutData(new GridData(GridData.FILL_VERTICAL | GridData.HORIZONTAL_ALIGN_END));
		composite.setLayout(new GridLayout(1, false));
		langSelector = new LanguageSelector(composite, SWT.NONE);
		langSelector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		btns = new LinerButtons(composite, SWT.NONE);
		btns.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		GridData wide2 = new GridData(GridData.FILL_HORIZONTAL);
		wide2.horizontalSpan = 2;
		faultyPath = new GroupedPathSelector(this, SWT.NONE, SWT.SAVE);
		faultyPath.setLayoutData(wide2);
		accuratePath = new GroupedPathSelector(this, SWT.NONE, SWT.SAVE);
		accuratePath.setLayoutData(wide2);
		FootComposite foot = new FootComposite(this, SWT.NONE);
		foot.setLayoutData(wide2);
		init();
		this.pack();
		setDnDGitDir(this);
	}

	private class FootComposite extends Composite {
		public FootComposite(Composite parent, int style) {
			super(parent, style);
			this.setLayout(new GridLayout(2, false));
			progress = new LearnProgress(this, SWT.BORDER);
			progress.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			startBtn = new Button(this, SWT.PUSH);
			startBtn.setLayoutData(new GridData(GridData.FILL_VERTICAL));
		}
	}

	private void setDnDGitDir(Control control){
        DropTarget target = new DropTarget(control,DND.DROP_DEFAULT|DND.DROP_COPY);
        FileTransfer transfer = FileTransfer.getInstance();
        Transfer[] types = new Transfer[]{transfer};
        target.setTransfer(types);
        target.addDropListener(new DropTargetAdapter() {
            public void dragEnter(DropTargetEvent evt){
            	evt.detail = DND.DROP_COPY;
            }
            public void drop(DropTargetEvent evt){
                String[] files = (String[]) evt.data;
                if (Files.exists(Paths.get(files[0]))) {
                	setProject(Paths.get(files[0]).toAbsolutePath());
                }
            }
        });
	}

	private class ProjectSelectionListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
		    try {
		    	if (projectSelector.getText() == null || projectSelector.getText().equals("")) {
		    		return;
		    	} else if (project.toAbsolutePath().toString().equals(projectSelector.getText())) {
		    		return;
		    	} else if (Files.exists(Paths.get(projectSelector.getText()))) {
		    		project = Paths.get(projectSelector.getText());
		    		setProject(project);
		    	}
		    } catch (java.util.ConcurrentModificationException e) {
		        this.widgetSelected(event);
		    }
		}
		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
		}
	}

	private static final String OPEN_GITDIR_TEXT = "Choose a Git Repository";
	private static final String FAULTY_BUTTON_TEXT = "Faulty";
	private static final String ACCURATE_BUTTON_TEXT = "Non-Faulty";
	private static final String CLEAR_BUTTON_TEXT = "CLEAR";
	private static final String FAULTY_PATH_MSG = "Faulty DataBase Path";
	private static final String FAULTY_EXTENSION = ".fpdb";
	private static final String ACCURATE_PATH_MSG = "Non-Faulty DataBase Path";
	private static final String ACCURATE_EXTENSION = ".apdb";
	private static final String START_BUTTON_TEXT = "START";
	private void init() {
		projectSelector.setDialogText(OPEN_GITDIR_TEXT);
		projectSelector.addSelectionListener(new ProjectSelectionListener());
		projectSelector.addBtnListener(new ProjectSelectionListener());
		langSelector.setInput(Language.getAll());
		btns.addButton(FAULTY_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.setInputFaultyMarker(table.getSelected(), true);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.addButton(ACCURATE_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.setInputFaultyMarker(table.getSelected(), false);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.addButton(CLEAR_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.clearFaultyMarker();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.setEnableAll(true);
		faultyPath.setGroupText(FAULTY_PATH_MSG);
		faultyPath.setExtension(FAULTY_EXTENSION);
		accuratePath.setGroupText(ACCURATE_PATH_MSG);
		accuratePath.setExtension(ACCURATE_EXTENSION);
		startBtn.setText(START_BUTTON_TEXT);
		startBtn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (projectSelector.getText() == null || projectSelector.equals("")) {
					startBtn.setEnabled(false);
					return;
				} else if (faultyPath.getText() == null || faultyPath.equals("")) {
					return;
				} else if (accuratePath.getText() == null || accuratePath.equals("")) {
					return;
				} else if (!Files.exists(Paths.get(faultyPath.getText()).getParent())
						|| !Files.exists(Paths.get(accuratePath.getText()).getParent())) {
					return;
	    		}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		startBtn.setEnabled(false);
	}

	private Path project;
	private static final List<String> FAULTY_TAGS = Arrays.asList("BUG-");
	private static final List<String> ACCURATE_TAG = Arrays.asList("INNOCENT-");

	private void setProject(Path project) {
		Path path = project.toAbsolutePath();
		projectSelector.addInput(path.toString());
		List<RevCommit> commits = CommitTrace.getAllComit(path.toString());
		if (commits == null || commits.isEmpty()) {
			return;
		}
		commits.forEach(e -> System.out.println(CommitTrace.limitIdLength(e.getId(), 9)));
		table.setInputCommit(commits);
		List<Ref> tags = CommitTrace.getAllTag(path.toString());
		if (tags == null || tags.isEmpty()) {
			return;
		}
		tags.forEach(e -> System.out.println(CommitTrace.getTagNameFromRef(e)));
		tags.forEach(e -> System.out.println(e.getPeeledObjectId()));
		table.setInputTag(tags);
		List<ObjectId> faulty = new ArrayList<>();
		tags.forEach(e -> {
			for (String entry : FAULTY_TAGS) {
				if (e.getName().startsWith(entry)) {
					faulty.add(e.getObjectId());
				}
			}
		});
		table.setInputMarkedId(faulty, true);
		List<ObjectId> accurate = new ArrayList<>();
		tags.forEach(e -> {
			for (String entry : ACCURATE_TAG) {
				if (e.getName().startsWith(entry)) {
					accurate.add(e.getObjectId());
				}
			}
		});
		table.setInputMarkedId(accurate, false);
	}

	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
//		shell.setLayout(new FillLayout());
		shell.setLayout(new GridLayout(1, false));
		MiningLamp2 cc = new MiningLamp2(shell, SWT.BORDER);
		cc.setLayoutData(new GridData(GridData.FILL_BOTH));
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}

}
