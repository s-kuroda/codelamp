package codelamp.mininglamp;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import codelamp.mininglamp.internal.Core;
import codelamp.mininglamp.widget.CheckerTable;
import codelamp.mininglamp.widget.DirectorySelector;
import codelamp.mininglamp.widget.GroupedPathSelector;
import codelamp.mininglamp.widget.LanguageSelector;
import codelamp.mininglamp.widget.LearnProgress;
import codelamp.mininglamp.widget.LinerButtons;
import codelamp.pio.Language;

public class MainUI extends Composite {

	private DirectorySelector projectSelector;
	private CheckerTable table;
	private LanguageSelector langSelector;
	private LinerButtons btns;
	private GroupedPathSelector faultyPath;
	private GroupedPathSelector accuratePath;
	private LearnProgress progress;
	private Button startBtn;
	private final Core core;

	public MainUI(Composite parent, int style) {
		super(parent, style);
		core = new Core(this);
		this.setLayout(new GridLayout(2, false));
		GridData wide = new GridData(GridData.FILL_HORIZONTAL);
		wide.horizontalSpan = 2;
		projectSelector = new DirectorySelector(this, SWT.BORDER);
		projectSelector.setLayoutData(wide);
		table = new CheckerTable(this, SWT.BORDER);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));
		Composite composite = new Composite(this, SWT.BORDER);
		composite.setLayoutData(new GridData(GridData.FILL_VERTICAL | GridData.HORIZONTAL_ALIGN_END));
		composite.setLayout(new GridLayout(1, false));
		langSelector = new LanguageSelector(composite, SWT.NONE);
		langSelector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		btns = new LinerButtons(composite, SWT.NONE);
		btns.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		GridData wide2 = new GridData(GridData.FILL_HORIZONTAL);
		wide2.horizontalSpan = 2;
		faultyPath = new GroupedPathSelector(this, SWT.NONE, SWT.SAVE);
		faultyPath.setLayoutData(wide2);
		accuratePath = new GroupedPathSelector(this, SWT.NONE, SWT.SAVE);
		accuratePath.setLayoutData(wide2);
		FootComposite foot = new FootComposite(this, SWT.NONE);
		foot.setLayoutData(wide2);
		init();
		this.pack();
		setDnDGitDir(this);
	}

	private class FootComposite extends Composite {
		public FootComposite(Composite parent, int style) {
			super(parent, style);
			this.setLayout(new GridLayout(2, false));
			progress = new LearnProgress(this, SWT.BORDER);
			progress.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			startBtn = new Button(this, SWT.PUSH);
			startBtn.setLayoutData(new GridData(GridData.FILL_VERTICAL));
		}
	}

	private void setDnDGitDir(Control control){
        DropTarget target = new DropTarget(control,DND.DROP_DEFAULT|DND.DROP_COPY);
        FileTransfer transfer = FileTransfer.getInstance();
        Transfer[] types = new Transfer[]{transfer};
        target.setTransfer(types);
        target.addDropListener(new DropTargetAdapter() {
            public void dragEnter(DropTargetEvent evt){
            	evt.detail = DND.DROP_COPY;
            }
            public void drop(DropTargetEvent evt){
                String[] files = (String[]) evt.data;
				if (core.setProject(files[0])) {;
					projectSelector.setInput(files[0]);;
				}
            }
        });
	}

	private static final String OPEN_GITDIR_TEXT = "Choose a Git Repository";
	private static final List<String> COLUMN_LABEL = Arrays.asList("isBUG", "ID", "Date", "Tags", "Message");
	private static final String FAULTY_BUTTON_TEXT = "Faulty";
	private static final String ACCURATE_BUTTON_TEXT = "Non-Faulty";
	private static final String CLEAR_BUTTON_TEXT = "CLEAR";
	private static final String FAULTY_PATH_MSG = "Faulty DataBase Path";
	private static final String FAULTY_EXTENSION = ".fpdb";
	private static final String ACCURATE_PATH_MSG = "Non-Faulty DataBase Path";
	private static final String ACCURATE_EXTENSION = ".apdb";
	private static final String START_BUTTON_TEXT = "START";

	private void init() {
		projectSelector.setText(OPEN_GITDIR_TEXT);
		projectSelector.addBtnListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if (!core.setProject(projectSelector.getText())) {;
					projectSelector.clearText();
				}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		table.setColumns(COLUMN_LABEL);
		langSelector.setInput(Language.getAll());
		btns.addButton(FAULTY_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.getSelectedItems().forEach(e -> {
					core.setSign(e, true);
				});
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.addButton(ACCURATE_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.getSelectedItems().forEach(e -> {
					core.setSign(e, false);
				});
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.addButton(CLEAR_BUTTON_TEXT, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				table.getSelectedItems().forEach(e -> {
					core.clearSign(e);
				});
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		btns.setEnabledAll(false);
		faultyPath.setGroupText(FAULTY_PATH_MSG);
		faultyPath.setExtension(FAULTY_EXTENSION);
		accuratePath.setGroupText(ACCURATE_PATH_MSG);
		accuratePath.setExtension(ACCURATE_EXTENSION);
		startBtn.setText(START_BUTTON_TEXT);
		startBtn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (projectSelector.getText() == null || projectSelector.equals("")) {
					startBtn.setEnabled(false);
					return;
				} else if (faultyPath.getText() == null || faultyPath.equals("")) {
					return;
				} else if (accuratePath.getText() == null || accuratePath.equals("")) {
					return;
				} else if (!Files.exists(Paths.get(faultyPath.getText()).getParent())
						|| !Files.exists(Paths.get(accuratePath.getText()).getParent())) {
					return;
	    		}
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		startBtn.setEnabled(false);
	}


	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
//		shell.setLayout(new FillLayout());
		shell.setLayout(new GridLayout(1, false));
		MainUI cc = new MainUI(shell, SWT.BORDER);
		cc.setLayoutData(new GridData(GridData.FILL_BOTH));
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}

	public void addInput(List<String> newItem, Object data) {
		table.addInput(newItem, data);
	}
	public void setEnabledAllBtn(boolean enabled) {
		btns.setEnabledAll(enabled);
	}
}
