package codelamp.mininglamp.internal;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.widgets.TableItem;

import codelamp.mininglamp.MainUI;
import codelamp.util.EpocDateTime;

public class Core {

	private final MainUI ui;

	public Core(MainUI ui) {
		this.ui = ui;
	}

	private static final int ID_LENGTH = 9;
	private Path project;
	private class SignedCommit {
		private final String id;
		private final RevCommit commit;
		private List<Ref> tag = new ArrayList<>();
		private boolean faultySign = false;
		private boolean accurateSign = false;

		public SignedCommit(RevCommit commit) {
			this.commit = commit;
			id = CommitTrace.limitIdLength(commit.getId(), ID_LENGTH);
		}

		public boolean checkTag(Ref tag) {
			if (commit.getId().equals(tag.getPeeledObjectId())) {
				if (!this.tag.contains(tag)) {
					this.tag.add(tag);
				}
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			} else if (obj instanceof RevCommit) {
				return this.equals((RevCommit) obj);
			} else if (obj instanceof ObjectId) {
				return this.commit.equals((ObjectId) obj);
			} else {
				return false;
			}
		}
	}

	private List<SignedCommit> commitList = new ArrayList<>();

	private static final List<String> FAULTY_TAGS = Arrays.asList("BUG-");
	private static final List<String> ACCURATE_TAG = Arrays.asList("INNOCENT-");
	private enum SIGN {Y, N, YN};

	public boolean setProject(String projectPath) {
		ui.setEnabledAllBtn(false);
		if (projectPath == null || projectPath.equals("")) {
			return false;
		}
		project = Paths.get(projectPath).toAbsolutePath();
		if (project == null || !Files.exists(project) || !Files.isDirectory(project)) {
			return false;
		}
		List<RevCommit> commits = CommitTrace.getAllComit(project.toString());
		if (commits == null || commits.isEmpty()) {
			return false;
		}
		commitList.clear();
		commits.forEach(e -> commitList.add(new SignedCommit(e)));
		commits.clear();
		List<Ref> tags = CommitTrace.getAllTag(project.toString());
		if (tags != null && !tags.isEmpty()) {
			tags.forEach(e -> {
				if (e.getPeeledObjectId() != null) {
					String tagId = CommitTrace.limitIdLength(e.getPeeledObjectId(), ID_LENGTH);
					commitList.stream().filter(f -> f.id.equals(tagId)).forEach(f -> {
						f.checkTag(e);
					});
				}
			});
		}
		autoLabel(commitList);
		if (commitList == null || commitList.isEmpty()) {
			return false;
		} else {
			commitList.forEach(e -> {
                List<String> itemData = new ArrayList<>();
                itemData.add("");
                if (e.faultySign == true && e.accurateSign == false) {
                	itemData.add(SIGN.Y.toString());
                } else if (e.faultySign == false && e.accurateSign == true) {
                	itemData.add(SIGN.N.toString());
                } else if (e.faultySign == true && e.accurateSign == true) {
                	itemData.add(SIGN.YN.toString());
                } else {
                	itemData.add("");
                }
                itemData.add(CommitTrace.limitIdLength(e.commit.getId()));
                itemData.add(EpocDateTime.convertEpocToString(e.commit.getCommitTime()));
                if (e.tag != null && !e.tag.isEmpty()) {
                	StringBuffer str = new StringBuffer();
                	e.tag.forEach(f -> str.append(CommitTrace.getTagNameFromRef(f)));
                	itemData.add(String.join(" , ", str));
                } else {
                	itemData.add("");
                }
                itemData.add(e.commit.getShortMessage());
                ui.addInput(itemData, e.id);
        	});
			ui.setEnabledAllBtn(true);
		}
		return true;
	}

	private void autoLabel(List<SignedCommit> commitList) {
		commitList.stream().filter(e -> !e.tag.isEmpty()).forEach(e -> {
			for (String sign : FAULTY_TAGS) {
				if (e.faultySign) {
					break;
				}
				for (Ref tag : e.tag) {
					if (CommitTrace.getTagNameFromRef(tag).startsWith(sign)) {
						e.faultySign = true;
						break;
					}
				}
			}
			for (String sign : ACCURATE_TAG) {
				if (e.accurateSign) {
					break;
				}
				for (Ref tag : e.tag) {
					if (CommitTrace.getTagNameFromRef(tag).startsWith(sign)) {
						e.accurateSign = true;
						break;
					}
				}
			}
		});
	}

	public void setSign(TableItem item, boolean isBug) {
		if (item.getData() instanceof String) {
			String id = (String) item.getData();
			commitList.stream().filter(e -> e.id.equals(id)).forEach(e -> {
				if (isBug) {
					e.faultySign = true;
				} else {
					e.accurateSign = true;
				}
                if (e.faultySign == true && e.accurateSign == false) {
                	item.setText(1, SIGN.Y.toString());
                } else if (e.faultySign == false && e.accurateSign == true) {
                	item.setText(1, SIGN.N.toString());
                } else if (e.faultySign == true && e.accurateSign == true) {
                	item.setText(1, SIGN.YN.toString());
                }
			});
		}
	}
	public void clearSign(TableItem item) {
		if (item.getData() instanceof String) {
			String id = (String) item.getData();
			commitList.stream().filter(e -> e.id.equals(id)).forEach(e -> {
				e.faultySign = false;
				e.accurateSign = false;
                item.setText(1, "");
			});
		}
	}
	public void launch() {
		ui.setEnabledAllBtn(false);
		List<RevCommit> faultyList = new ArrayList<>();
		List<RevCommit> accurateList = new ArrayList<>();
		commitList.forEach(e -> {
			if (e.faultySign && !e.accurateSign) {
				faultyList.add(e.commit);
			} else if (!e.faultySign && e.accurateSign) {
				accurateList.add(e.commit);
			}
		});
//		ui progress size
	}

	private Thread faultyThread = null;
	private Thread accurateThread = null;

	public void startAsFaulty() {
		List<RevCommit> list = new ArrayList<>();
		commitList.stream().filter(e -> e.faultySign).forEach();
		faultyThread = new DiffOutputThread(, repoPath, extensions, output)
	}
	public void startAsAccurate() {

	}


	public class DiffOutputThread extends Thread {
		private boolean kill = false;
		private final List<RevCommit> commitList;
		private final List<String> extensions;
		private final String repoPath;
		private final Path output;
		public DiffOutputThread(List<RevCommit> commitList, String repoPath, List<String> extensions, Path output) {
			this.commitList = commitList;
			this.repoPath = repoPath;
			this.extensions = extensions;
			this.output = output;
		}

//		private void setVisibleProgress(boolean visible) {
//			getDisplay().asyncExec(new Runnable() {
//				public void run() {
//					progress.setVisible(visible);
//				}
//			});
//		}

		public void run() {
			if (commitList == null || commitList.isEmpty()) {
				return;
			}
			List<DiffEntry> diffEntries = new ArrayList<>();
			for (RevCommit commit : commitList) {
				if (kill) {
					return;
				}
				List<DiffEntry> tmp = CommitTrace.getDiffEntry(commit, repoPath);
				if (tmp != null && !tmp.isEmpty()) {
					diffEntries.addAll(tmp);
					tmp.clear();
				}
			}
			if (diffEntries == null || diffEntries.isEmpty()) {
				return;
			}
			List<DiffEntry> trimedEntries = new ArrayList<>();
			extensions.forEach(e -> {
				List<DiffEntry> tmp = CommitTrace.trimExtraExtensionFiles(diffEntries, e);
				if (tmp != null && !tmp.isEmpty()) {
					trimedEntries.addAll(tmp);
					tmp.clear();
				}
			});
			if (trimedEntries == null || trimedEntries.isEmpty()) {
				return;
			}
			CommitTrace.outputDiffToText(trimedEntries, repoPath, output.toAbsolutePath().toString());
		}

		public void kill() {
		    this.kill = true;
		}
	}
}
