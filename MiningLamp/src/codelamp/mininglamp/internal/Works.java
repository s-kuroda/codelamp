package codelamp.mininglamp.internal;

import java.math.BigInteger;

public class Works {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

	}

	private class ID {
		private final Long id;
		private static final int ID_LENGTH = 9;
		public ID(long id) {
			this.id = id;
		}
		public ID(String id) {
			BigInteger tmp = new BigInteger(id.substring(0, ID_LENGTH),16);
			this.id = Long.parseLong(id);
		}

	}
}
