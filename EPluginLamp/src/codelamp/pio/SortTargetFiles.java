package codelamp.pio;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import codelamp.util.EpocDateTime;

public class SortTargetFiles {

    private SortTargetFiles(){
    }

    public enum SortType{Name, Value, State, Time, AbsolutePath};

    private static SortType defaultSort = SortType.Time;
    public static void changeDefaultSort(SortType type) {
    	defaultSort = type;
    }

    static int threthhold =0;

	public static void sortList(ArrayList<TargetFile> list) {
	    if (list == null) {
	        return;
	    }
	    sortList(list, defaultSort);
	}

    public static void sortList(ArrayList<TargetFile> list, SortType type) {
        if (list == null) {
            return;
        }
        try {
            list.forEach(file -> file.setTag(threthhold));
            switch (type) {
                case Name:
                    Collections.sort(list, new FileNameComparator());
                    break;
                case Value:
                    Collections.sort(list, new InferenceValueComparator());
                    break;
                case State:
                    Collections.sort(list, new StateComparator());
                    break;
                case Time:
                    Collections.sort(list, new LastClassifiedComparator());
                    break;
                case AbsolutePath:
                    Collections.sort(list, new AbsolutePathComparator());
                    break;
                default:
                    break;
            }
        } catch (java.util.ConcurrentModificationException e) {
        	// list
            sortList(list, type);
        }
    }

    private static class FileNameComparator implements Comparator<TargetFile> {
        @Override
        public int compare(TargetFile file1, TargetFile file2) {
            return file1.getName().compareTo(file2.getName());
        }
    }

    private static class InferenceValueComparator implements Comparator<TargetFile> {
        @Override
        public int compare(TargetFile file1, TargetFile file2) {
        	return (int) (file2.getInferenceVal() - file1.getInferenceVal());
        }
    }

    private static class StateComparator implements Comparator<TargetFile> {
        @Override
        public int compare(TargetFile file1, TargetFile file2) {
            return file1.getState().compareTo(file2.getState());
        }
    }
    private static class LastClassifiedComparator implements Comparator<TargetFile> {
        @Override
        public int compare(TargetFile file1, TargetFile file2) {
            LocalDateTime time1 = EpocDateTime.convertEpocToDateTime(file1.lastClassified());
            LocalDateTime time2 = EpocDateTime.convertEpocToDateTime(file2.lastClassified());
            return time2.compareTo(time1);
        }
    }
    private static class AbsolutePathComparator implements Comparator<TargetFile> {
        @Override
        public int compare(TargetFile file1, TargetFile file2) {
            return file1.getAbsolutePath().compareTo(file2.getAbsolutePath());
        }
    }
}
