package codelamp.util;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;

public class Commands {
	private Commands() {

	}

	public static final <T extends File> ArrayList<String> cat(T file) {
		ArrayList<String> list = new ArrayList<>();
    	if (PlatformInfo.isWindows() == true) {
    		list.add("cmd");
    		list.add("/c");
    		list.add("type");
    		list.add(file.getAbsolutePath());
    	} else if (PlatformInfo.isUnix() == true) {
    		list.add("cat");
    		list.add(file.getAbsolutePath());
    	}
		return list;
	}
	public static final ArrayList<String> cat(Path path) {
		ArrayList<String> list = new ArrayList<>();
    	if (PlatformInfo.isWindows() == true) {
    		list.add("cmd");
    		list.add("/c");
    		list.add("type");
    		list.add(path.toAbsolutePath().toString());
    	} else if (PlatformInfo.isUnix() == true) {
    		list.add("cat");
    		list.add(path.toAbsolutePath().toString());
    	}
		return list;
	}




}
