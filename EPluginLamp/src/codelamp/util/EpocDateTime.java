package codelamp.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class EpocDateTime {

	private EpocDateTime(){
	}

	private static final ZoneOffset ZONE = ZonedDateTime.now().getOffset();
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    private static final long getEpochSecond(long time) {
        return time/1000;
    }

    private static final int getEpochNano(long time) {
        long nano = time%100;
        return (int) nano;
    }

    public static final LocalDateTime convertEpocToDateTime(long epoc) {
        return  LocalDateTime.ofEpochSecond(getEpochSecond(epoc), getEpochNano(epoc), ZONE);
    }
    public static final LocalDateTime convertEpocToDateTime(int epoc) {
        return  LocalDateTime.ofEpochSecond(epoc, 0, ZONE);
    }
    public static final String convertEpocToString(long epoc) {
        return  convertEpocToDateTime(epoc).format(formatter);
    }
    public static final String convertEpocToString(int epoc) {
        return  convertEpocToDateTime(epoc).format(formatter);
    }
    public static final long now() {
        return ZonedDateTime.now().toEpochSecond() * 1000;
    }
}
