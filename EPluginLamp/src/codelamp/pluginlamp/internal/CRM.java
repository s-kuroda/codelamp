package codelamp.pluginlamp.internal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import codelamp.pio.Charsets;
import codelamp.pio.Language;
import codelamp.util.ApplicationPath;
import codelamp.util.Commands;


public final class CRM {

    private CRM() {
    }

	private static Map<Language, Optional<Path>> tokenizerList = new HashMap<>();
	static {
		tokenizerList.put(Language.JAVA, ApplicationPath.TOKENITHER_JAVA);
	}

//	public final static File create(String path) {
//		File file = new File(path);
//		List<String> pl = new ArrayList<String>();
//		pl.add(CRM.cssutil.getAbsolutePath());
//		pl.add("-b");
//		pl.add("-r");
//		pl.add("-S");
//		pl.add("500000");
//		pl.add(file.getAbsolutePath());
//		ProcessBuilder pb = new ProcessBuilder(pl);
//		// pb.redirectErrorStream(true);
//		try {
//		  Process p = pb.start();
//			InputStream is = p.getInputStream();	//標準出力
//			printInputStream(is);
//			InputStream es = p.getErrorStream();	//標準エラー
//			printInputStream(es);
//		  p.waitFor();
//		} catch (IOException | InterruptedException e) {
//			  System.out.println(e);
//			  return null;
//		}
//		pl.clear();
//		pl = null;
//		return file;
//	}

	static int count = 0;
    private final static String PROB_TEXT = "probability:";
    private final static String CLASSIFY_ARGUMENT = "-{match (:data:) /.*/ isolate (:stats:){classify [:data:] (:*:_arg2: | :*:_arg3:) (:stats:) /[[:graph:]]+/ output /:*:_arg2: matches better :*:_nl::*:stats::*:_nl:/ exit} output /:*:_arg3: matches better :*:_nl::*:stats::*:_nl:/}";
    public final static double classify(Path path, Language language, Path faultyDB, Path accurateDB) throws NullPointerException, IOException {
        if (Files.notExists(path) || Files.notExists(faultyDB) || Files.notExists(accurateDB)) {
        	return -1;
        }
        if (ApplicationPath.CRM114.isPresent()) {
	        ArrayList<String> pl = new ArrayList<>();
	        pl.addAll(Commands.cat(path));
	        getTokenizer(language).ifPresent(tokenizer -> {
	            pl.add("|");
	        	pl.add(tokenizer.toAbsolutePath().toString());
	        });
	        pl.add("|");
	        pl.add(ApplicationPath.CRM114.get().toAbsolutePath().toString());
	        pl.add(CLASSIFY_ARGUMENT);
	        pl.add(faultyDB.toAbsolutePath().toString());
	        pl.add(accurateDB.toAbsolutePath().toString());
	        ProcessBuilder pb = new ProcessBuilder(pl);
	        InputStream result;
	        try {
	          Process p = pb.start();
	          result = p.getInputStream();
	          p.waitFor();
	        } catch (IOException | InterruptedException e) {
	            e.printStackTrace();
	            return -1;
	        }
	        pl.clear();
            double val = -1;
	        try (BufferedReader reader = new BufferedReader(new InputStreamReader(result, Charsets.getDefault()))) {
	            String str;
	            while ((str = reader.readLine()) != null){
	    			System.out.println(str);
	                if (0 <= str.indexOf("CLASSIFY") ){
	                    int plusPointer = str.indexOf(PROB_TEXT) + PROB_TEXT.length();
	                    int minusPointer = str.indexOf("pR:");
	                    val = Double.parseDouble(str.substring(plusPointer, minusPointer - 1));
	                    break;
	                }
	            }
	        } catch(IOException e){
	            e.printStackTrace();
	        }
            return val;
        }
		System.out.println("NullPo");
        return -1;
    }

	private final static Optional<Path> getTokenizer(Language language) throws NullPointerException {
		for (Entry<Language, Optional<Path>> entry : tokenizerList.entrySet()) {
			if (entry.getKey().equals(language)) {
				return entry.getValue();
			}
		}
		return Optional.empty();
	}

	private final static String LEARN_ARGUMENT = "\"-{ match (:data:) /.*/ learn (:*:_arg2:) [:data:] /[[:graph:]]+/}\"";
	public final static boolean learn(Path path, Language language, Path database) throws NullPointerException, IOException {
		System.out.println("crm_learn");
		List<String> pl = new ArrayList<String>();
		pl.addAll(Commands.cat(path));
        getTokenizer(language).ifPresent(tokenizer -> {
            pl.add("|");
        	pl.add(tokenizer.toAbsolutePath().toString());
        });
		pl.add("|");
        pl.add(ApplicationPath.CRM114.get().toAbsolutePath().toString());
		pl.add(LEARN_ARGUMENT);
		pl.add(database.toAbsolutePath().toString());
		ProcessBuilder pb = new ProcessBuilder(pl);
        try {
        	Process p = pb.start();
        	p.waitFor();
        } catch (IOException | InterruptedException e) {
        	e.printStackTrace();
        	return false;
	    }
		pl.clear();
		return true;
	}

	public static void printInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		try {
			for (;;) {
				String line = br.readLine();
				if (line == null) break;
				System.out.println(line);
			}
		} finally {
			br.close();
		}
	}
}
