package codelamp.pluginlamp.internal;

import java.math.BigInteger;

public class Test {

	public static void main(String[] args) {

		System.out.println(String.valueOf(Integer.MAX_VALUE));
		System.out.println(String.valueOf(Integer.toHexString(Integer.MAX_VALUE)));
		System.out.println(String.valueOf(Long.MAX_VALUE));
		System.out.println(String.valueOf(Long.toHexString(Long.MAX_VALUE)));
		Long l = 0xFFFFFFFFFL;
		System.out.println(String.valueOf(l));
		BigInteger bi = new BigInteger("11F", 16);
		System.out.println("AAA".substring(0, 1));
	}

}
