package codelamp.pluginlamp.widget;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import codelamp.pio.Language;

public class LanguageSelector extends Composite {

	public LanguageSelector(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(1, false));
        combo = new Combo(this, SWT.READ_ONLY);
        combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.pack();
	}

	private Combo combo;

	public void setInput(List<Language> data) {
		data.forEach(e -> combo.add(e.getLanguage()));
		combo.select(0);
	}

	public String getText() {
		return this.combo.getText();
	}

    public void addSelectionListener(SelectionListener listener) {
    	combo.addSelectionListener(listener);
    }

//	public static void main(String[] args) {
//		Display display = new Display();
//		final Shell shell = new Shell(display);
//		shell.setLayout(new FillLayout());
//		LanguageSelector cc = new LanguageSelector(shell, SWT.BORDER);
////		cc.setInput(Arrays.asList("A","B"));
//		Button btn2 = new Button(shell, SWT.PUSH);
//		btn2.setText("ANOTHER");
//		shell.pack();
//		shell.open();
//		while (!shell.isDisposed()) {
//			if (!display.readAndDispatch()) {
//				display.sleep();
//				}
//			}
//		display.dispose();
//	}

}
