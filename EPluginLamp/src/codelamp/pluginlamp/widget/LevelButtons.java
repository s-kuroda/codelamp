package codelamp.pluginlamp.widget;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class LevelButtons
		extends Composite {

	private ArrayList<Button> btns = new ArrayList<>();

	public void addButton(String text, int style, SelectionListener listener) {
		Button btn = new Button(this, style);
		btn.setText(text);
		if (listener != null) {
			btn.addSelectionListener(listener);
		}
		btns.add(btn);
		this.pack();
	}

	public LevelButtons(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new FillLayout(SWT.HORIZONTAL));
		this.pack();
	}

	public void setEnabledAll(boolean enabled) {
		btns.forEach(e -> e.setEnabled(enabled));
	}

	public void setEnabled(int btnIndex, boolean enabled) {
		if (btns.size() <= btnIndex) {
			return;
		}
		btns.get(btnIndex).setEnabled(enabled);
	}

	@Override
	public void dispose() {
	    btns.forEach(e -> e.dispose());
	    super.dispose();
	}

	public static void main (String [] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		LevelButtons cc = new LevelButtons(shell, SWT.BORDER);
		cc.addButton("BTN1", SWT.PUSH, null);
		cc.addButton("BTN2", SWT.PUSH, null);
		Button btn2 = new Button(shell, SWT.PUSH);
		btn2.setText("ANOTHER");
//		TextViewer tv = new TextViewer(shell, SWT.BORDER);
//		tv.setInput("BAR");
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
				}
			}
		display.dispose();
	}
}
