package codelamp.pluginlamp.widget;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import codelamp.pio.Language;
import codelamp.pluginlamp.internal.CommitTrace;
import codelamp.pluginlamp.internal.PluginDiscriminator;
import codelamp.util.ApplicationPath;
import codelamp.util.EpocDateTime;
import codelamp.util.LimittedLengthString;

public class AbstractPluginLamp extends Composite {

	protected ProjectSelector projectSelector;
	protected GitResultTreeTable table;
	protected LevelButtons btns;
	protected DiffViewer textViewer;
	protected LanguageSelector languageSelector;

	public AbstractPluginLamp(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1, false));
		projectSelector = new ProjectSelector(this, SWT.NONE);
		projectSelector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		SashForm sash = new SashForm(this, SWT.HORIZONTAL);
		sash.setLayoutData(new GridData(GridData.FILL_BOTH));
		Composite left = new Composite(sash, SWT.NONE);
		left.setLayoutData(new GridData(GridData.FILL_BOTH));
		left.setLayout(new GridLayout(2, false));
		table = new GitResultTreeTable(left, SWT.NONE);
		GridData wide = new GridData(GridData.FILL_BOTH);
		wide.horizontalSpan = 2;
		table.setLayoutData(wide);
		languageSelector = new LanguageSelector(left, SWT.NONE);
		languageSelector.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING));
		btns = new LevelButtons(left, SWT.NONE);
		btns.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		textViewer = new DiffViewer(sash, SWT.NONE);
		textViewer.setLayoutData(new GridData(GridData.FILL_BOTH));
		int[] weights = {1, 1};
		sash.setWeights(weights);
		init();
		pack();
	}

	private final static String[] COLUMN_LABEL =
		{"CommitID / FileName", "Classified Result", "Commit Time / Last Modified", "Commit Message / Absolute Path"};
	private static final String FAULTY_LABEL = "Faulty";
	private static final String ACCURATE_LABEL = "Non-Faulty";
	private Language language;
	private void init() {
		getProjects();
		projectSelector.addComboSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
			    try {
			    	if (projectSelector.getText() != null) {
			    		Path project = projectsMap.get(projectSelector.getText());
			    		if (project == null || project.equals(targetProject)) {
			    			return;
			    		} else {
			    			changeProject(project);
			    		}
			    	}
			    } catch (java.util.ConcurrentModificationException e) {
			        this.widgetSelected(event);
			    }
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});
		projectSelector.addBtnSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				getProjects();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});

		initTable();

		languageSelector.setInput(Language.getAll());
		languageSelector.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				language = Language.parse(languageSelector.getText());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		language = Language.parse(languageSelector.getText());
		btns.addButton(FAULTY_LABEL, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				List<DiffEntry> tmpList = getSelectedLearnTargets();
				if (tmpList == null || tmpList.isEmpty()) {
					return;
				}
				if (leaner != null) {
					leaner.learnFaultyList.addAll(tmpList);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});

		btns.addButton(ACCURATE_LABEL, SWT.PUSH, new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				List<DiffEntry> tmpList = getSelectedLearnTargets();
				if (tmpList == null || tmpList.isEmpty()) {
					return;
				}
				if (leaner != null) {
					leaner.learnAccurateList.addAll(tmpList);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
			}
		});

//		textViewer.setDocument(System.getProperty("user.dir"));
//		textViewer.setDocument(ApplicationPath.APPLICATION_DIR.toString() + Files.exists(ApplicationPath.APPLICATION_DIR));
		textViewer.setColeredLine("+", new Color(getDisplay(), 255, 204, 204));
		textViewer.setColeredLine("-", new Color(getDisplay(), 204, 255, 255));
	}

	private void initTable() {
		table.setColumns(Arrays.asList(COLUMN_LABEL));
		table.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
		        Object obj = event.item;
		        if (obj != null && obj instanceof TreeItem) {
		        	TreeItem item = (TreeItem) obj;
		        	Object data = item.getData();
		        	if (data instanceof DiffEntry) {
			        		DiffEntry entry = (DiffEntry) data;
			        		CommitTrace.getDiffText(entry, gitDir);
			        		System.out.println("GETDIFF " + entry);
			        		textViewer.setDocument(CommitTrace.getDiffText(entry, gitDir));
		        	}else if (data instanceof RevCommit) {
		        		RevCommit commit = (RevCommit) data;
		        		textViewer.setDocument(CommitTrace.getDiffText(commit, gitDir, language));
		        	}
		        }
			}
		});
	}

	private HashMap<String, Path> projectsMap = new HashMap<>();
	private Path targetProject = null;
	private void getProjects() {
		List<Path> projectsPath = new ArrayList<>();
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject[] projects = root.getProjects();
		if (projects == null || projects.length <= 0) {
			return;
		}
		Arrays.stream(projects).forEach(e -> projectsPath.add(e.getLocation().toFile().toPath()));
//		projectsPath.add(Paths.get("D:/Users/ZiTsP/Develop/GitClone/jgit-cookbook"));
//		projectsPath.add(Paths.get("D:/Users/ZiTsP/Develop/GitClone/BitBucket/codelamp"));
//		projectsPath.add(Paths.get("C:/Users/zitsp/Develop/Git/Bitbucket_clone/eclipse"));
//		projectsPath.add(Paths.get("C:/Users/zitsp/Develop/Git/Bitbucket/CodeLamp"));
		projectsMap.clear();
		projectsPath.forEach(e -> projectsMap.put(e.getFileName().toString(), e));
		List<String> names = new ArrayList<>();
		projectsMap.forEach((k,v) -> names.add(k));
		projectSelector.setInput(names);
	}

	private List<DiffEntry> getSelectedLearnTargets() {
		List<TreeItem> selected = table.getSelectedItem();
		if (selected == null || selected.isEmpty()) {
			return null;
		}
		List<RevCommit> tmpCommits  = new ArrayList<>();
		selected.stream().filter(e -> e.getData() instanceof RevCommit)
			.forEach(e -> tmpCommits.add((RevCommit) e.getData()));
		List<DiffEntry> allEntries = new ArrayList<>();
		List<DiffEntry> tmpEntries = new ArrayList<>();
		if (tmpCommits != null && !tmpCommits.isEmpty()) {
			tmpCommits.forEach(e -> allEntries.addAll(CommitTrace.getDiffEntry(e, gitDir.toString())));
		}
		if (allEntries != null && !allEntries.isEmpty()) {
			for (String extension : language.getExtension()) {
				List<DiffEntry> tmpList = CommitTrace.trimExtraExtensionFiles(allEntries, extension);
				if (tmpList != null && !tmpList.isEmpty()) {
					tmpEntries.addAll(tmpList);
				}
			}
		}
		selected.stream().filter(e -> e.getData() instanceof DiffEntry)
			.map(e -> (DiffEntry) e.getData())
			.filter(e -> !tmpEntries.contains(e))
			.forEach(e -> tmpEntries.add(e));
		if (tmpEntries == null || tmpEntries.isEmpty()) {
			return null;
		}
		return tmpEntries;
	}

	private static final String NOTYET_CLASSIFIED = "---";

	protected class CommitDiffData {
		private final RevCommit commit;
		private final Optional<DiffEntry> diffEntry;
		private final ArrayList<String> text = new ArrayList<>();
		private OptionalDouble classifiedVal = OptionalDouble.empty();;
		protected CommitDiffData(RevCommit commit) {
			this.commit = commit;
			this.diffEntry = Optional.empty();
			text.add(CommitTrace.limitIdLength(commit.getId()));
			text.add("");
			text.add(EpocDateTime.convertEpocToString(commit.getCommitTime()));
			text.add(commit.getShortMessage());
		}
		protected CommitDiffData(RevCommit commit, DiffEntry diffEntry) {
			this.commit = commit;
			this.diffEntry = Optional.of(diffEntry);
			Path newPath = Paths.get(gitDir.getParent().toString(), diffEntry.getNewPath());
			text.add(newPath.getFileName().toString());
			text.add(NOTYET_CLASSIFIED);
			if (Files.exists(newPath)) {
				text.add(EpocDateTime.convertEpocToString(newPath.toFile().lastModified()));
			} else {
				text.add("");
			}
			text.add(newPath.toString());
		}
		protected void setClassifiedValue(double val) {
			if (text == null || text.size() < 4) {
				return;
			}
			classifiedVal = OptionalDouble.of(val);
			if (diffEntry.isPresent()) {
				text.set(1, LimittedLengthString.cutDouble(classifiedVal.getAsDouble()));
			} else {
				text.set(1, "(MAX) " + LimittedLengthString.cutDouble(classifiedVal.getAsDouble()));
			}
		}
	}

	private ArrayList<CommitDiffData> list = new ArrayList<>();

	private void addNewHead(RevCommit head) {
		List<DiffEntry> diffEntries = CommitTrace.getDiffEntry(head, gitDir.toString());
		ArrayList<CommitDiffData> tmpList = new ArrayList<>();
		tmpList.add(new CommitDiffData(head));
		for (String extension : language.getExtension()) {
			List<DiffEntry> entries = CommitTrace.trimExtraExtensionFiles(diffEntries, extension);
			if (entries != null && !entries.isEmpty()) {
				entries.forEach(e -> tmpList.add(new CommitDiffData(head, e)));
			}
		}
		tmpList.addAll(list);
		list.clear();
		list = tmpList;
		setTreeData();
	}

	private void setTreeData() {
		getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				table.clear();
				Tree tree = table.getTree();
				HashMap<RevCommit, TreeItem> itemMap = new HashMap<>();
				list.stream().filter(e -> !e.diffEntry.isPresent()).forEach(e -> {
					TreeItem item = new TreeItem(tree, SWT.NONE);
					item.setText(e.text.toArray(new String[e.text.size()]));
					item.setData(e.commit);
					itemMap.put(e.commit, item);
				});
				list.stream().filter(e -> e.diffEntry.isPresent()).forEach(e -> {
					Optional<Entry<RevCommit, TreeItem>> parent
						= itemMap.entrySet().stream().filter(f -> f.getKey().equals(e.commit)).findFirst();
					parent.ifPresent(f -> {
						TreeItem item = new TreeItem(f.getValue(), SWT.NONE);
						item.setText(e.text.toArray(new String[e.text.size()]));
						item.setData(e.diffEntry.get());
					});
				});
			}
		});
	}


	private static final String NOT_EXIST = "No such project exists";
	private static final String NOT_REPOSITORY = "Not git project";
	private Path gitDir;
	private HeadTrace headTrace = null;
	private LearnThread leaner = null;

	private void changeProject(Path path) {
		clearProjectData();
		targetProject = path;
		if (path == null || !Files.exists(path)) {
			textViewer.setDocument(NOT_EXIST + path);
			return;
		}
		gitDir = CommitTrace.setGitDir(path.toString());
		if (gitDir == null) {
			textViewer.setDocument(NOT_REPOSITORY);
			return;
		}
		headTrace = new HeadTrace(gitDir);
		headTrace.start();
		leaner = new LearnThread();
		leaner.start();
	}

	private void clearProjectData() {
		targetProject = null;
		gitDir = null;
		list.clear();
		table.clear();
		textViewer.clear();
		if (headTrace != null) {
			headTrace.kill();
			headTrace = null;
		}
		if (leaner != null) {
			leaner.kill();
			leaner = null;
		}
	}

	private class LearnThread extends Thread {
		private boolean kill = false;
		public LearnThread() {
		}

		@Override
		public void run() {
			try {
				while (!kill) {
					if (learnFaultyList == null || learnFaultyList.isEmpty()){
						sleep(100);
					} else {
						DiffEntry diff = learnFaultyList.removeFirst();
						if (diff != null) {
							System.out.println("LEARN as Bug");
							PluginDiscriminator.learn(diff, gitDir, language, true);
						}
					}
					if (kill) {
						break;
					}
					if (learnAccurateList == null || learnAccurateList.isEmpty()){
						sleep(100);
					} else {
						DiffEntry diff = learnAccurateList.removeFirst();
						if (diff != null) {
							System.out.println("LEARN as Acc");
							PluginDiscriminator.learn(diff, gitDir, language, false);
						}
					}
						sleep(1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			while(true){

			}
		}
		public void kill() {
			this.kill = true;
			learnAccurateList.clear();
			learnFaultyList.clear();
		}

		private LinkedList<DiffEntry> learnFaultyList = new LinkedList<>();
		private LinkedList<DiffEntry> learnAccurateList = new LinkedList<>();
	}

	private class HeadTrace extends Thread {
		private final Path gitDir;
		private boolean kill = false;
		public HeadTrace(Path gitDir) {
			this.gitDir = gitDir;
			if (this.gitDir == null) {
				kill = true;
			}
		}
		private RevCommit head = null;
		@Override
		public void run() {
			while (!kill) {
				if (head == null || !head.equals(CommitTrace.getHeadCommit(gitDir))){
					head = CommitTrace.getHeadCommit(gitDir);
					addNewHead(head);
					list.stream().filter(e -> e.diffEntry.isPresent()).forEach(e -> {
						System.out.println("classified");
						Optional<Double> val = Optional.empty();
						val = PluginDiscriminator.classify(e.diffEntry.get(), gitDir, language);
						val.ifPresent(d -> e.setClassifiedValue(d));
//						if (!val.isPresent()) {
//							e.setClassifiedValue(1.0);
//						}
					});
					setTreeData();
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
		public void kill() {
			this.kill = true;
		}
	}

	@Override
	public void dispose() {
		if (headTrace != null) {
			headTrace.kill();
			headTrace = null;
		}
		if (leaner != null) {
			leaner.kill();
			leaner = null;
		}
		projectSelector.dispose();
		table.dispose();
		btns.dispose();
		textViewer.dispose();
		languageSelector.dispose();
		super.dispose();
	}

//	public static void main(String[] args) {
//		Display display = new Display();
//		final Shell shell = new Shell(display);
//		shell.setLayout(new FillLayout());
//		AbstractPluginLamp c = new AbstractPluginLamp(shell, SWT.BORDER);
//		shell.pack();
//		shell.open();
//		shell.setSize(800, 400);
//		while (!shell.isDisposed()) {
//			if (!display.readAndDispatch()) {
//				display.sleep();
//				}
//			}
//		display.dispose();
//	}

}
