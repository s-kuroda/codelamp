package codelamp.pluginlamp.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

public class GitResultTreeTable extends Composite {

	private Tree tree;
	public GitResultTreeTable(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1,false));
	    tree = new Tree(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
	    tree.setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	public void setColumns(List<String> columnLabels) {
		tree.setHeaderVisible(true);
		columnLabels.forEach(e ->{
		    TreeColumn tmpCol = new TreeColumn(this.tree,SWT.LEFT);
		    tmpCol.setText(e);
		});
		resizeTable();
	}

	private void resizeTable() {
    	tree.setVisible(false);
    	TreeColumn[] columns = this.tree.getColumns();
    	Arrays.stream(columns).forEach(e -> e.pack());
    	tree.setVisible(true);
    }

	public void setParentInput(LinkedHashMap<ArrayList<String>, RevCommit> map) {
    	tree.removeAll();
    	if (map == null || map.isEmpty()) {
    		return;
    	}
    	map.forEach((k, v) -> {
    		if (k == null || k.isEmpty()) {
    			return;
    		}
    		TreeItem item = new TreeItem(tree, SWT.NONE);
    		item.setText(k.toArray(new String[k.size()]));
    		item.setData(v);
    	});
    	resizeTable();
	}

	public TreeItem getParentItem(Object data) {
		if (data == null) {
			return null;
		}
		TreeItem[] items = tree.getItems();
		Optional<TreeItem> item = Arrays.stream(items).filter(e -> data.equals(e.getData())).findFirst();
		if (item == null || item.equals(Optional.empty())) {
			return null;
		} else {
			return item.get();
		}
	}

	public void setChildInput(TreeItem parent, Map<ArrayList<String>, DiffEntry> map) {
    	tree.setVisible(false);
    	if (map == null || map.isEmpty()) {
    		return;
    	}
    	map.forEach((k, v) -> {
    		if (k == null || k.isEmpty()) {
    			return;
    		}
    		TreeItem item = new TreeItem(parent, SWT.NONE);
    		item.setText(k.toArray(new String[k.size()]));
    		item.setData(v);
    	});
    	tree.redraw();
    	resizeTable();
    	tree.setVisible(true);
	}

	public List<TreeItem> getSelectedItem() {
		TreeItem[] items = tree.getSelection();
		if (items == null || items.length <= 0) {
			return null;
		} else {
			return Arrays.asList(items);
		}
	}

	public void addSelectionListener(SelectionListener listener) {
		tree.addSelectionListener(listener);
	}

	public Tree getTree() {
		return tree;
	}

	public void clear() {
		tree.removeAll();
	}

	public List<DiffEntry> getChildsData(RevCommit head) {
		List<TreeItem> items = Arrays.asList(tree.getItems());
		if (items == null || items.isEmpty()) {
			return null;
		}
		List<DiffEntry> diffs = new ArrayList<>();
		Optional<TreeItem> matchCommit = items.stream()
				.filter(e -> e.getData() instanceof RevCommit && (head.equals((RevCommit) e.getData()))).findFirst();
		matchCommit.ifPresent(e -> {
			List<TreeItem> childItems = Arrays.asList(e.getItems());
			childItems.forEach(item -> {
				if (item.getData() instanceof DiffEntry) {
					diffs.add((DiffEntry) item.getData());
				}
			});
		});
		return diffs;
	}

//
//	  public static void main (String [] args) {
//		    Display display = new Display();
//		    final Shell shell = new Shell(display);
//		    shell.setLayout(new FillLayout());
//		    Tree tree = new Tree(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
//		    tree.setHeaderVisible(true);
//		    TreeColumn column1 = new TreeColumn(tree, SWT.LEFT);
//		    column1.setText("Column 1");
//		    column1.setWidth(200);
//		    TreeColumn column2 = new TreeColumn(tree, SWT.CENTER);
//		    column2.setText("Column 2");
//		    column2.setWidth(200);
//		    TreeColumn column3 = new TreeColumn(tree, SWT.RIGHT);
//		    column3.setText("Column 3");
//		    column3.setWidth(200);
//		    TreeItem sitem = null;
//		    for (int i = 0; i < 4; i++) {
//		      TreeItem item = new TreeItem(tree, SWT.NONE);
//		      item.setText(new String[] { "item " + i, "abc", "defghi" });
//		      for (int j = 0; j < 4; j++) {
//		        TreeItem subItem = new TreeItem(item, SWT.NONE);
//		        subItem
//		            .setText(new String[] { "subitem " + j, "jklmnop",
//		                "qrs" });
//		        for (int k = 0; k < 4; k++) {
//		          TreeItem subsubItem = new TreeItem(subItem, SWT.NONE);
//		          subsubItem.setText(new String[] { "subsubitem " + k, "tuv",
//		              "wxyz" });
//		        }
//			      sitem = subItem;
//				    tree.setSelection(sitem);
//		      }
//		    }
//		    GitResultTreeTable tree2 = new GitResultTreeTable(shell, SWT.BORDER);
//		    tree2.setColumns(Arrays.asList("1", "2", "3"));
//		    Map<List<String>, Object> map = new HashMap<>();
//		    map.put(Arrays.asList("a", "b", "c"), "ABC");
//		    map.put(Arrays.asList("d", "e", "f"), "DEF");
//		    tree2.setParentInput(map);
//		    tree2.setChildInput(tree2.getParentItem("ABC"), map);
//		    shell.pack();
//		    shell.setSize(200, 100);
//		    shell.open();
//		    while (!shell.isDisposed()) {
//		      if (!display.readAndDispatch()) {
//		        display.sleep();
//		      }
//		    }
//		    display.dispose();}

}
