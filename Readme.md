# CodeLamp

## Release

### CodelampCreator (JavaApp)

2016/2/12 for bachelor's degree thesis of Kyoto Institute of Technology

### Codelamp-J (JavaApp)

2016/2/12 for bachelor's degree thesis of Kyoto Institute of Technology

### Codelamp-E (EclipsePlugin)

2016/2/12 for bachelor's degree thesis of Kyoto Institute of Technology

### PluginLamp (EclipsePlugin)

2016/4/13 (Beta) It works as a Eclipse-plugin, but only include empty database.

Copy "Release/plugins/EPluginLamp...jar" and "Release/plugins/codelamp..." to under your eclipse's plugin directory like "eclipse/plugins/"

 
## Ongoing

### MiningLamp

This replace CodeLampCreator

### PluginLamp

This replace CodeLamp-E

## Schedule

PluginLamp Release(Beta) of 3th or 4th week in 2016 March

## ToDo & Problem

### All

- Unite each project to single API

### MiningLamp

- In Windows-ShiftJIS (CP932), fault of some special character, output file cannot read.
- It create marged git diff data.
	1. Only target commits
	1. Only Java sorce file
	1. Only added line
	1. Marge them
	1. (Have a problem) Tokenize
	1. (Not Yet) Learning

### PluginLamp

- Refine source code