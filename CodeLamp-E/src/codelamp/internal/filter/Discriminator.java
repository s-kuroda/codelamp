package codelamp.internal.filter;

import java.io.File;
import java.io.IOException;

import codelamp.internal.filter.crm.CRM;
import codelamp.pio.FilterGroup;

public final class Discriminator {

    public final static File create(String path) {
        File file = CRM.create(path);
        return file;
    }

    public final static void learn(File tmpFile, Boolean isAccurate, FilterGroup filter) {
        try {
            CRM.learn(tmpFile, isAccurate, filter);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
    }

    public static double classify(File tmpFile, FilterGroup filter) {
	    System.out.println("DISC : "+filter.getAccurateDB().getAbsolutePath());
        double val = 0;
        try {
            val = CRM.classify(tmpFile, filter);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
        return val;
    }

}
