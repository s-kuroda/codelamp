package codelamp.internal.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;

import codelamp.pio.FilterGroup;
import codelamp.pio.TargetFile;

/**
 * 各コンフィグ用XMLを扱うクラスを取りまとめる
 * @author zitsp
 */
public class Config {

	private final File configDir;
	private final DataBaseConfig dbConfig;
    private final FileRecordsConfig recordConfig;

	private ArrayList<TargetFile> fileList = new ArrayList<>();
	public ArrayList<TargetFile> getRecordedFiles() {
	    return this.recordConfig.getFileList();
	}
    private ArrayList<FilterGroup> filterList = new ArrayList<>();
    public ArrayList<FilterGroup> getRecordedFilters() {
        return this.dbConfig.getAllFilters();
    }
    private FilterGroup selectedFilter = null;
    public FilterGroup getSelectedFilter() {
        return this.dbConfig.getSelectedFilter();
    }

	/**
	 * コンストラクタ
	 * @param dir コンフィグディレクトリ(.codelamp)の所属するディレクトリ。またはそれ自体。
	 * 	cannot {@code null}
	 */
	public Config(File dir, File appRoot)  {
		if (dir == null || appRoot == null) {
			throw new NullPointerException();
		} if (dir.getName().equals(".codelamp") && dir.exists()) {
			this.configDir = dir;
		} else {
			this.configDir = new File(dir.getAbsolutePath(), "/.codelamp");
	        if (! this.configDir.exists()) {
	            this.configDir.mkdir();
	        }
        }
		File dbXML = new File(configDir.getAbsolutePath(), "/db.xml");
		File recordXML = new File(configDir.getAbsolutePath(), "/filerecord.xml");
	    this.dbConfig = new DataBaseConfig(dbXML, appRoot);
	    this.filterList = this.dbConfig.getAllFilters();
        this.selectedFilter = this.dbConfig.getSelectedFilter();
	    this.recordConfig = new FileRecordsConfig(recordXML, this.selectedFilter.getLanguage());
	    this.fileList = this.recordConfig.getFileList();
	}

    /**
     * データを書き出す
     */
    public void write() {
        try {
//        	dbConfig.setSelectedFilter(DataHub.getFilter());
//        	dbConfig.updateAllFilters(DataHub.getAllFilter());
            dbConfig.write();
//            recordConfig.updateFileList(DataHub.getFileList());
            recordConfig.write();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void changeSelectedFilter(FilterGroup filter) {
        this.selectedFilter = filter;
        dbConfig.setSelectedFilter(filter);
    }
    public void updateAllFilters(ArrayList<FilterGroup> filters) {
        this.filterList = filters;
        dbConfig.updateAllFilters(this.filterList);
    }
    public void updateAllFileRecords(ArrayList<TargetFile> files) {
        this.fileList = files;
        recordConfig.updateFileList(this.fileList);
    }
}
