package codelamp.pio;

import java.io.File;

import codelamp.util.EpocDateTime;

@SuppressWarnings("serial")
public final class TargetFile extends File {

    public TargetFile(String pathname) {
        super(pathname);
        this.setExtension();
        this.setLanguage(this.extension);
    }
    public TargetFile(String parent, String child) {
        super(parent,child);
        this.setExtension();
        this.setLanguage(this.extension);
    }
    public TargetFile(File file) {
        super(file.getAbsolutePath());
        this.setExtension();
        this.setLanguage(this.extension);
    }

    private String extension;
    private final void setExtension() {
        int index = this.getName().lastIndexOf(".");
        if (index > 0) {
            extension = this.getName().substring(index);
        } else {
            extension = null;
        }
    }
    public final String getExtension() {
    	return extension;
    }

    private Language language;
    private final void setLanguage(String extension) {
    	this.language = Language.getLanguage(extension);
    }
	public final Language getLanguage() {
		return language;
	}
	public final String getLanguageToString() {
		return this.language.getLanguage();
	}

    private long lastClassified;
    public final long lastClassified() {
		return lastClassified;
	}
	public final void setLastClassified(long EpocTime) {
		this.lastClassified = EpocTime;
	}
    public final void setLastClassified() {
        this.lastClassified = EpocDateTime.now();
    }

    private double inference;
    public final double getInferenceVal() {
		return this.inference;
    }
    public final void setInferenceVal(double val, double threthhold) {
        this.inference = val;
        this.setTag(threthhold);
    }

    private boolean errata = false;
	public final boolean isErrata() {
		return errata;
	}
	private final void setErrata(boolean errata) {
		this.errata = errata;
	}

    public enum TAG {FAULTY, ACCURATE}
    private TAG tag;
    public final void setTag(double threthhold) {
        this.setErrata(false);
        if (this.inference == 0) {
            this.tag = TAG.ACCURATE;
        } else if (this.inference < threthhold) {
            this.tag = TAG.ACCURATE;
        } else {
            this.tag = TAG.FAULTY;
        }
    }

    public final void setTag(boolean properTag) {
        this.setErrata(true);
        if (properTag) {
        	this.tag = TAG.ACCURATE;
        } else {
        	this.tag = TAG.FAULTY;
        }
    }
    public final boolean isBug() {
    	return (this.tag == TAG.FAULTY) ? false : true;
    }

    public final String getTag() {
    	return this.tag.toString();
    }

    public enum STATE {No_Order,Under_Surveillance, Waiting_Classify, Classifying, Waiting_Learn, Learning, Excluded}
    private STATE state = STATE.No_Order;
    public final boolean isNoOrder() {
    	return (this.state == STATE.No_Order);
    }
    public final void exclude() {
    	this.state = STATE.Excluded;
    }
    public final void watch() {
    	this.state = STATE.Under_Surveillance;
    }
    public final boolean isWatched() {
    	return (this.state == STATE.Under_Surveillance)? true: false;
    }

    public final boolean isExcluded() {
        if (this.state == STATE.Excluded) {
            return true;
        }
        return false;
    }
    public final void waitClassify() {
    	this.state = STATE.Waiting_Classify;
    }
    public final void nowClassify() {
    	this.state = STATE.Classifying;
    }
    public final void waitLearn() {
    	this.state = STATE.Waiting_Learn;
    }
    public final void nowLearn() {
    	this.state = STATE.Learning;
    }
    public final String getState() {
    	return this.state.toString();
    }
}
