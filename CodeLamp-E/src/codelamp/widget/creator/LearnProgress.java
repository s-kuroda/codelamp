package codelamp.widget.creator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public final class LearnProgress extends Composite {

	private Label percent;
	private Label rate;
	private ProgressBar progress;
	private final double MAX_PERCENT = 100.00;
	private final int PROGRESS_MAX = 9999;
	private final int PROGRESS_MIN = 0;
	private final String guide = "Select and Distinguish them as Faulty(Bug) or Accurate(NotBug) commit";

	public LearnProgress(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		GridData wide = new GridData(GridData.FILL_HORIZONTAL);
		wide.horizontalSpan = 2;
		this.progress = new ProgressBar(this, SWT.BORDER);
		this.progress.setMaximum(PROGRESS_MAX);
		this.progress.setMinimum(PROGRESS_MIN);
        this.progress.setVisible(false);
		this.progress.setLayoutData(wide);
		this.rate = new Label(this, SWT.NULL);
		this.rate.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING | GridData.FILL_HORIZONTAL));
		this.rate.setText(guide);
		this.percent = new Label(this, SWT.BORDER);
		this.percent.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		this.percent.setText("100.00%");
		this.percent.setVisible(false);
		this.pack();
	}


	private int itemCount = 0;
	private int itemsLength = 0;
	private double itemPercent = 0.0;

	public final void reset() {
		itemCount = 0;
		itemsLength = 0;
		itemPercent = 0;
		this.rate.setVisible(false);
		this.percent.setVisible(false);
		this.progress.setVisible(false);
	}

	public final void start(int length) {
		itemCount = 0;
		setItems(length);
		this.setProgress();
	}

	public final void setItems(int length) {
		itemsLength = length;
		setProgress();
	}
	private final String COMPLETE_MESSAGE = "All Tasks Completed !! ";
	public final void step() {
		if (itemsLength <= itemCount || 100 < itemPercent ) {
			setProgress();
			return;
		} else {
			itemCount++;
			this.setProgress();
		}
	}

	private String message = null;
	public final void setMessage(String message) {
		this.message = message;
	}


	private final void setProgress() {
		if (itemCount == 0 || itemsLength == 0) {
			itemPercent =0;
		} else {
			itemPercent = ((double)itemCount / (double)itemsLength) * 100;
		}
		this.progress.setVisible(true);
		this.rate.setVisible(true);
		int per = (int) itemPercent * 100;
		this.progress.setSelection(per);
		itemPercent = (double) per / 100;
		if (itemsLength == 0) {
			this.rate.setText(guide);
			this.percent.setVisible(false);
		} else if (itemsLength <= itemCount || 100 < itemPercent ) {
            if (this.message != null) {
                this.rate.setText(COMPLETE_MESSAGE + this.message);
            } else {
                this.rate.setText(COMPLETE_MESSAGE);
            }
			this.percent.setText(MAX_PERCENT + "%");
		} else {
			StringBuilder builder = new StringBuilder();
			builder.append("(");
			builder.append(itemCount);
			builder.append("/");
			builder.append(itemsLength);
			builder.append(") ");
			if (this.message != null) {
				builder.append(message);
			}
			this.rate.setText(builder.toString());
			this.percent.setText(itemPercent + "%");
		}
		System.out.println(itemCount);
	}

	public final boolean isComplete() {
		return (itemsLength <= itemCount) ? true : false;
	}


	public static void main(String[] args) {
        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setText("CodeLamp --- Fault-Prone Filtering Plugin");
        shell.setLayout(new GridLayout(1,false));
        Menu menu = new Menu(shell, SWT.BAR);
        shell.setMenuBar(menu);
        MenuItem configButton = new MenuItem(menu, SWT.PUSH);
        configButton.setText("Setting");
        configButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e){
            	System.out.println("MENU BUTTON SELECTED");
            	System.out.println(shell.getSize());
            }
        });
        LearnProgress composite = new LearnProgress(shell, SWT.BORDER);
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
//        composite.start(20);
        Button btn = new Button(shell, SWT.PUSH);
        btn.setText("STEP");
        btn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				composite.step();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
        shell.setSize(800,450);
        Button btn2 = new Button(shell, SWT.PUSH);
        btn2.setText("MINUS");
        btn2.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				composite.setItems(10);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
        shell.setSize(800,450);
        shell.open();
        while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                        display.sleep();
                }
        }
        display.dispose();
    }

	@Override
	public void dispose() {
	    this.progress.dispose();
	    this.rate.dispose();
	    this.percent.dispose();
	    super.dispose();
	}

}
