package codelamp.widget.gitlamp;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

import codelamp.internal.filter.Discriminator;
import codelamp.internal.git.GitCommitDiff;
import codelamp.pio.FilterGroup;

public class GitResultViewer  extends Composite {
    
    private GitResultTable table;
    private GitFilterButtons btns;
    private GitCommitDiff git;
    private ProgressBar progress;
    private GitFilterThread thread = null;
    
    public void setProject(File project, FilterGroup filter) {
        if (this.thread != null) {
            this.thread.kill();
        }
        this.table.clearItems();
        this.learnFaultyList.clear();
        this.learnFaultyList.clear();
        this.classifyList.clear();
        try {
            this.git = new GitCommitDiff(project);
            this.table.setCommitList(this.git.getCommitsList());
        } catch (NullPointerException e) {
            this.git = null;
            return;
        }
        this.thread = new GitFilterThread(filter);
        this.thread.start();
    }

	public GitResultViewer(Composite parent, int style) {
        super(parent, style);
        this.setLayout(new GridLayout(2,false));
        this.table = new GitResultTable(this, SWT.NONE);
        this.table.setLayoutData(new GridData(GridData.FILL_BOTH));
        GridData wide = new GridData(GridData.FILL_BOTH);
        wide.horizontalSpan = 2;
        this.table.setLayoutData(wide);
        this.progress = new ProgressBar(this, SWT.INDETERMINATE);
        this.progress.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        this.progress.setVisible(false);
        this.btns = new GitFilterButtons(this, SWT.NONE);
        this.btns.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
        this.pack();
    }

	private class GitFilterButtons extends AbstractGitFilterButtons {

        public GitFilterButtons(Composite parent, int style) {
            super(parent, style);
        }

        @Override
        public void accurateButtonAction() {
            GitResultViewer.this.table.getSelectedCommits().forEach(e -> {
                if (!GitResultViewer.this.learnAccurateList.contains(e)) {
                    GitResultViewer.this.learnAccurateList.add(e);
                }
            });
        }

        @Override
        public void faultyButtonAction() {
            GitResultViewer.this.table.getSelectedCommits().forEach(e -> {
                if (!GitResultViewer.this.learnFaultyList.contains(e)) {
                    GitResultViewer.this.learnFaultyList.add(e);
                }
            });
        }

        @Override
        public void classifyButtonAction() {
            GitResultViewer.this.table.getSelectedCommits().forEach(e -> {
                if (!GitResultViewer.this.classifyList.contains(e)) {
                    GitResultViewer.this.classifyList.add(e);
                }
            });
        }

	}

    private LinkedList<RevCommit> learnFaultyList = new LinkedList<>();
    private LinkedList<RevCommit> learnAccurateList = new LinkedList<>();
    private LinkedList<RevCommit> classifyList = new LinkedList<>();
    
    private class GitFilterThread extends Thread {
        
        private boolean kill = false;
        private final FilterGroup filter;
        private ObjectId headId = null;
        
        public GitFilterThread(FilterGroup filter) {
            this.filter = filter;
        }

        private boolean learn(RevCommit commit, boolean isAccurate) {
            try {
                File tmpFile = new File(this.filter.getFaultyDB().getParentFile().getAbsolutePath(), "diff.txt");
                GitResultViewer.this.git.getDiffFile(tmpFile, commit, this.filter.getLanguage());
                Discriminator.learn(tmpFile, isAccurate, this.filter);
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        public void classify(RevCommit commit) {
            if (!commit.equals(GitResultViewer.this.git.getFirstCommit())) {
                File tmpFile = new File(this.filter.getFaultyDB().getParentFile().getAbsolutePath(), "diff.txt");
                try {
                    GitResultViewer.this.git.getDiffFile(tmpFile, commit, this.filter.getLanguage());
                    double classifiedVal = Discriminator.classify(tmpFile, this.filter);
                    getDisplay().asyncExec(new Runnable() {
                        public void run() {
                            GitResultViewer.this.table.addInferenceVals(commit, classifiedVal);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        private void setVisibleProgress(boolean visible) {
            getDisplay().asyncExec(new Runnable() {
                public void run() {
                    GitResultViewer.this.progress.setVisible(visible);
                }
            });
        }
        
        public void run() {
            while (!kill) {
                if (GitResultViewer.this.git != null) {
                    if (!GitResultViewer.this.learnFaultyList.isEmpty()) {
                        this.setVisibleProgress(true);
                        RevCommit commit = learnFaultyList.removeFirst();
                        this.learn(commit, false);
                    }
                    if (!GitResultViewer.this.learnAccurateList.isEmpty()) {
                        this.setVisibleProgress(true);
                        RevCommit commit = learnAccurateList.removeFirst();
                        this.learn(commit, true);
                    }
                    if (!GitResultViewer.this.classifyList.isEmpty()) {
                        this.setVisibleProgress(true);
                        RevCommit commit = classifyList.removeFirst();
                        this.classify(commit);
                    }
                    if (this.headId == null || !headId.equals(GitResultViewer.this.git.getHeadId())) {
                        this.headId = GitResultViewer.this.git.getHeadId();
                        RevCommit headCommit = GitResultViewer.this.git.getHeadCommit();
                        System.out.println(headCommit);
                        getDisplay().asyncExec(new Runnable() {
                            public void run() {
                                GitResultViewer.this.table.setHeadCommit(headCommit);
                            }
                        });
                        this.setVisibleProgress(true);
                        GitResultViewer.this.git.refresh();
                        this.classify(headCommit);
                    }
                    this.setVisibleProgress(false);
                }
            }

        }

        public void kill() {
            this.kill = true;
        }
    }

    @Override
    public void dispose() {
        if (this.thread != null) {
            this.thread.kill();
        }
        this.learnFaultyList.clear();
        this.learnFaultyList.clear();
        this.classifyList.clear();
        this.btns.dispose();
        this.table.dispose();
        super.dispose();
    }

    public static void main(String[] args) {
        System.out.println(System.getProperty("user_dir"));
        Display display = Display.getDefault();
        Shell shell = new Shell();
        shell.setLayout(new GridLayout(1, false));
        GitResultViewer f = new GitResultViewer(shell,SWT.NONE);
        shell.pack();
        shell.open();
        shell.setSize(600,500);
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        f.dispose();
        display.dispose();
    }
}
