package codelamp.widget.srclamp;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import codelamp.pio.Language;
import codelamp.pio.SortTargetFiles;
import codelamp.pio.TargetFile;
import codelamp.util.EpocDateTime;

public class SrcResultTable
		extends Composite implements SelectionListener {
	private Table table;
	private ArrayList<TableColumn> column = new ArrayList<>();
	private ArrayList<TargetFile> itemList = new ArrayList<>();
    private Language itemLanguage = Language.JAVA;
	public void setTarget(ArrayList<TargetFile> list, Language language) {
		this.itemList = list;
		this.itemLanguage = language;
		this.initTableItem();
	}

    private final ArrayList<String> COLUMN_TEXT = new ArrayList<>();
    {
        COLUMN_TEXT.add("No");
        COLUMN_TEXT.add("Result");
        COLUMN_TEXT.add("File Name");
        COLUMN_TEXT.add("State");
        COLUMN_TEXT.add("Classified Time");
        COLUMN_TEXT.add("Full Path");
    }


	public SrcResultTable(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(1,false));
        table = new Table(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.addSelectionListener(this);
        table.setHeaderVisible(true);
        this.table.setHeaderVisible(true);
        COLUMN_TEXT.forEach(e ->{
            TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
            tmpCol.setText(e);
            tmpCol.addSelectionListener(this);
            SrcResultTable.this.column.add(tmpCol);
        });
        this.setTableItem();
        this.pack();
	}

	protected void initTableItem(){
        SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Time);
        this.setTableItem();
	}

    protected  void setTableItem(){
        System.out.println("Taable");
        this.table.removeAll();
        int counter = 1;
        try {
	        if(itemList == null) {
	            TableItem item = new TableItem(this.table,SWT.NULL);
	            String[] itemData = new String[6];
	        	for (int i = 0; i < 6; i++) {
	        		itemData[i] = "bar";
	        	}
	            item.setText(itemData);
	        } else {
		        for (TargetFile file : itemList) {
		            if (file.getExtension() == null) {
		                continue;
		            } else if (file.getLanguage() == itemLanguage){
		        		System.out.println(file.getName());
		                TableItem item = new TableItem(this.table,SWT.NULL);
		                String[] itemData = new String[6];
		                itemData[0] = String.valueOf(counter++);
//		                if ((file.getInferenceVal() == 0) || (file.isNoOrder() == true)) {
		                if ( file.lastClassified() == 0) {
		                    itemData[1] = "";
		                } else {
		                	StringBuilder tmpStrBuilder = new StringBuilder();
		                	if (file.isErrata()) {
		                    	tmpStrBuilder.append("*");
		                	}
		                	file.setTag(0.5);
		                	tmpStrBuilder.append(file.getTag()).append(" ");
		                	tmpStrBuilder.append(file.getInferenceVal());
		                	itemData[1] = tmpStrBuilder.toString();
		                }
		                itemData[2] = file.getName();
		                itemData[3] = file.getState();
		                if (file.lastClassified() == 0) {
		                    itemData[4] = "";
		                } else {
		                    itemData[4] = EpocDateTime.convertEpocToString(file.lastClassified());
		                }
		                itemData[5] = file.getAbsolutePath();
		                item.setText(itemData);
		                item.setData(file);
		        		System.out.println("TABLED");
		            }
		        }
	        }
        } catch (NullPointerException e) {
        	System.out.println("Target : null");
        }
        this.ResizeTable();
    }

    private void ResizeTable(){
        this.table.setVisible(false);
        TableColumn[] columns = this.table.getColumns();
        Arrays.stream(columns).forEach(e -> e.pack());
        this.table.setVisible(true);
    }

    public ArrayList<TargetFile> getSelectedFiles() {
        TableItem[] selectedItems = this.table.getSelection();
        ArrayList<TargetFile> itemList = new ArrayList<>();
        for (TableItem entry: selectedItems) {
            if (entry.getData() instanceof TargetFile) {
                TargetFile file = (TargetFile) entry.getData();
                itemList.add(file);
            }
        }
        return itemList;
    }

    public void setState(TargetFile target, TargetFile.STATE state) {
        TableItem[] items = table.getItems();
        Arrays.stream(items).forEach(e -> {
            if (e.getData() instanceof TargetFile) {
                if (((TargetFile) e.getData()).equals(target)) {
                    e.setText(3, state.toString());
                }
            }
        });
    }

    public void setState(TargetFile target) {
        TableItem[] items = table.getItems();
        Arrays.stream(items).forEach(e -> {
            if (e.getData() instanceof TargetFile && ((TargetFile) e.getData()).equals(target)) {
                e.setText(3, target.getState());
            }
        });
    }

	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
        Object obj = event.getSource();
        if (obj == this.column.get(0)) {
            this.table.selectAll();
        } else if (obj == this.column.get(1)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Value);
            this.setTableItem();
        } else if (obj == this.column.get(2)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Name);
            this.setTableItem();
        } else if (obj == this.column.get(3)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.State);
            this.setTableItem();
        } else if (obj == this.column.get(4)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Time);
            this.setTableItem();
        } else if (obj == this.column.get(5)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.AbsolutePath);
            this.setTableItem();
        }
	}

	@Override
	public void dispose() {
	    this.table.dispose();
	    this.column.clear();
	    this.itemList.clear();
	    this.itemLanguage = null;
	    super.dispose();
	}

	public static void main(String[] args) {
        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setText("CodeLamp --- Fault-Prone Filtering Plugin");
        shell.setLayout(new GridLayout(1,false));
        Menu menu = new Menu(shell, SWT.BAR);
        shell.setMenuBar(menu);
        MenuItem configButton = new MenuItem(menu, SWT.PUSH);
        configButton.setText("Setting");
        configButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e){
            	System.out.println("MENU BUTTON SELECTED");
            	System.out.println(shell.getSize());
            }
        });
        ArrayList<File> tmp = new ArrayList<>();
        tmp.add(new File("first"));
        tmp.add(new File("second"));
        SrcResultTable composite = new SrcResultTable(shell, SWT.BORDER);
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        shell.setSize(800,450);
        shell.open();
        while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                        display.sleep();
                }
        }
        display.dispose();
    }

}
