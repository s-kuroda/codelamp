package codelamp.widget.config;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import codelamp.pio.FilterGroup;

public abstract class AbstractFilterTable<T>
		extends Composite implements SelectionListener{

	private Table table;
	private Label label;
	private ArrayList<TableColumn> column = new ArrayList<TableColumn>();
	private ArrayList<FilterGroup> itemList = null;
	public void setFilterList(ArrayList<FilterGroup> filters) {
		this.itemList = filters;
	}
	private FilterGroup targetFilter;
	public void setFilter(FilterGroup targetFilter) {
		this.targetFilter = targetFilter;
	}

    private final String[] COLUMN_TEXT = {"", "Language","Fault-Prone DB","Non Fault-Prone DB"};

	public AbstractFilterTable(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1, false));
		label = new Label(this, SWT.NONE);
		label.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		label.setText("Language and DataBase For Filtering");
	    table = new Table(this, SWT.SINGLE|SWT.FULL_SELECTION|SWT.BORDER);
	    table.setLayoutData(new GridData(GridData.FILL_BOTH));
	    table.setHeaderVisible(true);
	    table.setLinesVisible(true);
        table.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetDefaultSelected(SelectionEvent e) {
        		AbstractFilterTable.this.changeTargetFilter();
        	}
        });
        for(int i=0; i < COLUMN_TEXT.length; i++){
            TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
            tmpCol.setText(COLUMN_TEXT[i].toString());
            tmpCol.addSelectionListener(this);
            column.add(tmpCol);
        }
        this.setTableItem();
    }

	protected abstract void changeTargetFilter();

	protected  void setTableItem(){
        this.table.removeAll();
        try {
	        if(itemList == null) {
	            TableItem item = new TableItem(this.table,SWT.NULL);
	            String[] itemData = new String[6];
	        	for (int i = 0; i < 6; i++) {
	        		itemData[i] = "bar";
	        	}
	            item.setText(itemData);
	        } else {
		        for (FilterGroup file : itemList) {
		            if (file.getLanguage() == null) {
		                continue;
		            } else {
		                TableItem item = new TableItem(this.table,SWT.NULL);
		                String[] itemData = new String[4];
		                itemData[0] = (file == this.targetFilter)? "*": "";
		                itemData[1] = file.getLanguageToString();
		                itemData[2] = file.getFaultyDB().getAbsolutePath();
		                itemData[3] = file.getAccurateDB().getAbsolutePath();
		                item.setText(itemData);
		            }
		        }
	        }
        } catch (NullPointerException e) {
        	System.out.println("Target : null");
        }
        this.ResizeTable();
        this.table.select(0);
    }

    private void ResizeTable(){
        table.setVisible(false);
        TableColumn[] columns = table.getColumns();
        for (int i = 0; i < columns.length; i++)
        {
          columns[i].pack();
        }
        table.setVisible(true);
    }

	protected void setTableLabel(String str){
		this.label.setText(str);
	}

	protected void setItemFromXML(){
	}


	public TableItem[] getSelection() {
		return this.table.getSelection();
	}

	public int getItemCount() {
		return this.table.getItemCount();
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {

	}

}