package codelamp.widget.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public abstract class AbstractYNButtons extends Composite implements SelectionListener{

	Button okButton;
	Button escButton;

	public AbstractYNButtons(Composite parent, int style) {
		super(parent, style);
		this.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
        this.setLayout(new GridLayout(2,false));
        okButton = new Button(this, SWT.PUSH);
        okButton.setText("OK");
        okButton.addSelectionListener(this);
        escButton = new Button(this, SWT.PUSH);
        escButton.setText("Cancel");
        escButton.addSelectionListener(this);
        this.pack();
	}

	@Override
	public void  widgetSelected(SelectionEvent event) {
		Object obj=event.getSource();
		if (obj == okButton) {
			okButtonAction();
		} else if (obj == escButton) {
			cancelButtonAction();
		}
	}

	protected abstract void okButtonAction();

	protected abstract void cancelButtonAction();

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
	}

	public void dispose() {
		this.okButton.dispose();
		this.escButton.dispose();
		super.dispose();
	}
}
