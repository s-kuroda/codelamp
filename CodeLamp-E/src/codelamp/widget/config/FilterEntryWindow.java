package codelamp.widget.config;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import codelamp.pio.FilterGroup;

public class FilterEntryWindow extends Composite {
	private LanguageAndExtension lang;
	private DataBaseComposite fpComposite;
	private DataBaseComposite apComposite;
	private FilterGroup modify = null;
	private Shell shell;
	private Group group;
	private final FilterTab parentWidget;

	public FilterEntryWindow(Shell parent, int style, FilterTab parentWidget) {
	    super(parent, style);
	    this.parentWidget = parentWidget;
	    shell = parent;
        GridData wideData = new GridData(GridData.FILL_HORIZONTAL);
        GridData rightData = new GridData(GridData.HORIZONTAL_ALIGN_END);
        this.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.setLayout(new GridLayout(1,false));
        group = new Group(this,SWT.NONE);
        group.setLayout(new GridLayout(1, false));
        group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        group.setText("New Entry");
        lang = new  LanguageAndExtension(group, SWT.NONE);
        lang.setLayoutData(wideData);
        fpComposite = new DataBaseComposite(group, SWT.NONE);
        fpComposite.setExtension("*.fpdb");
        fpComposite.setLabel("Fault-Prone DataBase");
        fpComposite.setLayoutData(wideData);
        apComposite = new DataBaseComposite(group, SWT.NONE);
        apComposite.setExtension("*.apdb");
        apComposite.setLabel("Non Fault-Prone DataBase");
        apComposite.setLayoutData(wideData);
        OkCancelButtonComposite okCancel = new OkCancelButtonComposite(this, SWT.NONE);
        okCancel.setLayoutData(rightData);
	}

	public FilterEntryWindow(Shell parent, int style, FilterTab parentWidget, FilterGroup filter) {
	    this(parent, style, parentWidget);
	    this.setDataPair(filter);
    }

	public void setDataPair(FilterGroup filter) {
        modify = filter;
        group.setText("Modify Entry");
        lang.setCombo(filter.getLanguage());
        fpComposite.setText(filter.getFaultyDB().getAbsolutePath());
        apComposite.setText(filter.getAccurateDB().getAbsolutePath());
        this.redraw();
	}

    private class LanguageAndExtension extends AbstractFilterLanguageSelector {

        public LanguageAndExtension(Composite parent, int style) {
            super(parent, style);
        }

        @Override
        public void widgetSelected(SelectionEvent e) {
            setText();
        }

        @Override
        public void widgetDefaultSelected(SelectionEvent e) {
        }
	}

    private class DataBaseComposite extends AbstractDataBaseSelector {

        public DataBaseComposite(Composite parent, int style) {
            super(parent, style);
            setButtonText("Open");
        }

        @Override
        void buttonAction() {
            try {
                FileDialog dialog = new FileDialog(this.getShell());
                dialog.setFilterPath(System.getProperty("user.dir"));
                dialog.setFilterExtensions(getExtension());
                File file = new File(dialog.open());
                setText(file.getAbsolutePath());
            } catch (NullPointerException e) {
            }
        }

    }

    private class OkCancelButtonComposite extends AbstractYNButtons {

        public OkCancelButtonComposite(Composite parent, int style) {
            super(parent, style);
        }

        @Override
        protected void okButtonAction() {
            if (modify != null) {
                FilterEntryWindow.this.parentWidget.remove(modify);
            }
            File fp = new File(fpComposite.getText());
            File ap = new File(apComposite.getText());
            if (fp.exists() && ap.exists()) {
				FilterGroup filterEntry = new FilterGroup(fp, ap, lang.getSelect());
                FilterEntryWindow.this.parentWidget.add(filterEntry);
            }
            FilterEntryWindow.this.dispose();
        }

        @Override
        protected void cancelButtonAction() {
            FilterEntryWindow.this.dispose();
        }

    }
    public static void main(String[] args) {
        Display display = Display.getDefault();
        Shell shell = new Shell();
        shell.setLayout(new GridLayout(1, false));
        FilterEntryWindow f = new FilterEntryWindow(shell,SWT.NONE, null);
        shell.pack();
        shell.open();
        shell.setSize(600,500);
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        f.dispose();
        display.dispose();
    }

    public void dispose() {
    	this.lang.dispose();
    	this.fpComposite.dispose();
    	this.apComposite.dispose();
    	this.group.dispose();
    	super.dispose();
    	this.shell.dispose();
    }
}
