package codelamp.widget.common;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;

public abstract class AbstractProjectSelector extends Composite implements SelectionListener {

	private Combo combo;
	private File targetProject = null;
    private ArrayList<File> projectList = new ArrayList<>();
    private final String SELECT_PROJECT = "Other Project Folder or Git Repository";
    private final String ENABLE_DnD = "Enable Drag and Drop Project Folder or Git Repository Here";

	public AbstractProjectSelector(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		Label label = new Label(this, SWT.NONE);
		label.setText("Project :");
		this.combo = new Combo(this,SWT.DROP_DOWN | SWT.READ_ONLY);
		this.combo.addSelectionListener(this);
		this.combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.setComboItems();
		this.pack();
	}

	private void setComboItems() {
		this.combo.removeAll();
		if (this.projectList.isEmpty()) {
			this.combo.add(ENABLE_DnD);
		} else {
			for (File entry : projectList) {
				if (entry.exists()) {
					this.combo.add(entry.getAbsolutePath());
				}
			}
		}
		this.combo.add(SELECT_PROJECT);
		this.combo.select(this.getSelectedProject());
		this.reflect(this.targetProject);
	}

	public abstract void reflect(File targetProject);
	
    private int getSelectedProject() {
	    if (this.targetProject == null) {
	        return 0;
	    } else {
	        return projectList.indexOf(targetProject);
	    }
    }
	
	public void addProject(String selectedProject) {
		File project = null;
		for (File entry : this.projectList) {
			if (entry.getAbsolutePath().equals(selectedProject)) {
				project = entry;
			}
		}
		if (project == null) {
			project = new File(selectedProject);
		    this.projectList.add(project);
		}
		if (project.exists()) {
			this.targetProject = project;
			this.setComboItems();
		}
	}

	public File getProject() {
		if (projectList == null) {
		    return null;
		}
        for (File tmpFile: projectList) {
        	if (tmpFile.getAbsolutePath().equals(combo.getText())) {
        		return tmpFile;
        	}
        }
		return null;
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
	    try {
	    	if (this.combo.getText().equals(SELECT_PROJECT)) {
	    		this.getOtherProject();
	    	} else {
		    	this.targetProject = this.getProject();
				this.setComboItems();
	    	}
	    } catch (java.util.ConcurrentModificationException e) {
	        this.widgetSelected(event);
	    }
	}

	public void getOtherProject() {
		try {
			DirectoryDialog dialog = new DirectoryDialog(this.getShell());
			dialog.setFilterPath(System.getProperty("user.dir"));
			this.addProject(dialog.open());
		} catch (NullPointerException e) {
			this.setComboItems();
		}
	}


	@Override
	public void dispose() {
	    this.combo.dispose();
	    super.dispose();
	}
//   public static void main(String[] args) {
//       System.out.println(System.getProperty("user.dir"));
//         Display display = Display.getDefault();
//         Shell shell = new Shell();
//         shell.setLayout(new GridLayout(1, false));
//         ProjectSelector f = new ProjectSelector(shell,SWT.NONE);
//         shell.pack();
//         shell.open();
//         shell.setSize(600,500);
//         while (!shell.isDisposed()) {
//             if (!display.readAndDispatch())
//                 display.sleep();
//         }
//         f.dispose();
//         display.dispose();
//     }
}
