package codelamp.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileCopy {

	private FileCopy() {
	}

    @SuppressWarnings("resource")
    public static void copyFile(String sourceFile, String destinationFile) throws IOException {
        FileChannel sourceChannel = new FileInputStream(sourceFile).getChannel();
        FileChannel destinationChannel = new FileOutputStream(destinationFile).getChannel();
        try {
            sourceChannel.transferTo(0, sourceChannel.size(),destinationChannel);
            sourceChannel.close();
            destinationChannel.close();
        } catch (IOException e) {
            sourceChannel.close();
            destinationChannel.close();
            throw e;
        }
    }

    public static void copyFile(File sourceFile, File destinationFile) {
        if (sourceFile.exists()) {
            try {
                copyFile(sourceFile.getAbsolutePath(), destinationFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void copyBinaryFile(String sourceFile, String destinationFile) throws IOException {
        int stream;
        BufferedInputStream  source  = new BufferedInputStream(new FileInputStream(sourceFile));
        BufferedOutputStream destination = new BufferedOutputStream(new FileOutputStream(destinationFile));
        while ((stream = source.read()) >= 0){
            destination.write(stream);
        }
        destination.flush();
        source.close();
        destination.close();
    }

    public static void copyBinaryFile(File sourceFile, File destinationFile) {
        if (sourceFile.exists()) {
            try {
                copyBinaryFile(sourceFile.getAbsolutePath(), destinationFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
