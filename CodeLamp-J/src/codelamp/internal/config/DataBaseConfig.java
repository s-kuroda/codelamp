package codelamp.internal.config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import codelamp.pio.FilterGroup;
import codelamp.pio.Language;
import codelamp.util.FileCopy;

/**
 * Fault-Prone Filterに使用するDBと言語の組み合わせをXMLに読み書きする。
 * @author zitsp
 */
public class DataBaseConfig implements IXMLHandler {

    private final File configXML;
    private final File appRoot;
	private ArrayList<FilterGroup> filters = new ArrayList<>();
	private FilterGroup selectedFilter = null;

	/**
	 * コンストラクタ
	 * @param file コンフィグ用XML
	 * 	cannot {@code null}
	 */
	public DataBaseConfig(File file, File appRoot) {
	    this.appRoot = appRoot;
		if (file == null) {
			throw new NullPointerException();
		} else if (file.exists() && file.isFile()) {
			this.configXML = file;
                try {
                    this.read();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XMLStreamException e) {
                    this.init();
                }
		} else {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.configXML = file;
			this.init();
		}
	}

	private void init() {
        this.configXML.delete();
        File configDir = this.configXML.getParentFile();
        File cpFPDB = new File(configDir.getAbsolutePath(), "db/default.fpdb");
        File cpNFPDB = new File(configDir.getAbsolutePath(), "db/default.apdb");
        try {
            this.configXML.createNewFile();
            this.filters.clear();
            if (!cpFPDB.getParentFile().exists()) {
                cpFPDB.getParentFile().mkdirs();
            } else if (!cpFPDB.getParentFile().isDirectory()) {
                cpFPDB.getParentFile().delete();
                cpFPDB.getParentFile().mkdirs();
            }
            if (cpFPDB.exists()) {
                cpFPDB.delete();
            }
            cpFPDB.createNewFile();
            if (cpNFPDB.exists()) {
                cpNFPDB.delete();
            }
            cpNFPDB.createNewFile();
            File defFPDB = new File(this.appRoot.getAbsolutePath(), "default/default.fpdb");
            FileCopy.copyBinaryFile(defFPDB, cpFPDB);
            File defNFPDB = new File(this.appRoot.getAbsolutePath(), "default/default.apdb");
            FileCopy.copyBinaryFile(defNFPDB, cpNFPDB);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        FilterGroup dbs = new FilterGroup(cpFPDB, cpNFPDB, Language.JAVA.getLanguage());
        this.filters.add(dbs);
        this.selectedFilter = this.filters.get(0);
    }

    private static enum XMLTAG {DB, FPDB, NFPDB};
    private static enum XMLATTRIBUTE {LANGUAGE, CURRENT};

    @Override
    public void read() throws IOException, XMLStreamException {
        if (! this.configXML.exists()) {
            throw new FileNotFoundException();
        }
        XMLInputFactory factory = XMLInputFactory.newInstance();
        BufferedInputStream stream = new BufferedInputStream(new FileInputStream(this.configXML));
        XMLStreamReader reader = factory.createXMLStreamReader(stream);
        for (; reader.hasNext(); reader.next()) {
            if ((reader.getEventType() == XMLStreamConstants.START_ELEMENT)
                    && (reader.getName().toString().equals(XMLTAG.DB.toString()))) {
                reader = this.createDBPair(reader);
            }
        }
        if (this.filters == null || this.filters.isEmpty()) {
        	this.init();
        } else if (this.selectedFilter == null) {
        	this.selectedFilter = filters.get(0);
        }
        reader.close();
    }

    /**
     * XMLから、DB+言語のセットごとに読み込む
     * @param readerb 読み出し位置
     * @return 読み出し位置
     */
    private XMLStreamReader createDBPair(XMLStreamReader reader) {
        File fpDB = null;
        File nfpDB = null;
        String language = null;
        boolean current = false;
        for (int i = 0; i < reader.getAttributeCount(); i++) {
            if (reader.getAttributeName(i).toString() == XMLATTRIBUTE.LANGUAGE.toString()) {
               language = reader.getAttributeValue(i);
            } else if ((reader.getAttributeName(i).toString() == XMLATTRIBUTE.CURRENT.toString())
                    && (reader.getAttributeValue(i).toString().equals("true"))) {
                current = true;
            }
        }
        try {
            for (; reader.hasNext(); reader.next()) {
                if ((reader.getEventType() == XMLStreamConstants.START_ELEMENT)
                        && (reader.getName().toString().equals(XMLTAG.FPDB.toString()))) {
                    fpDB = new File(reader.getElementText());
                } else if ((reader.getEventType() == XMLStreamConstants.START_ELEMENT)
                        && (reader.getName().toString().equals(XMLTAG.NFPDB.toString()))) {
                    nfpDB = new File(reader.getElementText());
                } else if ((reader.getEventType() == XMLStreamConstants.END_ELEMENT)
                        && (reader.getName().toString().equals(XMLTAG.DB.toString()))) {
                    break;
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        if (fpDB == null || nfpDB == null || language == null) {
            return reader;
        } else if (fpDB.exists() && nfpDB.exists() && Language.has(language)){
        	FilterGroup tmpFilter = new FilterGroup(fpDB, nfpDB, language);
            this.filters.add(tmpFilter);
            if (current) {
            	this.selectedFilter = tmpFilter;
            }
        }
        return reader;
    }

    @Override
    public void write() throws IOException, XMLStreamException {
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        factory.setProperty("javax.xml.stream.isRepairingNamespaces", Boolean.TRUE);
        XMLStreamWriter writer = factory.createXMLStreamWriter(new FileWriter(this.configXML));
        writer.writeStartDocument();
        writer.writeStartElement("CONFIG");
        writer.writeCharacters("DB_LIST");
        for (FilterGroup entry: this.filters) {
            writer.writeStartElement(XMLTAG.DB.toString());
            writer.writeAttribute(XMLATTRIBUTE.LANGUAGE.toString(), entry.getLanguageToString());
            if (entry.equals(this.selectedFilter)) {
                writer.writeAttribute(XMLATTRIBUTE.CURRENT.toString(), "true");
            }
            writer.writeStartElement(XMLTAG.FPDB.toString());
            writer.writeCharacters(entry.getFaultyDB().getAbsolutePath());
            writer.writeEndElement();
            writer.writeStartElement(XMLTAG.NFPDB.toString());
            writer.writeCharacters(entry.getAccurateDB().getAbsolutePath());
            writer.writeEndElement();
            writer.writeEndElement();
        }
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();
    }

	public ArrayList<FilterGroup> getAllFilters() {
		return this.filters;
	}

	public void updateAllFilters(ArrayList<FilterGroup> filters) {
		this.filters = filters;
	}

	public FilterGroup getSelectedFilter() {
		return selectedFilter;
	}

	public void setSelectedFilter(FilterGroup selectedFilter) {
		this.selectedFilter = selectedFilter;
	}

	public static void main(String[] args) {
        File file = new File(System.getProperty("user.dir"),"/.codelamp/db.xml");
        File file2 = new File(System.getProperty("user.dir"));
        DataBaseConfig config = new DataBaseConfig(file,file2);
        System.out.println(config.filters);
        System.out.println(config.selectedFilter);
        try {
            config.write();
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
