package codelamp.internal.filter.crm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import codelamp.pio.FilterGroup;
import codelamp.pio.Language;
import codelamp.util.ApplicationPath;
import codelamp.util.Commands;
import codelamp.util.PlatformInfo;


public final class CRM {

    private CRM() {
    }

	private static final File crm = initCrmPath();
	private static final File cssutil = initCssutilPath();
//	private static final String classifier = "";


	private static ArrayList<TokenizerFile> tokenizerList = new ArrayList<>();
	static {
		try {
			tokenizerList.add(setJavaTokenizer());
		} catch (NullPointerException e) {
            e.printStackTrace();
		}
	}

	private final static TokenizerFile setJavaTokenizer() throws NullPointerException{
	    TokenizerFile app = null;
    	if (PlatformInfo.isWindows() == true) {
		    app = new TokenizerFile(ApplicationPath.root.getAbsolutePath(), "/lib/tokenizer/windows/java/mkJavaTokenSC.exe", Language.JAVA.getLanguage());
    	} else if (PlatformInfo.isLinux() == true) {
            app = new TokenizerFile(ApplicationPath.root.getAbsolutePath(), "/lib/tokenizer/linux/java/mkJavaTokenSC", Language.JAVA.getLanguage());
        } else if (PlatformInfo.isMac() == true) {
            app = new TokenizerFile(ApplicationPath.root.getAbsolutePath(), "/lib/tokenizer/mac/java/mkJavaTokenSC", Language.JAVA.getLanguage());
        }
    	if (app == null || !app.exists()) {
    		throw new NullPointerException();
    	} else {
    		return app;
    	}
	}

	private final static File initCrmPath() {
	    File app = null;
    	if (PlatformInfo.isWindows() == true) {
    		if (PlatformInfo.is64bitArch() == true) {
    		    app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/windows/x64_release/crm114.exe");
    		} else if (PlatformInfo.is32bitArch() == true) {
    			app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/windows/i382_release/crm114.exe");
    		}
    	} else if (PlatformInfo.isMac() == true) {
			app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/mac/crm114");
    	} else if (PlatformInfo.isLinux() == true) {
			app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/linux/crm114");
    	}
    	return app.exists() ? app : null;

	}

	private final static File initCssutilPath() {
	    File app = null;
    	if (PlatformInfo.isWindows() == true) {
    		if (PlatformInfo.is64bitArch() == true) {
    			app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/windows/x64_release/cssutil.exe");
    		} else if (PlatformInfo.is32bitArch() == true) {
    		    app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/windows/i382_release/cssctil.exe");
    		}
    	} else if (PlatformInfo.isMac() == true) {
    	    app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/mac/cssutil");
    	} else if (PlatformInfo.isLinux() == true) {
    	    app = new File(ApplicationPath.root.getAbsolutePath(), "/lib/CRM114/linux/cssutil");
    	}
        return app.exists() ? app : null;
	}

	private final static boolean checkFields() {
		if (crm == null || !crm.exists()) {
			return false;
		} else if (cssutil == null || !cssutil.exists()) {
			return false;
		} else if (tokenizerList == null || tokenizerList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public final static File create(String path) {
		File file = new File(path);
		List<String> pl = new ArrayList<String>();
		pl.add(CRM.cssutil.getAbsolutePath());
		pl.add("-b");
		pl.add("-r");
		pl.add("-S");
		pl.add("500000");
		pl.add(file.getAbsolutePath());
		ProcessBuilder pb = new ProcessBuilder(pl);
		// pb.redirectErrorStream(true);
		try {
		  Process p = pb.start();
			InputStream is = p.getInputStream();	//標準出力
			printInputStream(is);
			InputStream es = p.getErrorStream();	//標準エラー
			printInputStream(es);
		  p.waitFor();
		} catch (IOException | InterruptedException e) {
			  System.out.println(e);
			  return null;
		}
		pl.clear();
		pl = null;
		return file;
	}

	static int count = 0;
    private final static String PROB_TEXT = "probability:";
    private final static String CLASSIFY_ARGUMENT = "-{match (:data:) /.*/ isolate (:stats:){classify [:data:] (:*:_arg2: | :*:_arg3:) (:stats:) /[[:graph:]]+/ output /:*:_arg2: matches better :*:_nl::*:stats::*:_nl:/ exit} output /:*:_arg3: matches better :*:_nl::*:stats::*:_nl:/}";
    public final static <T extends File> double classify(T file, FilterGroup filter) throws NullPointerException, IOException {
        if (!checkFields()) {
          throw new NullPointerException();
        }
        File tmpFile;
        System.out.println(filter.getAccurateDB().getAbsolutePath());
        try {
//            tmpFile = File.createTempFile("classifyresult", ".tmp");
            tmpFile = new File(filter.getAccurateDB().getParentFile().getAbsolutePath(), count + ".tmp");
            tmpFile.createNewFile();
        } catch (IOException e1) {
//          throw new IOException();
            e1.printStackTrace();
            return -1;
        }
	    System.out.println(filter.getAccurateDB().getAbsolutePath());
        ArrayList<String> pl = new ArrayList<>();
        pl.addAll(Commands.cat(file));
        pl.add("|");
        pl.add(getTokenizer(filter).getAbsolutePath());
        pl.add("|");
        pl.add(crm.getAbsolutePath());
        pl.add(CLASSIFY_ARGUMENT);
        pl.add(filter.getFaultyDB().getAbsolutePath());
        pl.add(filter.getAccurateDB().getAbsolutePath());
        ProcessBuilder pb = new ProcessBuilder(pl);
        pb.redirectOutput(tmpFile);
        try {
          Process p = pb.start();
        InputStream is = p.getInputStream();    //標準出力
        printInputStream(is);
        InputStream es = p.getErrorStream();    //標準エラー
        printInputStream(es);
          p.waitFor();
        } catch (IOException | InterruptedException e) {
              System.out.println(e);
              return -1;
        }
        pl.clear();
        System.out.println(tmpFile.getAbsolutePath());
        try{
            BufferedReader br = new BufferedReader(new FileReader(tmpFile));
            String str = br.readLine();
            Boolean reverse = false;
            while (str != null){
            	if (str.startsWith(filter.getAccurateDB().getAbsolutePath())) {
            		reverse = true;
            	}
                if (0 <= str.indexOf("CLASSIFY") ){
                    int plusPointer = str.indexOf(PROB_TEXT) + PROB_TEXT.length();
                    int minusPointer = str.indexOf("pR:");
                    double val = Double.parseDouble(str.substring(plusPointer, minusPointer - 1));
                    br.close();
                    return val;
//                    tmpFile.delete();
//                    return (reverse) ? 1 - val : val;
                }
                str = br.readLine();
            }
            tmpFile.delete();
            br.close();
        } catch(IOException e){
            System.out.println(e);
            return -1;
        }
        return -1;
    }


	private final static File getTokenizer(FilterGroup filter) throws NullPointerException {
		for (TokenizerFile tokenizer: CRM.tokenizerList) {
			if (tokenizer.getLanguage().equals(filter.getLanguage())) {
				return tokenizer;
			}
		}
		throw new NullPointerException();
	}

	private final static String LEARN_ARGUMENT = "\"-{ match (:data:) /.*/ learn (:*:_arg2:) [:data:] /[[:graph:]]+/}\"";
	public final static <T extends File> double learn(T file, boolean isAccurate, FilterGroup filter) throws NullPointerException, IOException {
		File learnDB = (isAccurate)? filter.getAccurateDB() : filter.getFaultyDB();
		List<String> pl = new ArrayList<String>();
		pl.addAll(Commands.cat(file));
		pl.add("|");
		pl.add(getTokenizer(filter).getAbsolutePath());
		pl.add("|");
		pl.add(crm.getAbsolutePath());
		pl.add(LEARN_ARGUMENT);
		pl.add(learnDB.getAbsolutePath());
		ProcessBuilder pb = new ProcessBuilder(pl);
		pb.redirectOutput(new File(learnDB.getParent(), "err_learning.log"));
		 pb.redirectErrorStream(true);
		try {
		  Process p = pb.start();
//			InputStream is = p.getInputStream();	//標準出力
//			printInputStream(is);
//			InputStream es = p.getErrorStream();	//標準エラー
//			printInputStream(es);
		  p.waitFor();
		} catch (IOException | InterruptedException e) {
			  System.out.println(e);
			  return -1;
		}
		pl.clear();
		pl = null;
		return 1;
	}

	public static void printInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		try {
			for (;;) {
				String line = br.readLine();
				if (line == null) break;
				System.out.println(line);
			}
		} finally {
			br.close();
		}
	}
}
