package codelamp.widget.config;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import codelamp.pio.Language;


public abstract class AbstractFilterLanguageSelector extends Composite implements SelectionListener {
    private Label label;
	private Text text;
	private Combo combo;
    private ArrayList<Language> langMap;

	public AbstractFilterLanguageSelector(Composite parent, int style) {
		super(parent, style);
		langMap= Language.getAll();
        this.setLayout(new GridLayout(2,false));
        GridData data = new GridData(GridData.FILL_HORIZONTAL);
        data.horizontalSpan = 1;
        GridData wideData = new GridData(GridData.FILL_HORIZONTAL);
        wideData.horizontalSpan = 2;
        label = new Label(this, SWT.NONE);
        label.setLayoutData(wideData);
        label.setText("Language and Extensions");
        combo =  new Combo(this,SWT.DROP_DOWN | SWT.READ_ONLY);
        combo.addSelectionListener(this);
        combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        langMap.forEach(e -> combo.add(e.getLanguage()));
        combo.select(0);
        combo.setLayoutData(data);
        text = new Text(this, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
        text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        text.setLayoutData(data);
        setText();
        this.pack();
	}

	protected void setLabel(String str) {
	    label.setText(str);
	}

    protected void clearText() {
        text.setText("");;
    }

    protected void setText(String str) {
    	Language tmpLang = Language.pase(str);
    	if (tmpLang == Language.EMPTY) {
    		return;
    	} else if (langMap.contains(tmpLang)) {
            StringBuilder builder = new StringBuilder();
            for (String tmp : tmpLang.getExtension()) {
                builder.append(tmp);
                builder.append(" ");
            }
            text.setText(new String(builder));
    	}
    }

    protected void setText() {
        this.setText(combo.getText());
    }

    protected void setCombo(String str) {
        String[] langs = combo.getItems();
        int index = 0;
        for (int i = 0; i < langs.length; i++) {
            if (langs[i].equals(str)) {
                index = i;
                break;
            }
        }
        combo.select(index);
        this.setText(str);
    }
    protected void setCombo(Language language) {
        String[] langs = combo.getItems();
        int index = 0;
        for (int i = 0; i < langs.length; i++) {
            if (langs[i].equals(language.getLanguage())) {
                index = i;
                break;
            }
        }
        combo.select(index);
        this.setText();
    }

    protected String getSelect() {
        return combo.getText();
    }

    public void dispose() {
    	this.label.dispose();
    	this.combo.dispose();
    	this.text.dispose();
    	super.dispose();
    }

}
