package codelamp.widget.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import codelamp.widget.SrcGitLamp;

public class ConfigWindow extends Composite {
    
    private final SrcGitLamp mainWindow;
    private FilterTab filterTab;

    public ConfigWindow(Shell parent, int style, SrcGitLamp srcGitLamp) {
        super(parent, style);
        this.mainWindow = srcGitLamp;
        this.setLayout(new GridLayout(2,false));
        CTabFolder tab = new CTabFolder(this, SWT.BORDER);
        tab.setSimple(false);
        tab.setLayoutData(new GridData(GridData.FILL_BOTH));
        CTabItem firstTabItem = new CTabItem(tab, SWT.NONE);
        firstTabItem.setText("Filter Setting");
        this.filterTab = new FilterTab(tab, SWT.NONE, this.mainWindow);
        this.filterTab.setLayoutData(new GridData(GridData.FILL_BOTH));
        firstTabItem.setControl(this.filterTab);
//        CTabItem secondTabItem = new CTabItem(tab, SWT.NONE);
//        secondTabItem.setText("");
//        Composite c = new Composite(tab,SWT.NONE);
//        c.setLayoutData(new GridData(GridData.FILL_BOTH));
//        secondTabItem.setControl(c);
        this.pack();
    }

}
