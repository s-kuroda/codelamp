package codelamp.widget.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public abstract class AbstractDataBaseSelector extends Composite
		implements SelectionListener {
    private Label label;
	private Text text;
	private Button button;

	private String[] extension;
	public void setExtension(String[] extensions) {
		this.extension = extensions;
	}
	public void setExtension(String extension) {
		String[] tmpArray = {extension};
		this.extension = tmpArray;
	}
	public String[] getExtension() {
		return this.extension;
	}

	public AbstractDataBaseSelector(Composite parent, int style) {
		super(parent, style);
        this.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        GridLayout layout = new GridLayout();
        layout.numColumns = 3;
        layout.makeColumnsEqualWidth = false;
		this.setLayout(layout);
        label = new Label(this, SWT.NONE);
        GridData wideData = new GridData(GridData.FILL_HORIZONTAL);
        wideData.horizontalSpan = 3;
        label.setLayoutData(wideData);
        text = new Text(this, SWT.SINGLE | SWT.BORDER);
        text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        button = new Button(this, SWT.PUSH);
        button.setText("OPEN");
        button.addSelectionListener(this);
        this.pack();
        this.clearText();
	}

	protected void setLabel(String str) {
	    label.setText(str);
	}
    protected void clearText() {
        text.setText("");;
    }

  protected void setButtonText(String str) {
      button.setText(str);
  }

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		Object obj=event.getSource();
		if (obj == button) {
		    buttonAction();
		}
	}

	abstract void buttonAction();

    protected void setText(String str) {
        text.setText(str);
    }

    protected String getText() {
        return text.getText();
    }

    public void dispose() {
        this.label.dispose();
    	this.text.dispose();
    	this.button.dispose();
    	super.dispose();
    }
}
