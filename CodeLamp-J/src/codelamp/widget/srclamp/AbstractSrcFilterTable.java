package codelamp.widget.srclamp;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import codelamp.pio.Language;
import codelamp.pio.SortTargetFiles;
import codelamp.pio.TargetFile;
import codelamp.util.EpocDateTime;

public abstract class AbstractSrcFilterTable<T>
		extends Composite implements SelectionListener {
	private Table table;
	private ArrayList<TableColumn> column = new ArrayList<TableColumn>();
	private ArrayList<TargetFile> itemList = null;
	public void setTargetList(ArrayList<TargetFile> list) {
		this.itemList = list;
		this.setTableItem();
	}
	private Language itemLanguage;

	enum COLUMN_TEXT  {No, Result, File_Name, STATE, Classified_Time, Full_Path};

	public AbstractSrcFilterTable(Composite parent, int style) {
		super(parent, style);
		this.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.setLayout(new GridLayout(1,false));
        Label tableLabel = new Label(this,SWT.NONE);
        tableLabel.setText("Prediction Result And File State in the Project");
        table = new Table(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.addSelectionListener(this);
        table.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent event) {
				System.out.println("RELEASED");
				System.out.println("CHAR = " + event.character);
				System.out.println("CODE = " + event.keyCode);
			}

			@Override
			public void keyPressed(KeyEvent event) {
				System.out.println("PRESSED");
				System.out.println("CHAR = " + event.character);
				System.out.println("CODE = " + event.keyCode);
			}
		});
        this.table.setHeaderVisible(true);
        for(int i=0; i<COLUMN_TEXT.values().length; i++){
          TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
          tmpCol.setText(COLUMN_TEXT.values()[i].toString());
          tmpCol.addSelectionListener(this);
          column.add(tmpCol);
        }
        this.initTableItem();
	}

	protected void initTableItem(){
        SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Time);
        this.setTableItem();
	}

    protected  void setTableItem(){
        this.table.removeAll();
        int counter = 1;
        try {
	        if(itemList == null) {
	            TableItem item = new TableItem(this.table,SWT.NULL);
	            String[] itemData = new String[6];
	        	for (int i = 0; i < 6; i++) {
	        		itemData[i] = "bar";
	        	}
	            item.setText(itemData);
	        } else {
		        for (TargetFile file : itemList) {
		            if (file.getExtension() == null) {
		                continue;
		            } else if (file.getLanguage() == itemLanguage){
		                TableItem item = new TableItem(this.table,SWT.NULL);
		                String[] itemData = new String[6];
		                itemData[0] = String.valueOf(counter++);
		                if ((file.getInferenceVal() == 0) || (file.isNoOrder() == true)) {
		                    itemData[1] = "";
		                } else {
		                	StringBuilder tmpStrBuilder = new StringBuilder();
		                	if (file.isErrata()) {
		                    	tmpStrBuilder.append("*");
		                	}
		                	tmpStrBuilder.append(file.getTag()).append(" ");
		                	tmpStrBuilder.append(file.getInferenceVal());
		                	itemData[1] = tmpStrBuilder.toString();
		                }
		                itemData[2] = file.getName();
		                itemData[3] = file.getState();
		                if (file.lastClassified() == 0) {
		                    itemData[4] = "";
		                } else {
		                    itemData[4] = EpocDateTime.convertEpocToString(file.lastClassified());
		                }
		                itemData[5] = file.getAbsolutePath();
		                item.setText(itemData);
		            }
		        }
	        }
        } catch (NullPointerException e) {
        	System.out.println("Target : null");
        }
        this.ResizeTable();
    }

    private void ResizeTable(){
        this.table.setVisible(false);
        TableColumn[] columns = this.table.getColumns();
        Arrays.stream(columns).forEach(e -> e.pack());
        this.table.setVisible(true);
    }
	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
        Object obj = event.getSource();
        if (obj == this.column.get(0)) {
            this.table.selectAll();
        } else if (obj == this.column.get(1)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Value);
            this.setTableItem();
        } else if (obj == this.column.get(2)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Name);
            this.setTableItem();
        } else if (obj == this.column.get(3)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.State);
            this.setTableItem();
        } else if (obj == this.column.get(4)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.Time);
            this.setTableItem();
        } else if (obj == this.column.get(5)) {
            SortTargetFiles.sortList(itemList, SortTargetFiles.SortType.AbsolutePath);
            this.setTableItem();
        } else {
        }
	}

	public TableItem[] getSelection() {
		return this.table.getSelection();
	}

}
