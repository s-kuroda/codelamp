package codelamp.widget.srclamp;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import codelamp.internal.filter.Discriminator;
import codelamp.pio.FilterGroup;
import codelamp.pio.Language;
import codelamp.pio.TargetFile;
import codelamp.util.EpocDateTime;
import codelamp.util.FileListCapture;

public class SrcResultViewer extends Composite {

	private SrcResultTable table;
	private SrcFilterButtons btns;

	private File project = null;
	private Language language = null;
    private ArrayList<TargetFile> fileList = new ArrayList<>();
    public ArrayList<TargetFile> getFileList() {
        return this.fileList;
    }

    public void setTarget(File project, FilterGroup filter, ArrayList<TargetFile> fileList) {
        if (project == null || filter == null) {
            return;
        }
        btns.setBtnsEnabled(true);
        if (this.project == null || this.language == null ||
                !this.project.equals(project) || !this.language.equals(language)) {
            if (this.fileWatcher != null) {
                this.fileWatcher.kill();
            }
            if (this.autoFilter != null) {
                this.autoFilter.kill();
            }
            this.project = project;
            this.language = filter.getLanguage();
            this.fileList.clear();
            this.fileList = fileList;
            this.table.setTarget(this.fileList, this.language);
            this.classifyQueue.clear();
            this.learnQueue.clear();
            this.fileWatcher = new FileWatcher(project, filter.getLanguage());
            this.autoFilter = new AutoFilter(filter);
            this.fileWatcher.start();
            this.autoFilter.start();
        }
    }

//    // ソート後、大小比較しながら比較すると効率的
//    public void reflectRecords(ArrayList<TargetFile> recordedFiles) {
//        recordedFiles.forEach(e -> {
//            for (TargetFile entry: SrcResultViewer.this.fileList) {
//                if (e.getAbsolutePath().equals(entry.getAbsolutePath())) {
//                    entry = e;
//                }
//            }
//        });
//    }

    private LinkedList<TargetFile> classifyQueue = new LinkedList<>();
    private LinkedList<TargetFile> learnQueue = new LinkedList<>();

	public SrcResultViewer(Composite parent, int style) {
		super(parent, style);
		this.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.setLayout(new GridLayout(1,false));
        SashForm sash = new SashForm(this, SWT.HORIZONTAL);
        sash.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.table = new SrcResultTable(sash, SWT.NONE);
        this.table.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.btns = new SrcFilterButtons(sash, SWT.NONE);
        this.btns.setLayoutData(new GridData(GridData.FILL_BOTH));
        this.btns.setBtnsEnabled(false);
        int[] sashWeight = {3, 1};
        sash.setWeights(sashWeight);
        this.pack();
	}

	private class SrcFilterButtons extends AbstractSrcFilterButtons {

		public SrcFilterButtons(Composite parent, int style) {
			super(parent, style);
		}

		@Override
		protected void faultyButtonAction() {
		    SrcResultViewer.this.table.getSelectedFiles().forEach(e -> {
		    	e.setTag(false);
                e.waitLearn();
                SrcResultViewer.this.learnQueue.add(e);
                SrcResultViewer.this.table.setState(e);
		    });
//            SrcResultViewer.this.table.setTarget(fileList, language);
            System.out.println("BUG");
        }

		@Override
		protected void accurateButtonAction() {
		    SrcResultViewer.this.table.getSelectedFiles().forEach(e -> {
		    	e.setTag(true);
                e.waitLearn();
                SrcResultViewer.this.learnQueue.add(e);
                SrcResultViewer.this.table.setState(e);
		    });
//            SrcResultViewer.this.table.setTarget(fileList, language);
            System.out.println("INNOCENT");
        }

		@Override
		protected void excludeButtonAction() {
            SrcResultViewer.this.table.getSelectedFiles().forEach(e -> {
                e.exclude();
                SrcResultViewer.this.table.setState(e);
            });
            System.out.println("ECLUDE");
		}

		@Override
		protected void classifyButtonAction() {
            SrcResultViewer.this.table.getSelectedFiles().forEach(e -> {
                e.waitClassify();
                SrcResultViewer.this.classifyQueue.add(e);
                SrcResultViewer.this.table.setState(e);
            });
            System.out.println("CLASSIFY");
        }

		@Override
		protected void learnButtonAction() {
            SrcResultViewer.this.table.getSelectedFiles().forEach(e -> {
                e.waitLearn();
                SrcResultViewer.this.learnQueue.add(e);
                SrcResultViewer.this.table.setState(e);
            });
            System.out.println("LEARN");
        }
	}

    private FileWatcher fileWatcher = null;
    private AutoFilter autoFilter = null;

    private class AutoFilter extends Thread {

        private boolean kill = false;
        private final FilterGroup filter;

        public AutoFilter(FilterGroup filter) {
            this.filter = filter;
        }

        public void run() {
            kill = false;
            while (! kill) {
                if (! SrcResultViewer.this.classifyQueue.isEmpty()) {
                    classify(SrcResultViewer.this.classifyQueue.removeFirst());
                }
                if (! SrcResultViewer.this.learnQueue.isEmpty()) {
                    learn(SrcResultViewer.this.learnQueue.removeFirst());
                } else {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private void learn(TargetFile target) {
            try {
                target.nowClassify();
                getDisplay().asyncExec(new Runnable() {
                    public void run() {
                        SrcResultViewer.this.table.setState(target);
                    }
                });
                Discriminator.learn(target, !target.isBug(), this.filter);
                System.out.println("Learn");
                target.watch();
                getDisplay().asyncExec(new Runnable() {
                    public void run() {
                            SrcResultViewer.this.table.setTarget(fileList, language);
                    }
                });
            } catch (ConcurrentModificationException e) {
                learn(target);
            }
        }

        private void classify(TargetFile target) {
            try {
                target.nowClassify();
                getDisplay().asyncExec(new Runnable() {
                    public void run() {
                        SrcResultViewer.this.table.setState(target);
                    }
                });
                double infVal = Discriminator.classify(target, this.filter);
                target.setInferenceVal(infVal, 0.5);
                target.setLastClassified();
                System.out.println("Classify");
                target.watch();
                getDisplay().asyncExec(new Runnable() {
                    public void run() {
                            SrcResultViewer.this.table.setTarget(fileList, language);
                    }
                });
            } catch (ConcurrentModificationException e) {
                classify(target);
            }
        }

        public void kill() {
            kill = true;
        }
    }

    private class FileWatcher extends Thread {

        private boolean kill = false;
        private final File project;
        private final Language language;

        public FileWatcher(File project, Language language) {
            this.project = project;
            this.language = language;
        }

        public void run() {
            kill = false;
            while (! kill) {
                if (this.project == null || this.language == null) {
                    System.out.println("NOT PROJECT SELECTED");
                    return;
                } else {
                    try {
                        checkAdded();
                        checkRemoved();
                        checkModified();
                        Thread.sleep(500);
                    } catch (ConcurrentModificationException | InterruptedException e) {
                        System.out.println("Do not care Exception " + e);
                        return;
                    }
                }
            }
        }
        private void checkAdded() {
            ArrayList<TargetFile> currentList = FileListCapture.getChildTargetFileList(this.project);
            boolean added = false;
            for (TargetFile tmp : currentList) {
                if (tmp.getLanguage().equals(this.language)) {
                    if (isAdded(tmp, SrcResultViewer.this.fileList)) {
                        SrcResultViewer.this.fileList.add(tmp);
                        added = true;
                    }
                }
            }
            if (added == true) {
                getDisplay().asyncExec(new Runnable() {
                    public void run() {
                            SrcResultViewer.this.table.setTarget(SrcResultViewer.this.fileList, FileWatcher.this.language);
                    }
                });
            }
        }

        private <T extends File> boolean isAdded(T file, ArrayList<T> list) {
            for (File tmp : list) {
                if (file.getAbsolutePath().equals(tmp.getAbsolutePath())) {
                    return false;
                }
            }
            return true;
        }

        private void checkRemoved() {
            for (TargetFile tmp : SrcResultViewer.this.fileList) {
                if (isRemoved(tmp)) {
                    SrcResultViewer.this.fileList.remove(tmp);
                    getDisplay().asyncExec(new Runnable() {
                        public void run() {
                            SrcResultViewer.this.table.setTarget(SrcResultViewer.this.fileList, FileWatcher.this.language);
                        }
                    });
                }
            }
        }

        private <T extends File> boolean isRemoved(T file) {
            return (file.exists()) ? false : true;
        }

        private void checkModified() {
            for (TargetFile tmp : SrcResultViewer.this.fileList) {
                if (tmp.isWatched()) {
                    if (isModified(tmp.lastModified(), tmp.lastClassified())) {
                        SrcResultViewer.this.classifyQueue.add(tmp);
                        tmp.waitClassify();
                        getDisplay().asyncExec(new Runnable() {
                            public void run() {
                                SrcResultViewer.this.table.setState(tmp);
                            }
                        });
                    }
                }
            }
        }

        private boolean isModified(long currentTime, long previousTime) {
            LocalDateTime current = EpocDateTime.convertEpocToDateTime(currentTime);
            LocalDateTime previous = EpocDateTime.convertEpocToDateTime(previousTime);
            return (current.compareTo(previous) > 0) ? true : false ;
        }

        public void kill() {
            kill = true;
        }
    }

	@Override
	public void dispose() {
        if (this.fileWatcher != null) {
            this.fileWatcher.kill();
        }
        if (this.autoFilter != null) {
            this.autoFilter.kill();
        }
        this.fileList.clear();
        this.classifyQueue.clear();
        this.learnQueue.clear();
	    this.table.dispose();
	    this.btns.dispose();
	    super.dispose();
	}

//	public static void main(String[] args) {
//		System.out.println(System.getProperty("user_dir"));
//        Display display = Display.getDefault();
//        Shell shell = new Shell();
//        shell.setLayout(new GridLayout(1, false));
//        SrcResultViewer f = new SrcResultViewer(shell,SWT.NONE);
//        shell.pack();
//        shell.open();
//        shell.setSize(600,500);
//        while (!shell.isDisposed()) {
//            if (!display.readAndDispatch())
//                display.sleep();
//        }
//        f.dispose();
//        display.dispose();
//    }


}
