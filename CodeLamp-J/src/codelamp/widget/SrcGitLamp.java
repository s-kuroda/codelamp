package codelamp.widget;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import codelamp.internal.config.Config;
import codelamp.pio.FilterGroup;
import codelamp.pio.TargetFile;
import codelamp.util.ApplicationPath;
import codelamp.widget.common.AbstractProjectSelector;
import codelamp.widget.config.ConfigWindow;
import codelamp.widget.gitlamp.GitResultViewer;
import codelamp.widget.srclamp.SrcResultViewer;

public class SrcGitLamp extends Composite {

	private TargetProjectSelector selector;
	private Button configBtn;
	private SrcResultViewer srcView;
	private GitResultViewer gitView;
	private Shell configShell = null;

	private Config config;

    private File project;
    public ArrayList<TargetFile> getAllFiles() {
        if (this.srcView == null) {
            return null;
        }
        return this.srcView.getFileList();
    }
    public void setAllFilter(ArrayList<FilterGroup> filterList) {
        this.filterList = filterList;
    }

    private FilterGroup filter;
    public void setFilter(FilterGroup filter) {
        if (!this.filter.equals(filter)) {
            this.filter = filter;
            setProject(this.project);
        }
    }
    public FilterGroup getFilter() {
        return this.filter;
    }

    private ArrayList<FilterGroup> filterList = null;
    public ArrayList<FilterGroup> getAllFilters() {
        if (this.srcView == null) {
            return null;
        }
        return this.filterList;
    }

    public SrcGitLamp(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(2,false));
        GridData wide = new GridData(GridData.FILL_BOTH);
        wide.horizontalSpan = 2;
        selector = new TargetProjectSelector(this, SWT.NONE);
        selector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        configBtn = new Button(this, SWT.PUSH);
        configBtn.setText("Config");
        configBtn.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                if (project != null && (configShell == null || configShell.isDisposed())) {
                    configShell = new Shell();
                    configShell.setText("CodeLamp --- Config");
                    configShell.setLayout(new GridLayout(1,false));
                    ConfigWindow entry = new ConfigWindow(configShell, SWT.NONE, SrcGitLamp.this);
                    entry.setLayoutData(new GridData(GridData.FILL_BOTH));
                    configShell.setSize(800,450);
                    configShell.open();
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
            }
        });
        configBtn.setEnabled(false);
        CTabFolder tab = new CTabFolder(this, SWT.BORDER);
		tab.setSimple(false);
		tab.setLayoutData(wide);
		CTabItem firstTabItem = new CTabItem(tab, SWT.NONE);
		firstTabItem.setText("Source Codes");
        this.srcView = new SrcResultViewer(tab, SWT.NONE);
        this.srcView.setLayoutData(new GridData(GridData.FILL_BOTH));
		firstTabItem.setControl(this.srcView);
		CTabItem secondTabItem = new CTabItem(tab, SWT.NONE);
		secondTabItem.setText("Git Repository");
        this.gitView = new GitResultViewer(tab,SWT.NONE);
        this.gitView.setLayoutData(new GridData(GridData.FILL_BOTH));
		secondTabItem.setControl(this.gitView);
		this.pack();
		tab.setSelection(firstTabItem);
        DropTarget target = new DropTarget(this,DND.DROP_DEFAULT|DND.DROP_COPY);
        FileTransfer transfer = FileTransfer.getInstance();
        Transfer[] types = new Transfer[]{transfer};
        target.setTransfer(types);
        target.addDropListener(new DropTargetAdapter() {
            public void dragEnter(DropTargetEvent evt){
            evt.detail = DND.DROP_COPY;
            }
            public void drop(DropTargetEvent evt){
                String[] files = (String[])evt.data;
                SrcGitLamp.this.selector.addProject(files[0]);
            }
        });
	}


	private class TargetProjectSelector extends AbstractProjectSelector {

		public TargetProjectSelector(Composite parent, int style) {
			super(parent, style);
		}

        @Override
        public void reflect(File targetProject) {
            if (targetProject == null || !targetProject.exists()) {
                return;
            }
            SrcGitLamp.this.setProject(targetProject);
        }
	}

    public void setProject(File file) {
        if (file == null || file.isFile()) {
            return;
        }
        if (this.project == null || !this.project.getAbsolutePath().equals(file.getAbsolutePath())) {
            if (this.project != null) {
                config.updateAllFileRecords(this.srcView.getFileList());
                config.updateAllFilters(this.filterList);
                config.changeSelectedFilter(this.filter);
                SrcGitLamp.this.config.write();
            }
            configBtn.setEnabled(true);
            this.project = file;
            this.selector.addProject(file.getAbsolutePath());
            this.config = new Config(this.project, ApplicationPath.root);
            this.filterList = config.getRecordedFilters();
            this.filter = config.getSelectedFilter();
            this.filterList = this.config.getRecordedFilters();
            this.srcView.setTarget(this.project, this.filter, config.getRecordedFiles());
            this.gitView.setProject(this.project, this.filter);
        }
    }

    public void setProject(FilterGroup filter) {
        if (filter == null || this.project == null) {
            return;
        }
        if (!this.filter.equals(filter)) {
            config.updateAllFilters(this.filterList);
            config.changeSelectedFilter(filter);
            this.config.write();
            this.config = new Config(this.project, ApplicationPath.root);
            this.filterList = config.getRecordedFilters();
            this.filter = config.getSelectedFilter();
            this.filterList = this.config.getRecordedFilters();
            this.srcView.setTarget(this.project, this.filter, config.getRecordedFiles());
            this.gitView.setProject(this.project, this.filter);
        }
    }

	@Override
	public void dispose() {
	    config.updateAllFileRecords(this.srcView.getFileList());
	    config.updateAllFilters(this.filterList);
	    config.changeSelectedFilter(this.filter);
        config.write();
        if (this.configShell != null && !this.configShell.isDisposed()) {
            this.configShell.dispose();
        }
	    selector.dispose();
	    srcView.dispose();
	    gitView.dispose();
	    super.dispose();
	}

	public static void main(String[] args) {
		System.out.println(System.getProperty("user.dir"));
        Display display = Display.getDefault();
        Shell shell = new Shell();
        shell.setLayout(new GridLayout(1, false));
        SrcGitLamp lamp = new SrcGitLamp(shell,SWT.NONE);
        lamp.setLayoutData(new GridData(GridData.FILL_BOTH));
        shell.pack();
        shell.open();
        shell.setSize(600,500);
        if (args != null) {
            if (1 < args.length) {
                File file = new File(args[0]);
                lamp.setProject(file);
            }
        }
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        lamp.dispose();
        display.dispose();
    }


}