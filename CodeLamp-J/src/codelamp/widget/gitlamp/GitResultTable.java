package codelamp.widget.gitlamp;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.HashMap;

import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import codelamp.util.EpocDateTime;

public class GitResultTable extends Composite {

	private Table table;
    private ArrayList<TableColumn> column = new ArrayList<TableColumn>();
	private final ArrayList<String> COLUMN_TEXT = new ArrayList<>();
	{
    	COLUMN_TEXT.add("");
    	COLUMN_TEXT.add("Result");
    	COLUMN_TEXT.add("Commit Date");
    	COLUMN_TEXT.add("Commit Message");
	}

    private HashMap<RevCommit, Double> inferenceVals = new HashMap<>();
    private ArrayList<RevCommit> inferenceErratas = new ArrayList<>();

    private double threshhold = 0.5;

	public GitResultTable(Composite parent, int style) {
		super(parent, style);
		this.setLayout(new GridLayout(1,false));
		this.table = new Table(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION);
		this.table.setLayoutData(new GridData(GridData.FILL_BOTH));
		this.table.setHeaderVisible(true);
        this.table.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent event) {
				 if ((event.stateMask & SWT.CTRL) != 0 && (event.keyCode == 'c')) {
					 Clipboard clipboard = new Clipboard(getDisplay());
					 TableItem[] selectedItems = table.getSelection();
					 StringBuffer sb = new StringBuffer();
					 Arrays.stream(selectedItems).forEach(e -> {
						 for (int i = 0; i < column.size(); i++) {
							 sb.append(e.getText(i));
							 sb.append("\t");
						 }
						 sb.append("\n");
					 });
					 clipboard.setContents(new Object[] {sb.toString()}, new Transfer[] {TextTransfer.getInstance()});
				}
			}

			@Override
			public void keyPressed(KeyEvent event) {
			}
		});
		COLUMN_TEXT.forEach(e ->{
		    TableColumn tmpCol = new TableColumn(this.table,SWT.LEFT);
		    tmpCol.setText(e);
		    GitResultTable.this.column.add(tmpCol);
		});
		this.setTableItem();
	}

    private final String FAULTY = "Faulty";
    private final String ACCURATE = "Accurate";
    private final String FIRST_COMMIT = "FIRST";
    private final String HEAD_COMMIT = Constants.HEAD;

	protected void setTableItem(){
    	this.table.removeAll();
    	try {
    	    if(commitList == null) {
                this.resizeTable();
                return;
                /* View Test */
//    	        TableItem item = new TableItem(this.table,SWT.NULL);
//                String[] itemData = new String[4];
//                itemData[0] = "";
//                itemData[1] = "";
//                itemData[2] = EpocDateTime.convertEpocToString(new Date().getTime());
//                itemData[3] = "bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n";
//                item.setText(itemData);
//                item.setData("BAR");
//                item = new TableItem(this.table,SWT.NULL);
//                itemData = new String[4];
//                itemData[0] = "";
//                itemData[1] = "";
//                itemData[2] = EpocDateTime.convertEpocToString(new Date().getTime());
//                itemData[3] = "bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n bar bar bar \n";
//                item.setText(itemData);
//                item.setData("BAR2");
    	    } else {
    	    	commitList.forEach(e -> {
    	            TableItem item = new TableItem(this.table,SWT.NULL);
    	            String[] itemData = new String[4];
    	            if (e.equals(GitResultTable.this.firstCommit)) {
                        itemData[0] = FIRST_COMMIT;
                    } else if (e.equals(GitResultTable.this.headCommit)) {
                        itemData[0] = HEAD_COMMIT;
                    } else {
    	                itemData[0] = "";
    	            }
    	            if (GitResultTable.this.inferenceVals.containsKey(e)) {
                        StringBuilder tmpStrBuilder = new StringBuilder();
                        if (GitResultTable.this.inferenceErratas.contains(e)) {
                            tmpStrBuilder.append("*");
                            tmpStrBuilder.append((GitResultTable.this.inferenceVals.get(e) < threshhold)? FAULTY: ACCURATE);
                        } else {
                            tmpStrBuilder.append((GitResultTable.this.inferenceVals.get(e) < threshhold)? ACCURATE: FAULTY);
                        }
                        tmpStrBuilder.append(" ");
                        tmpStrBuilder.append(GitResultTable.this.inferenceVals.get(e));
                        itemData[1] = tmpStrBuilder.toString();
    	            } else {
    	                itemData[1] = "";
    	            }
    	            itemData[2] = EpocDateTime.convertEpocToString(e.getCommitTime());
    	            itemData[3] = e.getShortMessage();
    	            item.setText(itemData);
    	            item.setData(e);
    	    	});
    	    }
    	} catch (NullPointerException e) {
            e.printStackTrace();
    	}
    	this.resizeTable();
	}

	private void resizeTable(){
    	this.table.setVisible(false);
    	TableColumn[] columns = this.table.getColumns();
    	Arrays.stream(columns).forEach(e -> e.pack());
    	columns[0].setWidth(60);
    	this.table.setVisible(true);
	}

    private ArrayList<RevCommit> commitList = null;
    public void setCommitList(ArrayList<RevCommit> list) {
        this.commitList = list;
        this.firstCommit = list.get(list.size()-1);
        this.setTableItem();
    }

    public RevCommit headCommit;
    public void setHeadCommit(RevCommit headCommit) {
        this.headCommit = headCommit;
        try {
        	this.setTableItem();
        } catch (ConcurrentModificationException e) {
        	this.setHeadCommit(this.headCommit);
        }
    }

    public RevCommit firstCommit;
    public void setFirstCommit(RevCommit firstCommit) {
        this.firstCommit = firstCommit;
        this.setTableItem();
    }

	public ArrayList<RevCommit> getSelectedCommits() {
	    TableItem[] selectedItems = this.table.getSelection();
	    ArrayList<RevCommit> itemList = new ArrayList<>();
	    for (TableItem entry: selectedItems) {
	        if (entry.getData() instanceof RevCommit) {
	            RevCommit commit = (RevCommit) entry.getData();
	            if (commit != null) {
	                itemList.add(commit);
	            }
	        }
	    }
	    return itemList;
	}

	public void addInferenceVals(RevCommit commit, double val) {
	    if (this.inferenceVals.containsKey(commit)) {
	        this.inferenceVals.replace(commit, val);
	    } else {
	        this.inferenceVals.put(commit, val);
	    }
        this.setTableItem();
	}

    public void clearItems() {
        this.table.removeAll();
    }

    @Override
    public void dispose() {
        this.table.dispose();
        this.column.clear();
        super.dispose();
    }

    public static void main(String[] args) {
        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setText("CodeLamp --- Fault-Prone Filtering Plugin");
        shell.setLayout(new GridLayout(1,false));
        Menu menu = new Menu(shell, SWT.BAR);
        shell.setMenuBar(menu);
        MenuItem configButton = new MenuItem(menu, SWT.PUSH);
        configButton.setText("Setting");
        configButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e){
                System.out.println("MENU BUTTON SELECTED");
                System.out.println(shell.getSize());
            }
        });
        ArrayList<File> tmp = new ArrayList<>();
        tmp.add(new File("first"));
        tmp.add(new File("second"));
        GitResultTable composite = new GitResultTable(shell, SWT.BORDER);
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        Button btn = new Button(shell, SWT.PUSH);
        btn.setText("Setting");
        btn.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e){
                System.out.println(composite.table.getSelection());
            }
        });
        shell.setSize(800,450);
        shell.open();
        while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                        display.sleep();
                }
        }
        System.out.println(shell.isDisposed());
        display.dispose();
    }
}
