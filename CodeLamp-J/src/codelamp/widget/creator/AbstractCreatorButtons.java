package codelamp.widget.creator;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import codelamp.pio.Language;

public abstract class AbstractCreatorButtons
		extends Composite implements SelectionListener {

	private Combo language;
	private final ArrayList<Language> LANGUAGE_TEXT = new ArrayList<>();
	{
		LANGUAGE_TEXT.add(Language.JAVA);
	}
	public Language getLanguage() {
		return Language.pase(this.language.getText());
	}

	private ArrayList<Button> btns = new ArrayList<>();
	private final ArrayList<String> BUTTON_TEXT = new ArrayList<>();
	{
		BUTTON_TEXT.add("Select as Faulty");
		BUTTON_TEXT.add("Select as Accurate");
		BUTTON_TEXT.add("Cancel Selected");
		BUTTON_TEXT.add("Clear Checked");
		BUTTON_TEXT.add("Reverse Checked");
		BUTTON_TEXT.add("START DB CREATING");
	}

	public AbstractCreatorButtons(Composite parent, int style) {
		super(parent, style);
        this.setLayout(new GridLayout(1, false));
        this.language = new Combo(this, SWT.READ_ONLY );
		LANGUAGE_TEXT.forEach(e -> {
			this.language.add(e.getLanguage());
		});
		this.language.select(0);
        this.language.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        Label empty = new Label(this, SWT.NULL);
		empty.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Button tmpBtn = new Button(this, SWT.PUSH);
		tmpBtn.setText(BUTTON_TEXT.get(0));
		tmpBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn.addSelectionListener(this);
		this.btns.add(tmpBtn);
		tmpBtn = new Button(this, SWT.PUSH);
		tmpBtn.setText(BUTTON_TEXT.get(1));
        tmpBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn.addSelectionListener(this);
		this.btns.add(tmpBtn);
		tmpBtn = new Button(this, SWT.PUSH);
		tmpBtn.setText(BUTTON_TEXT.get(2));
        tmpBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn.addSelectionListener(this);
		this.btns.add(tmpBtn);
//		Label separater1 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
        empty = new Label(this, SWT.NULL);
        empty.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn = new Button(this, SWT.PUSH);
		tmpBtn.setText(BUTTON_TEXT.get(3));
        tmpBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn.addSelectionListener(this);
		this.btns.add(tmpBtn);
		tmpBtn = new Button(this, SWT.PUSH);
		tmpBtn.setText(BUTTON_TEXT.get(4));
        tmpBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn.addSelectionListener(this);
		this.btns.add(tmpBtn);
//		Label separater2 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		empty = new Label(this, SWT.NULL);
        empty.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn = new Button(this, SWT.PUSH);
		tmpBtn.setText(BUTTON_TEXT.get(5));
        tmpBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpBtn.addSelectionListener(this);
		this.btns.add(tmpBtn);
		this.pack();
	}

	public void setButtonsLock(boolean lock) {
		this.language.setEnabled(!lock);
		this.btns.forEach(e -> e.setEnabled(!lock));
	}

	public void setButtonsLock(Integer[] lockList, boolean lock) {
		Arrays.stream(lockList).forEach(i -> btns.get(i).setEnabled(lock));
	}
	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
      Object obj = event.getSource();
		if (obj == this.btns.get(0)) {
			this.setFaulty();
		} else if (obj == this.btns.get(1)) {
			this.setAccurate();
		} else if (obj == this.btns.get(2)) {
			this.unSelect();
		} else if (obj == this.btns.get(3)) {
			this.clearChecked();
		} else if (obj == this.btns.get(4)) {
			this.reverseChecked();
		} else if (obj == this.btns.get(5)) {
			this.startSetUp();
		}
	}

	protected abstract void setFaulty();
	protected abstract void setAccurate();
	protected abstract void unSelect();
	protected abstract void clearChecked();
	protected abstract void reverseChecked();
	protected abstract void startSetUp();

	@Override
	public void dispose() {
	    this.btns.forEach(e -> e.dispose());
	    this.language.dispose();
	    super.dispose();
	}
}
